# Clone repo
Here is a git submodule for the front end part, so git clone should be used with `--recurse-submodules` argument

`git clone --recurse-submodules`

If the repo has already cloned, you need to initialize the submodule and fetch it:

`git submodule init`

`git submodule update`

# Add start script to PATH
 
`sudo cp app /usr/local/bin`

# Translation

To apply fixtures with translation use command:

`app dev translation`

# Testing

- `app testing run django pytest`

# Run locally

## First run

Install front end dependencies

`app dev run node npm i`

Apply fixtures

`app dev fixtures`

---

UPD for first run application on local machine use:

`./app dev deploy_local_upd`
for creating containers and restoring db from last dumps

---

Up both front & back

`app dev up`

---

Downloading from google docs

`./app dev upload_from_google`

it will use all awailable django-commands for it

---

#Front-end assembly

`gulp dev` 

It will make all of what needed for developing

---

`gulp prod` 

It will run style preprocessor, move images/styles/files from /static folder to /staticfiles folder. 

---

# Commands
Django commands list, that loads info from google docs:

`ll server/apps/core/management/commands/`

To run one of these commands use

`python3 server/manage.py %command_name_without_extension%`

For example 

`python3 server/manage.py load_contractors`

will load all contractors from 'Для заливки' google docs table 

## Additional commands

`delete_test_services`
`delete_test_projects`
`delete_test_bloggers`
`delete_all_projects`
`slugify_company_ownership`
---

# CMS pages
For creating cms tables new dump run

`./app dev cms_dump`

It will create new dump file in ./pg_dumps directory with mask 
cms_dump_%date%_%time%.%username%.sql

For restoring data from damp use
`./scripts/cms_restore.sh %path_to_dump_file%`

---

#Deleting test bloggers
Careful! This will delete all bloggers except Tod, Lesya and Darryl. See help for more information!
For deleting test bloggers use 
`server/manage.py delete_test_bloggers`
