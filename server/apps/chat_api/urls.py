from django.urls import path
from . import views

urlpatterns = [
    path("<int:chat_room_id>/projects/", views.ChatProjectsListApiView.as_view()),
    path(
        "<int:chat_room_id>/projects/<int:project_id>/",
        views.GetChatProjectApiView.as_view(),
    ),
    path("pm/", views.CreatePMChatApiView.as_view(), name="create_pm_chat"),
]
