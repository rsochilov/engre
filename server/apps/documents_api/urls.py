from django.urls import path
from . import views

urlpatterns = [
    path(
        "<int:pk>/download/",
        views.DownloadDocumentView.as_view(),
        name="documents-download",
    ),
    path("<int:pk>/", views.UpdateDocumentsView.as_view(), name="documents-update"),
    path(
        "<int:pk>/resign/", views.ResignDocumentView.as_view(), name="documents-resign"
    ),
    path("", views.CreateDocumentsView.as_view(), name="documents-create"),
]
