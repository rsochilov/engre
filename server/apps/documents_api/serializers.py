from rest_framework.serializers import ModelSerializer, ValidationError
from rest_framework import status
from company.models import Document
import re


class DocumentSerializer(ModelSerializer):
    version_regex = re.compile(r"^\d{1,2}\.\d{1,2}$")

    class Meta:
        model = Document
        fields = "__all__"

    def validate_version(self, value):
        if not self.version_regex.match(value):
            raise ValidationError("Invalid version")
        return value

    def update(self, instance, validated_data):
        old_version = float(self.instance.version)
        new_version = float(validated_data["version"])
        if old_version > new_version:
            raise ValidationError("Invalid version")
        elif old_version == new_version:
            validated_data["version"] = "{:.2f}".format(old_version + 0.01)
        return super().update(instance, validated_data)
