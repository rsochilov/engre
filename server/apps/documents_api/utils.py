from os import path
from uuid import uuid4
from django.template.loader import render_to_string
from html import unescape
from django.core.files.storage import FileSystemStorage

from weasyprint import HTML

from company.models import Document, Signature


class DocumentPDFConverter:
    @staticmethod
    def write_to_file(document: Document, name: str) -> str:
        """Writes document info to html file

        Args:
            document (Document): document model

        Returns:
            str: filename with mask {uuid4}_{name}
        """
        context = {
            "document": document,
        }
        rendered_template = unescape(
            render_to_string("documents_api/document.html", context)
        )
        uuid_name_part = str(uuid4())

        filename = f"{uuid_name_part}_{name}"

        html = HTML(string=rendered_template)
        html.write_pdf(target=path.join("/tmp", filename))
        return filename
