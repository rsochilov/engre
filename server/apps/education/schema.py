import graphene

from .tasks import send_learn_more_email


class SendLearnMoreEmail(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        email = graphene.String(required=True)
        company = graphene.String()
        phone = graphene.String()
        comment = graphene.String()

    status = graphene.Boolean()

    def mutate(self, info, name, email, company=None, phone=None, comment=None):
        if name and email:
            send_learn_more_email.delay(
                name=name, email=email, company=company, phone=phone, comment=comment,
            )
            status = True
        else:
            status = False
        return SendLearnMoreEmail(status=status)


class Mutation(graphene.ObjectType):
    send_learn_more_email = SendLearnMoreEmail.Field()
