from unittest.mock import patch

from django.core import mail
from django.test import TestCase
from django.urls import reverse

from .tasks import send_learn_more_email


class SchemaSendLearnMoreEmailTestCase(TestCase):
    def test_send(self):
        query = """
            mutation {
              sendLearnMoreEmail(name: "Steve", email: "steve@email.com") {
                status
              }
            }
        """

        with patch("education.schema.send_learn_more_email.delay") as mock_function:
            response = self.client.post(reverse("api"), data={"query": query})
            self.assertTrue(mock_function.called)

        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["sendLearnMoreEmail"]["status"])

    def test_send_without_required_fields(self):
        query = """
            mutation {
              sendLearnMoreEmail {
                status
              }
            }
        """

        with patch("education.schema.send_learn_more_email.delay") as mock_function:
            response = self.client.post(reverse("api"), data={"query": query})
            self.assertFalse(mock_function.called)

        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertIn("errors", data)


class TaskSendLearnMoreEmailTestCase(TestCase):
    def test_send(self):
        r = send_learn_more_email(
            name="Steve",
            email="steve@email.com",
            company=None,
            phone=None,
            comment=None,
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "steve@email.com / Learn more")
        self.assertIn("Steve", mail.outbox[0].alternatives[0][0])
