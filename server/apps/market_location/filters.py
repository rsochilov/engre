import django_filters

from .models import City


class CityFilterSet(django_filters.FilterSet):
    class Meta:
        model = City
        fields = {
            "name": ["icontains"],
        }
