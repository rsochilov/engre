from django.test import TestCase
from django.urls import reverse


class LocationAPITestCase(TestCase):
    def test_get_counties_with_pagination(self):
        data = {
            "query": """
            query {
              allCountries(first:1) {
                edges {
                  node {
                    name
                  }
                }
              }
            }
          """
        }
        response = self.client.post(reverse("api"), data=data, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_get_regions_with_pagination(self):
        data = {
            "query": """
            query {
              allRegions(first:1) {
                edges {
                  node {
                    name
                  }
                }
              }
            }
          """
        }
        response = self.client.post(reverse("api"), data=data, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_get_cities_with_pagination(self):
        data = {
            "query": """
            query {
              allCities(first:1) {
                edges {
                  node {
                    name
                  }
                }
              }
            }
          """
        }
        response = self.client.post(reverse("api"), data=data, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())
