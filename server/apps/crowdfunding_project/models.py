from core.models import AbstractRateable, ArchivableMixin, Language, Tag, WatchableMixin
from accounts.models import User
from market_location.models import City
from project.models import ACTIVE, STATUSES


class CrowdfundingProject(ArchivableMixin, WatchableMixin):
    name = models.CharField(max_length=255)
    description = models.TextField()
    budget = models.DecimalField(decimal_places=2, max_digits=10)
    date_start = models.DateTimeField()
    date_finish = models.DateTimeField()
    website = models.CharField(max_length=255)
    owner = models.ForeignKey(User, related_name="crowdfunding_projects")
    languages = models.ManyToManyField(Language, related_name="crowdfunding_projects")
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=ACTIVE)
    tags = models.ManyToManyField(Tag, related_name="crowdfunding_projects")

    picture = models.ImageField(upload_to="project_images", null=True, blank=True)
    document = models.FileField(upload_to="project_documents", null=True, blank=True)
    video = models.ImageField(upload_to="project_videos", null=True, blank=True)

    location = models.ForeignKey(City, related_name="crowdfunding_projects")

    category = models.ForeignKey("core.Category", related_name="crowdfunding_projects")


class CrowdfundingProjectRating(AbstractRateable):
    crowd_project = models.ForeignKey(CrowdfundingProject, related_name="rating")

    class Meta:
        db_table = "crowdfunding_project_crowdfundingproject_rating"
