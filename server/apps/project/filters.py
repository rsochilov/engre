import django_filters
from core.filters import IdArrayFilter
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.db.models import Q, Avg

from .models import Category, CoreSkill, Industry, IntellectualService, Project


class CustomOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extra["choices"] += [
            ("relevance", "Relevance"),
            ("-relevance", "Relevance (descending)"),
        ]

    def filter(self, qs, value):
        if value:
            if "rating" in value:
                value.remove("rating")
                qs = qs.annotate(rating=Avg("company__reviews__rating")).order_by(
                    "-rating"
                )
            if "relevance" in value:
                value.remove("relevance")
                if "search" in self.parent.data.keys():
                    qs = qs.distinct("rank", "id").order_by("-rank", "id")

        return super().filter(qs, value)


class CategoryFilterSet(django_filters.FilterSet):
    class Meta:
        model = Category
        fields = {
            "name": ["icontains"],
            "slug": ["icontains"],
        }


class IndustryFilterSet(django_filters.FilterSet):
    class Meta:
        model = Industry
        fields = {
            "name": ["icontains"],
            "slug": ["icontains"],
        }


class CoreSkillFilterSet(django_filters.FilterSet):
    class Meta:
        model = CoreSkill
        fields = {
            "name": ["icontains"],
            "slug": ["icontains"],
        }


class ServiceBaseFilterSet(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")
    category__slug = django_filters.CharFilter(
        field_name="category__slug", lookup_expr="iexact"
    )
    industry__slug = django_filters.CharFilter(
        field_name="industry__slug", lookup_expr="iexact"
    )
    core_skills__slug = django_filters.CharFilter(
        field_name="core_skills__slug", lookup_expr="iexact"
    )

    company__flexible_working_hours = django_filters.BooleanFilter(
        field_name="company__flexible_working_hours", lookup_expr="exact"
    )
    company__ownership__slug = django_filters.CharFilter(
        field_name="company__ownership__slug", lookup_expr="exact"
    )
    company__company_size = django_filters.NumberFilter(
        field_name="company__company_size", lookup_expr="exact"
    )

    def search_filter(self, qs, name, value):
        if len(value) == 0:
            return qs

        title_vector = SearchVector("name", "company__name", weight="A")
        description_vector = SearchVector(
            "description", "company__description", weight="B"
        )
        tags_vector = SearchVector("tags__name", weight="C")
        vectors = title_vector + description_vector + tags_vector
        query = SearchQuery(value)

        qs = (
            qs.annotate(search=vectors)
            .filter(search=query)
            .annotate(rank=SearchRank(vectors, query))
        )
        return qs


class IntellectualServiceFilterSet(ServiceBaseFilterSet):
    rate_range_type = django_filters.NumberFilter(method="rate_filter")

    order_by = CustomOrderingFilter(
        fields=(
            ("created_at", "date"),
            ("views", "popularity"),
            ("rating", "rating"),
            ("is_recommended", "recommended"),
        )
    )

    def rate_filter(self, qs, name, value):
        if value == 1:
            return qs.filter(hourly_rate__lte=30)
        elif value == 2:
            return qs.filter(Q(hourly_rate__gte=31) & Q(hourly_rate__lte=100))
        elif value == 3:
            return qs.filter(hourly_rate__gte=101)

        return qs.none()


class ProjectFilterSet(ServiceBaseFilterSet):
    budget_range_type = django_filters.NumberFilter(method="budget_filter")

    order_by = CustomOrderingFilter(
        fields=(("created_at", "date"), ("views", "popularity"), ("rating", "rating"),)
    )

    def budget_filter(self, queryset, name, value):
        if value == 1:
            return queryset.filter(budget__lte=10000)
        elif value == 2:
            return queryset.filter(Q(budget__gte=10001) & Q(budget__lte=50000))
        elif value == 3:
            return queryset.filter(Q(budget__gte=50001) & Q(budget__lte=150000))
        elif value == 4:
            return queryset.filter(budget__gte=150001)

        return queryset.none()
