import graphene
from core.utils import get_media_absolute_url
from graphene_django import DjangoConnectionField
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required

from .filters import (
    CategoryFilterSet,
    CoreSkillFilterSet,
    IndustryFilterSet,
    IntellectualServiceFilterSet,
    ProjectFilterSet,
)
from .models import (
    Category,
    Comment,
    CoreSkill,
    Industry,
    IntellectualService,
    Issue,
    IssueAttachment,
    IssueGroup,
    Project,
)


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class IndustryType(DjangoObjectType):
    class Meta:
        model = Industry
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class CoreSkillType(DjangoObjectType):
    class Meta:
        model = CoreSkill
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class ServiceFields:
    video = graphene.String()
    tag_list = graphene.String()
    logo = graphene.String()
    document = graphene.String()
    language_list = graphene.String()
    core_skill_list = graphene.String()

    def resolve_video(self, info):
        return get_media_absolute_url(self.video)

    def resolve_logo(self, info):
        return get_media_absolute_url(self.logo)

    def resolve_document(self, info):
        return get_media_absolute_url(self.document)

    def resolve_industry_list(self, info):
        return ", ".join(self.core_skills.values_list("industry__name", flat=True))

    def resolve_tag_list(self, info):
        return ", ".join(self.tags.all().values_list("name", flat=True))

    def resolve_language_list(self, info):
        return ", ".join(self.languages.all().values_list("name", flat=True))

    def resolve_core_skill_list(self, info):
        return ", ".join(self.core_skills.all().values_list("name", flat=True))


class IntellectualServiceType(ServiceFields, DjangoObjectType):
    class Meta:
        model = IntellectualService
        interfaces = (graphene.relay.Node,)


class ProjectType(ServiceFields, DjangoObjectType):
    management_option = graphene.Int()
    status = graphene.Int()
    picture = graphene.String()
    document = graphene.String()
    video = graphene.String()
    logo = graphene.String()

    def resolve_company(self, info):
        return self.company

    def resolve_members(self, info):
        return self.members.all()

    def resolve_picture(self, info):
        return get_media_absolute_url(self.picture)

    def resolve_document(self, info):
        return get_media_absolute_url(self.document)

    def resolve_video(self, info):
        return get_media_absolute_url(self.video)

    class Meta:
        model = Project
        interfaces = (graphene.relay.Node,)


class IssueGroupType(DjangoObjectType):
    project = graphene.Field(ProjectType)

    class Meta:
        model = IssueGroup
        interfaces = (graphene.relay.Node,)

    @classmethod
    @login_required
    def get_node(cls, info, id):
        return super().get_node(info, id)


class IssueType(DjangoObjectType):
    service = graphene.Field(IntellectualServiceType)
    group = graphene.Field(IssueGroupType)

    class Meta:
        model = Issue
        interfaces = (graphene.relay.Node,)

    @classmethod
    @login_required
    def get_node(cls, info, id):
        return super().get_node(info, id)


# class CommentType(DjangoObjectType):
#     owner = graphene.Field(UserType)

#     class Meta:
#         model = Comment
#         interfaces = (graphene.relay.Node,)


class IssueAttachmentType(DjangoObjectType):
    file = graphene.String()
    issue = graphene.Field(IssueType)

    def resolve_file(self, info):
        return get_media_absolute_url(self.file)

    class Meta:
        model = IssueAttachment
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    issue_lists = DjangoConnectionField(IssueGroupType)
    issues = DjangoConnectionField(IssueType)
    # all_comments = DjangoConnectionField(CommentType)
    issue_attachments = DjangoConnectionField(IssueAttachmentType)
    issue_list = graphene.relay.Node.Field(IssueGroupType)
    issue = graphene.relay.Node.Field(IssueType)
    # comment = graphene.relay.Node.Field(CommentType)
    issue_attachment = graphene.relay.Node.Field(IssueAttachmentType)

    # intellectual service categories, industries, core skills
    projects = DjangoFilterConnectionField(
        ProjectType, filterset_class=ProjectFilterSet
    )
    project = graphene.relay.Node.Field(ProjectType)
    intellectual_services = DjangoFilterConnectionField(
        IntellectualServiceType, filterset_class=IntellectualServiceFilterSet
    )
    intellectual_service = graphene.relay.Node.Field(IntellectualServiceType)
    intellectual_categories = DjangoFilterConnectionField(
        CategoryType, filterset_class=CategoryFilterSet
    )
    intellectual_industries = DjangoFilterConnectionField(
        IndustryType, filterset_class=IndustryFilterSet
    )
    intellectual_core_skills = DjangoFilterConnectionField(
        CoreSkillType, filterset_class=CoreSkillFilterSet
    )

    @login_required
    def resolve_issue_lists(self, info, **kwargs):
        return IssueGroup.objects.all()

    @login_required
    def resolve_issues(self, info, **kwargs):
        return Issue.objects.all()

    @login_required
    def resolve_comments(self, info, **kwargs):
        return Comment.objects.all()

    @login_required
    def resolve_issue_attachments(self, info, **kwargs):
        return IssueAttachment.objects.all()
