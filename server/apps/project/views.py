from accounts.serializers import UserSerializer
from company.models import Company, Ownership, Review
from company.serializers import CompanySerializer, WorkerSerializer
from company.utils import is_customer
from core.decorators import profile_completed_required, role_required
from core.mixins import VueTemplateMixin
from core.models import NOT_ACTIVE, IntellectualServicesSettings
from core.serializers import IntellectualServicesSettingsSerializer, NameSlugSerializer
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from project.serializers import IndustrySerializer
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from .filters import CoreSkillFilterSet, IntellectualServiceFilterSet, ProjectFilterSet
from .models import Category, CoreSkill, Industry, IntellectualService, Project
from .pagination import ServicesPagination
from .serializers import (
    ProjectSerializer,
    ReviewSerializer,
    ServiceSerializer,
    PartialServiceSerializer,
)


class IntellectualServicesMixin(VueTemplateMixin):
    projects_queryset = Project.objects.filter(status=NOT_ACTIVE)

    def get_projects(self) -> tuple:
        category_slug = self.kwargs.get("category")
        industry_slug = self.kwargs.get("industry")
        core_skill_slug = self.kwargs.get("core_skill")

        projects = self.projects_queryset.filter(category__slug=category_slug)

        # PMs can see platform projects only
        if self.request.user.is_pm:
            projects = projects.filter(management_option=Project.PLATFORM)
        else:
            projects = projects.filter(management_option=Project.DIRECT)

        if industry_slug:
            projects = projects.filter(industry__slug=industry_slug)

        if core_skill_slug:
            projects = projects.filter(core_skills__slug=core_skill_slug)

        serializer = ProjectSerializer(
            ProjectFilterSet(self.request.GET, projects).qs[:5], many=True
        )

        has_more = (
            ProjectFilterSet(
                self.request.GET,
                Project.objects.filter(
                    category__slug=category_slug,
                    industry__slug=industry_slug,
                    core_skills__slug=core_skill_slug,
                ),
            ).qs.count()
            > 5
        )

        return serializer.data, has_more

    def get_services(self) -> tuple:
        category_slug = self.kwargs.get("category")
        industry_slug = self.kwargs.get("industry")
        core_skill_slug = self.kwargs.get("core_skill")
        # Will not display PM services
        service_filter = Q(category__slug=category_slug) & ~Q(company__name="ENGRE")

        if industry_slug:
            service_filter &= Q(industry__slug=industry_slug)

        if core_skill_slug:
            service_filter &= Q(core_skills__slug=core_skill_slug)

        services = IntellectualService.objects.filter(service_filter)

        is_set = IntellectualServiceFilterSet(self.request.GET, services)
        serializer = ServiceSerializer(is_set.qs[:5], many=True)
        has_more = is_set.qs.count() > 5

        return serializer.data, has_more

    def get_props(self, is_anonymous: bool = False) -> dict:
        core_skills = []
        ownerships = NameSlugSerializer(Ownership.objects.all(), many=True).data
        settings = IntellectualServicesSettingsSerializer(
            IntellectualServicesSettings.objects.get()
        ).data

        if is_anonymous:
            user_data = None
        else:
            user_data = UserSerializer(self.request.user).data

        core_skills = NameSlugSerializer(CoreSkill.objects.all(), many=True).data

        return {
            "categories": NameSlugSerializer(Category.objects.all(), many=True).data,
            "industries": NameSlugSerializer(Industry.objects.all(), many=True).data,
            "coreSkills": core_skills,
            "ownerships": ownerships,
            "user": user_data,
            "settings": settings,
        }


# @method_decorator(login_required, name="dispatch")
# @method_decorator(profile_completed_required, name="dispatch")
# @method_decorator(role_required, name="dispatch")
class ProjectView(IntellectualServicesMixin, TemplateView):
    template_name = "project/intellectual_services.html"

    def get_props(self):
        user = self.request.user

        if not user.is_authenticated:
            return self.get_partial_props()

        props = super().get_props()

        if is_customer(user):
            props["items"], props["hasMore"] = self.get_services()
            props["showServices"] = True
        else:
            props["items"], props["hasMore"] = self.get_projects()
            props["showProjects"] = True

        return props

    def get_partial_props(self) -> dict:
        props = super().get_props(is_anonymous=True)
        props["items"] = self.get_partial_services()
        props["showServices"] = True
        return props

    def get_partial_services(self):
        category_slug = self.kwargs.get("category")
        industry_slug = self.kwargs.get("industry")
        core_skill_slug = self.kwargs.get("core_skill")
        service_filter = Q(category__slug=category_slug)

        if industry_slug:
            service_filter &= Q(industry__slug=industry_slug)

        if core_skill_slug:
            service_filter &= Q(core_skills__slug=core_skill_slug)

        services = IntellectualService.objects.filter(service_filter).order_by(
            "-is_priority"
        )

        is_set = IntellectualServiceFilterSet(self.request.GET, services)
        serializer = PartialServiceSerializer(is_set.qs[:5], many=True)

        return serializer.data


@method_decorator(login_required, name="dispatch")
class ProjectsPMView(IntellectualServicesMixin, TemplateView):
    template_name = "project/intellectual_services.html"

    def get_props(self):
        props = super().get_props()

        props["items"], props["hasMore"] = self.get_projects()
        props["showProjects"] = True

        return props


@method_decorator(login_required, name="dispatch")
class ServicesPMView(IntellectualServicesMixin, TemplateView):
    template_name = "project/intellectual_services.html"

    def get_props(self):
        props = super().get_props()

        props["items"], props["hasMore"] = self.get_services()
        props["showServices"] = True

        return props


class ServiceListAPIView(generics.ListAPIView):
    queryset = IntellectualService.objects.filter(~Q(company__name="ENGRE"))
    serializer_class = ServiceSerializer
    filterset_class = IntellectualServiceFilterSet
    pagination_class = ServicesPagination
    permission_classes = (IsAuthenticated,)


class ProjectListAPIView(generics.ListAPIView):
    queryset = Project.objects.filter(status=NOT_ACTIVE)
    serializer_class = ProjectSerializer
    filterset_class = ProjectFilterSet
    pagination_class = ServicesPagination
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if self.request.user.is_pm:
            return Project.objects.filter(
                management_option=Project.PLATFORM, status=NOT_ACTIVE
            )
        return Project.objects.filter(
            management_option=Project.DIRECT, status=NOT_ACTIVE
        )


class CoreSkillListAPIView(generics.ListAPIView):
    queryset = CoreSkill.objects.all()
    serializer_class = NameSlugSerializer
    filterset_class = CoreSkillFilterSet
    # permission_classes = (IsAuthenticated,)
    # pagination_class = CoreSkillPagination


@method_decorator(login_required, name="dispatch")
class ServiceDetailView(VueTemplateMixin, DetailView):
    template_name = "project/service.html"
    model = IntellectualService

    def get_object(self):
        return IntellectualService.objects.get(
            slug=self.kwargs.get("slug"), company__slug=self.kwargs.get("company")
        )

    def get_props(self):
        self.object.views += 1
        self.object.save()

        return {
            "item": ServiceSerializer(self.object).data,
            "url": self.object.get_absolute_url(),
            "title": self.object.name,
            "description": self.object.description,
            "user": UserSerializer(self.request.user).data,
            "workers": WorkerSerializer(
                self.object.company.workers.all(), many=True
            ).data,
        }


@method_decorator(login_required, name="dispatch")
class ProjectDetailView(VueTemplateMixin, DetailView):
    template_name = "project/project.html"
    model = Project

    def get_object(self):
        return Project.objects.get(
            slug=self.kwargs.get("slug"), company__slug=self.kwargs.get("company")
        )

    def get_props(self):
        self.object.views += 1
        self.object.save()

        return {
            "item": ProjectSerializer(self.object).data,
            "url": self.object.get_absolute_url(),
            "title": self.object.name,
            "description": self.object.description,
            "user": UserSerializer(self.request.user).data,
            "workers": WorkerSerializer(
                self.object.company.workers.all(), many=True
            ).data,
        }


@method_decorator(login_required, name="dispatch")
class CompanyDetailView(VueTemplateMixin, DetailView):
    template_name = "project/company.html"
    model = Company

    def get_props(self):
        return {
            "item": CompanySerializer(self.object).data,
            "url": self.object.get_absolute_url(),
            "title": self.object.name,
            "description": self.object.description,
            "user": UserSerializer(self.request.user).data,
            "workers": WorkerSerializer(self.object.workers.all(), many=True).data,
        }


class ReviewListAPIView(generics.ListAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (IsAuthenticated,)
    filterset_fields = ("company", "project")


class IndustryListAPIView(generics.ListAPIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    permission_classes = (IsAuthenticated,)
