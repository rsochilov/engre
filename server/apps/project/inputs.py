import graphene
from core.inputs import FileInput


class WorkInput:
    name = graphene.String(required=True)
    description = graphene.String(required=True)
    company = graphene.ID(required=True)
    category = graphene.ID(required=True)
    core_skills = graphene.List(graphene.ID, required=True)
    languages = graphene.List(graphene.ID, required=True)
    tags = graphene.List(graphene.String, required=True)
    logo = graphene.InputField(FileInput)
    document = graphene.InputField(FileInput)
    video = graphene.InputField(FileInput)
    status = graphene.Int()


class IntellectualServiceInput(WorkInput, graphene.InputObjectType):
    hourly_rate = graphene.Float(required=True)


class ProjectInput(WorkInput, graphene.InputObjectType):
    budget = graphene.Float(required=True)
    date_start = graphene.Date(required=True)
    date_finish = graphene.Date(required=True)
    service = graphene.ID()
    pm = graphene.ID()
    management_option = graphene.Int()


class IssueGroupInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    project = graphene.ID(required=True)
    is_archived = graphene.Boolean()


class IssueInput(graphene.InputObjectType):
    group = graphene.ID(required=True)
    name = graphene.String(required=True)
    description = graphene.String()
    due_date = graphene.Date()
    assigned_users = graphene.List(graphene.ID)


class CommentInput(graphene.InputObjectType):
    text = graphene.String(required=True)
    user = graphene.ID(required=True)
    issue = graphene.ID(required=True)


class IssueAttachmentInput(graphene.InputObjectType):
    file = graphene.InputField(FileInput)
    issue = graphene.ID(required=True)
