from os import path
from django.utils.text import slugify
from django.core.management.base import BaseCommand
from loguru import logger

from project.models import Industry


class Command(BaseCommand):
    help = "Updates slugs for industries from file"

    def add_arguments(self, parser):
        parser.add_argument("file_path", nargs="+", type=str)

    def handle(self, *args, **options):
        file_path = options["file_path"][0]
        if not path.isfile(file_path):
            logger.critical(f'File not found "{file_path}"')
            exit(-1)

        with open(file_path, "r") as file:
            industry_name = file.readline().strip()
            while industry_name:
                new_slug = file.readline().strip().replace("/", "")
                try:
                    industry = Industry.objects.get(name=industry_name)
                    industry.slug = new_slug
                    industry.save(update_fields=["slug"])
                    logger.success(f'"{industry_name}" updated!')
                except Industry.DoesNotExist:
                    logger.error(f"Industry {industry_name} doesn't exist")
                file.readline()
                industry_name = file.readline().strip()
