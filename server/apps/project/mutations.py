import graphene
from core.schema import UserType
from core.types import Mutation
from core.utils import retrieve_object
from graphql import GraphQLError
from graphql_jwt.decorators import login_required
from project.models import (
    Comment,
    IntellectualService,
    Issue,
    IssueAttachment,
    IssueGroup,
    Project,
)

from .inputs import (
    IntellectualServiceInput,
    IssueAttachmentInput,
    IssueGroupInput,
    IssueInput,
    ProjectInput,
)
from .schema import (
    IntellectualServiceType,
    IssueAttachmentType,
    IssueGroupType,
    IssueType,
    ProjectType,
)


class IntServiceCreateMutation(Mutation):
    class Arguments:
        service_data = IntellectualServiceInput(required=True)

    service = graphene.Field(IntellectualServiceType)
    user = graphene.Field(UserType)

    @login_required
    def mutate(self, info, service_data):
        service = IntellectualService.objects.super_create(**service_data)

        return IntServiceCreateMutation(service=service, user=info.context.user)


class IntServiceUpdateMutation(Mutation):
    class Arguments:
        service_id = graphene.ID(required=True)
        service_data = IntellectualServiceInput()

    service = graphene.Field(IntellectualServiceType)
    user = graphene.Field(UserType)

    @login_required
    def mutate(self, info, service_id, service_data):
        service = IntellectualService.objects.super_update(
            instance_id=service_id, **service_data
        )

        return IntServiceUpdateMutation(service=service, user=info.context.user)


class IntServiceDeleteMutation(Mutation):
    class Arguments:
        service_id = graphene.ID(required=True)

    success = graphene.Boolean()

    @login_required
    def mutate(self, info, service_id):
        service = retrieve_object(service_id, IntellectualService)
        service.delete()

        return IntServiceDeleteMutation(success=True)


class ProjectCreateMutation(Mutation):
    class Arguments:
        project_data = ProjectInput(required=True)

    project = graphene.Field(ProjectType)
    user = graphene.Field(UserType)

    @login_required
    def mutate(self, info, project_data):
        project = Project.objects.super_create(**project_data)

        return ProjectCreateMutation(project=project, user=info.context.user)


class ProjectUpdateMutation(Mutation):
    class Arguments:
        project_id = graphene.ID(required=True)
        project_data = ProjectInput(required=True)

    project = graphene.Field(ProjectType)
    user = graphene.Field(UserType)

    @login_required
    def mutate(self, info, project_id, project_data):
        project = retrieve_object(project_id, Project)

        if project.service or project.pm:
            raise GraphQLError("The project in progress. You cannot modify it!")

        if not project.service and not project.pm:
            project = Project.objects.super_update(instance=project, **project_data)

        return ProjectUpdateMutation(project=project, user=info.context.user)


class IssueGroupCreateMutation(Mutation):
    class Arguments:
        issue_group_data = IssueGroupInput()

    issue_group = graphene.Field(IssueGroupType)

    @login_required
    def mutate(self, info, issue_group_data):
        issue_group = IssueGroup.objects.super_create(**issue_group_data)

        return IssueGroupCreateMutation(issue_group=issue_group)


class IssueGroupUpdateMutation(Mutation):
    class Arguments:
        issue_group_id = graphene.ID(required=True)
        issue_group_data = IssueGroupInput()

    issue_group = graphene.Field(IssueGroupType)

    @login_required
    def mutate(self, info, issue_group_id, issue_group_data):
        issue_group = retrieve_object(issue_group_id, IssueGroup)
        issue_group = IssueGroup.objects.super_update(
            instance=issue_group, **issue_group_data
        )

        return IssueGroupUpdateMutation(issue_group=issue_group)


class IssueCreateMutation(Mutation):
    class Arguments:
        issue_data = IssueInput()

    issue = graphene.Field(IssueType)

    @login_required
    def mutate(self, info, issue_data):
        issue = Issue.objects.super_create(**issue_data)

        return IssueCreateMutation(issue=issue)


class IssueUpdateMutation(Mutation):
    class Arguments:
        issue_id = graphene.ID(required=True)
        issue_data = IssueInput()

    issue = graphene.Field(IssueType)

    @login_required
    def mutate(self, info, issue_id, issue_data):
        issue = retrieve_object(issue_id, Issue)
        issue = Issue.objects.super_update(instance=issue, **issue_data)
        return IssueUpdateMutation(issue=issue)


class IssueDeleteMutation(Mutation):
    class Arguments:
        issue_id = graphene.ID(required=True)

    success = graphene.Boolean()

    @login_required
    def mutate(self, info, issue_id):
        issue = retrieve_object(issue_id, Issue)
        issue.delete()

        return IssueDeleteMutation(success=True)


# class CommentCreateMutation(Mutation):
#     class Arguments:
#         comment_data = CommentInput()

#     comment = graphene.Field(CommentType)

#     @login_required
#     def mutate(self, info, comment_data):
#         comment = Comment.objects.super_create(**comment_data)

#         return CommentCreateMutation(comment=comment)


# class CommentUpdateMutation(Mutation):
#     class Arguments:
#         comment_id = graphene.ID(required=True)
#         text = graphene.String(required=True)

#     comment = graphene.Field(CommentType)

#     @login_required
#     def mutate(self, info, **kwargs):
#         comment = retrieve_object(kwargs.pop('comment_id'), Comment)
#         comment = Comment.objects.super_update(
#             instance=comment,
#             **kwargs
#         )

#         return CommentUpdateMutation(comment=comment)


class CommentDeleteMutation(Mutation):
    class Arguments:
        comment_id = graphene.ID(required=True)

    success = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        comment = retrieve_object(kwargs.pop("comment_id"), Comment)
        comment.delete()

        return CommentDeleteMutation(success=True)


class IssueAttachmentCreateMutation(Mutation):
    class Arguments:
        data = IssueAttachmentInput()

    attachment = graphene.Field(IssueAttachmentType)

    @login_required
    def mutate(self, info, data):
        attachment = IssueAttachment.objects.super_create(**data)

        return IssueAttachmentCreateMutation(attachment=attachment)


class IssueAttachmentDeleteMutation(Mutation):
    class Arguments:
        attachment_id = graphene.ID(required=True)

    success = graphene.Boolean()

    @login_required
    def mutate(self, info, attachment_id):
        attachment = retrieve_object(attachment_id, IssueAttachment)
        attachment.delete()

        return IssueAttachmentDeleteMutation(success=True)


class Mutation(graphene.ObjectType):
    create_intellectual_service = IntServiceCreateMutation.Field()
    update_intellectual_service = IntServiceUpdateMutation.Field()
    delete_intellectual_service = IntServiceDeleteMutation.Field()
    create_project = ProjectCreateMutation.Field()
    update_project = ProjectUpdateMutation.Field()
    create_issue_group = IssueGroupCreateMutation.Field()
    update_issue_group = IssueGroupUpdateMutation.Field()
    create_issue = IssueCreateMutation.Field()
    update_issue = IssueUpdateMutation.Field()
    delete_issue = IssueDeleteMutation.Field()
    # create_comment = CommentCreateMutation.Field()
    # update_comment = CommentUpdateMutation.Field()
    delete_comment = CommentDeleteMutation.Field()
    create_issue_attachment = IssueAttachmentCreateMutation.Field()
    delete_issue_attachment = IssueAttachmentDeleteMutation.Field()
