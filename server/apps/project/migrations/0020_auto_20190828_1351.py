# Generated by Django 2.1.4 on 2019-08-28 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0019_auto_20190828_1346"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="created_at",
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
