from django.contrib import admin
from rangefilter.filter import DateRangeFilter

from .models import Category, CoreSkill, Industry, IntellectualService, Project


@admin.register(Category)
class AchievementAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class ProjectAdmin(admin.ModelAdmin):
    list_filter = (("created_at", DateRangeFilter), "management_option")


class IntellServiceAdmin(admin.ModelAdmin):
    list_filter = ("category__name", "is_priority")
    list_display = ("name", "mini_description", "category_name")
    search_fields = (
        "name",
        "description",
    )
    ordering = ("name",)

    def mini_description(self, obj):
        return f"{obj.description[:60]}..."

    def category_name(self, obj):
        return obj.category.name


admin.site.register(Industry)
admin.site.register(IntellectualService, IntellServiceAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(CoreSkill)
