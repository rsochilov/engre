from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    url(
        r"services-api/",
        views.ServiceListAPIView.as_view(),
        name="intellectual-services-api",
    ),
    url(r"projects-api/", views.ProjectListAPIView.as_view(), name="projects-api"),
    url(
        r"core-skills-api/",
        views.CoreSkillListAPIView.as_view(),
        name="core-skills-api",
    ),
    url(r"reviews-api/", views.ReviewListAPIView.as_view(), name="reviews-api"),
    url(r"industry-api/", views.IndustryListAPIView.as_view(), name="industry-api"),
    url(
        r"service/(?P<company>[-a-zA-Z0-9_]+)/(?P<slug>[-a-zA-Z0-9_]+)/",
        views.ServiceDetailView.as_view(),
        name="service-details",
    ),
    url(
        r"project/(?P<company>[-a-zA-Z0-9_]+)/(?P<slug>[-a-zA-Z0-9_]+)/",
        views.ProjectDetailView.as_view(),
        name="project-details",
    ),
    path(
        "company/<int:pk>/",
        views.CompanyDetailView.as_view(),
        name="company_detail_by_id",
    ),
    url(
        r"company/(?P<slug>[-a-zA-Z0-9_]+)/",
        views.CompanyDetailView.as_view(),
        name="company-details",
    ),
    url(
        r"(?P<category>[-a-zA-Z0-9_]+)/(?P<industry>[-a-zA-Z0-9_]+)/(?P<core_skill>[-a-zA-Z0-9_]+)/",
        views.ProjectView.as_view(),
    ),
    url(
        r"(?P<category>[-a-zA-Z0-9_]+)/(?P<industry>[-a-zA-Z0-9_]+)/",
        views.ProjectView.as_view(),
    ),
    url(
        r"(?P<category>[-a-zA-Z0-9_]+)/",
        views.ProjectView.as_view(),
        name="project-list",
    ),
]
