from django.http import Http404
from accounts.serializers import UserSerializer
from core.mixins import VueTemplateMixin
from django.views.generic import TemplateView
from news.models import NewsCategory
from news.serializers import NewsCategorySerializer

from .models import LandingPage


class LandingView(VueTemplateMixin, TemplateView):
    template_name = "landing.html"

    def get_context_data(self, **kwargs):
        context = super(LandingView, self).get_context_data(**kwargs)
        context["landing"] = LandingPage.objects.get()

        return context

    def get_props(self):
        context = {"user": self.request.user}

        if self.request.user.is_authenticated:
            user = UserSerializer(self.request.user).data
        else:
            user = None

        return {
            "categories": NewsCategorySerializer(
                NewsCategory.objects.all(), many=True, context=context
            ).data,
            "user": user,
        }

    def get(self, request, *args, **kwargs):
        if len(self.request.GET):
            raise Http404()
        return super().get(request, *args, **kwargs)


def landing_error(request, path):
    raise Http404()
