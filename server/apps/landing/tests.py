from django.test import TestCase, Client
from django.shortcuts import reverse

headers = {"HTTP_USER_AGENT": "Mozilla/5.0"}


class RoutesTestCase(TestCase):
    def test_landing_with_upper_case(self):
        cl = Client(**headers)
        response = cl.get("/landinG/")
        self.assertEqual(response.status_code, 404)

    def test_landing_with_parameters(self):
        cl = Client(**headers)
        response = cl.get("/landing/?param1=113241")
        self.assertEqual(response.status_code, 404)

    def test_landing_without_slash(self):
        cl = Client(**headers)
        response = cl.get("/landing")
        self.assertEqual(response.status_code, 301)
