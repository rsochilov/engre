from django.db import models
from django.db.models import Q


class SearchFilter(models.Model):
    user = models.ForeignKey("accounts.User", on_delete=models.CASCADE)
    budget_rate = models.PositiveSmallIntegerField(null=True)
    name = models.CharField(max_length=100, null=False, default="unnamed_filter")
    coreskill = models.ForeignKey(
        "project.CoreSkill", on_delete=models.CASCADE, null=True
    )
    sorted_by = models.CharField(max_length=25, null=True)
    category = models.ForeignKey(
        "project.Category", on_delete=models.CASCADE, null=True
    )
    industry = models.ForeignKey(
        "project.Industry", on_delete=models.CASCADE, null=True
    )
    company_size = models.PositiveSmallIntegerField(null=True)
    working_hours = models.BooleanField(null=True)
    ownership = models.ForeignKey(
        "company.Ownership", on_delete=models.CASCADE, null=True
    )

    class Meta:
        unique_together = [["user", "name"]]

    def __str__(self):
        return self.name

    def get_projects_filter(self):
        projects_filter = self._get_default_filter()
        if self.budget_rate:
            return projects_filter & self._get_budget_filter()
        return projects_filter

    def get_services_filter(self):
        services_filter = self._get_default_filter()
        if self.budget_rate:
            return services_filter & self._get_rate_filter()
        return services_filter

    def _get_default_filter(self):
        projects_filter = models.Q()
        if self.coreskill:
            projects_filter &= Q(core_skills__id__exact=self.coreskill.pk)
        if self.category:
            projects_filter &= Q(category=self.category)
        if self.industry:
            projects_filter &= Q(industry=self.industry)
        if self.company_size:
            projects_filter &= Q(company__company_size=self.company_size)
        if self.working_hours:
            projects_filter &= Q(company__flexible_working_hours=self.working_hours)
        if self.ownership:
            projects_filter &= Q(company__ownership=self.ownership)
        return projects_filter

    def _get_budget_filter(self):
        if self.budget_rate == 1:
            return Q(budget__lte=10000)
        elif self.budget_rate == 2:
            return Q(budget__range=(10000, 50000))
        elif self.budget_rate == 3:
            return Q(budget__range=(50000, 150000))
        else:
            return Q(budget__gte=150000)

    def _get_rate_filter(self):
        if self.budget_rate == 1:
            return Q(hourly_rate__lt=30)
        elif self.budget_rate == 2:
            return Q(hourly_rate__range=(30, 100))
        else:
            return Q(hourly_rate__gte=100)
