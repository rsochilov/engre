from django.test import TestCase
from django.urls import reverse

from rest_framework.response import Response
from rest_framework.test import APIRequestFactory, force_authenticate

from accounts.models import User, ProjectManager
from company.models import Worker, Company
from . import views


class RoleApiTests(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.auth_user = User.objects.get(email__contains="kudrenko")
        cls.pm_id = ProjectManager.objects.first().id
        cls.contractor_id = (
            Worker.objects.filter(company__role=Company.CONTRACTOR).first().user.id
        )
        cls.customer_id = (
            Worker.objects.filter(company__role=Company.CUSTOMER).first().user.id
        )

    @classmethod
    def tearDownClass(cls):
        pass

    def test_valid_contractor(self):
        response = self.get_role_response_by_id(self.contractor_id)
        self.assertEqual(response.status_code, 200, response.data)
        self.assertEqual(response.data["role"], 1)

    def test_invalid_user(self):
        response = self.get_role_response_by_id(21131231231231)
        self.assertEqual(response.status_code, 404, response.data)

    def test_valid_customer(self):
        response = self.get_role_response_by_id(self.customer_id)
        self.assertEqual(response.status_code, 200, response.data)
        self.assertEqual(response.data["role"], 2)

    def test_valid_pm(self):
        response = self.get_role_response_by_id(self.pm_id)
        self.assertEqual(response.status_code, 200, response.data)
        self.assertEqual(response.data["role"], -1)

    def get_role_response_by_id(self, id: int) -> Response:
        factory = APIRequestFactory()
        request = factory.get(reverse("user_by_role", kwargs={"pk": id}))
        force_authenticate(request, user=self.auth_user)
        return views.GetUserRoleRetrieveView.as_view()(request, pk=id)
