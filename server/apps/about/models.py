from django.db import models


class Century(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class Activity(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Person(models.Model):
    name = models.CharField(max_length=100)
    century = models.ForeignKey(
        Century, related_name="person", on_delete=models.PROTECT
    )
    activity = models.ForeignKey(
        Activity, related_name="persons", on_delete=models.PROTECT
    )
    short_bio = models.TextField()
    bio = models.TextField()
    photo = models.ImageField(upload_to="about/people")
    year_of_birth = models.CharField(max_length=4)
    year_of_death = models.CharField(max_length=4, null=True, blank=True)

    def __str__(self):
        return self.name


class FieldOfActivity(models.Model):
    name = models.CharField(max_length=255)


class Slide(models.Model):
    class Meta:
        abstract = True

    title = models.CharField(max_length=150)
    year = models.PositiveSmallIntegerField()
    slug = models.SlugField(max_length=200, unique=True)
    image = models.ImageField(upload_to="about/%(class)s")
    description = models.TextField()

    def __str__(self):
        return self.title


class Achievement(Slide, models.Model):
    activity = models.ForeignKey(FieldOfActivity, on_delete=models.PROTECT)


class CommonProject(Slide, models.Model):
    industry = models.ForeignKey("project.Industry", on_delete=models.PROTECT)
