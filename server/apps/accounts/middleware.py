import datetime

from django.conf import settings
from django.core.cache import cache


def active_user_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        current_user = request.user
        if current_user.is_authenticated:
            now = datetime.datetime.now()
            cache.set(
                "seen_%s" % (current_user.id), now, settings.USER_LASTSEEN_TIMEOUT
            )

        response = get_response(request)

        return response

    return middleware
