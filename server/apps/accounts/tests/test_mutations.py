import pytest
from graphene.test import Client
from marketplace.schema import schema

from ..factories import UserFactory
from ..models import User


@pytest.mark.django_db
def test_user_sign_up(snapshot):
    client = Client(schema)
    obj = UserFactory.stub()

    executed = client.execute(
        """
        mutation($userData: RegisterInput!) {
            signUp(userData: $userData, userType: USER) {
                errors
                token
                user {
                    name
                    email
                }
            }
        }
    """,
        variables={
            "userData": {
                "name": obj.name,
                "email": obj.email,
                "password": "testpass123",
            }
        },
    )

    assert User.objects.filter(email=obj.email).exists()

    snapshot.assert_match(executed)
