# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["test_user_sign_up 1"] = {
    "data": {
        "signUp": {
            "errors": None,
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImJvYjBAZ21haWwuY29tIiwiZXhwIjoxNTQzMjQ4OTY2LCJvcmlnSWF0IjoxNTQzMjQ1MzY2fQ._xpADH5coETzOUFER2RElg0EVYmo2orWuTqsXQyT2IA",
            "user": {"email": "bob0@gmail.com", "name": "bob0"},
        }
    }
}
