from core.mixins import VueTemplateMixin
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import (
    PasswordResetConfirmView as BasePasswordResetConfirmView,
)
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django.views.generic.base import TemplateView
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Blogger, User
from .serializers import (
    BloggerSerializer,
    BloggerSubscribeSerializer,
    PasswordResetSerializer,
    UserSerializer,
)
from .tokens import account_activation_token, private_key_link_token


class BloggerSubscribeAPIView(UpdateAPIView):
    queryset = Blogger.objects.all()
    serializer_class = BloggerSubscribeSerializer
    permission_classes = (IsAuthenticated,)


class AuthTemplateView(VueTemplateMixin, TemplateView):
    template_name = "auth.html"

    def get_props(self):
        return {"referer": self.request.META.get("HTTP_REFERER")}


@api_view(["POST"])
def sign_in(request):
    email = request.data.get("email")
    password = request.data.get("password")
    user = authenticate(request, username=email, password=password)
    response = {}
    if user is not None:
        login(request, user)
        response["is_profile_completed"] = user.is_profile_completed
        response["success"] = True
        return Response(response)
    else:
        return Response({"success": False})


class SignUpView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SignUpBloggerView(generics.CreateAPIView):
    queryset = Blogger.objects.all()
    serializer_class = BloggerSerializer


class ResetPasswordAPIView(APIView):
    email_template_name = "registration/password_reset_email.html"
    from_email = None
    subject = "Engre password reset"
    success_url = reverse_lazy("password_reset_done")

    def post(self, request):
        serializer = PasswordResetSerializer(data=request.data)

        opts = {
            "use_https": self.request.is_secure(),
            "from_email": self.from_email,
            "email_template_name": self.email_template_name,
            "subject": self.subject,
            "request": self.request,
        }

        if serializer.is_valid():
            serializer.save(**opts)

        return Response(status=200)


class PasswordResetConfirmView(VueTemplateMixin, BasePasswordResetConfirmView):
    pass


class ActivateAccountView(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()

            return render(request, "registration/activate_success.html")
        else:
            return render(request, "registration/activate_failure.html")


class GetSecretKeyView(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and private_key_link_token.check_token(user, token):
            user.private_key_downloaded = True
            user.save()

            response = HttpResponse(user.get_public_key().export_key().decode())
            response["content_type"] = "application/x-pem-file"
            response["Content-Disposition"] = "attachment;filename=secret-key.pem"

            return response
        else:
            # invalid link
            raise Http404()
