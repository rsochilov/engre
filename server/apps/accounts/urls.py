from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    url(r"api-token-auth/", obtain_jwt_token),
    url(r"sign-in/", views.sign_in, name="sign-in"),
    url(
        r"subscribe-blogger/(?P<pk>[0-9]+)/",
        views.BloggerSubscribeAPIView.as_view(),
        name="subscribe-blogger",
    ),
    url(r"sign-up-blogger/", views.SignUpBloggerView.as_view(), name="sign-up-blogger"),
    url(r"sign-up/", views.SignUpView.as_view(), name="sign-up"),
    url(
        r"password-reset/", views.ResetPasswordAPIView.as_view(), name="password-reset"
    ),
    url(r"", views.AuthTemplateView.as_view(), name="auth"),
]
