from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from marketplace.celery import app


@app.task
def send_email(
    subject, email_body, emails, is_html=False, from_email=settings.DEFAULT_FROM_EMAIL,
):
    email = EmailMessage(subject, email_body, from_email, emails,)
    if is_html:
        email.content_subtype = "html"
    print(f"\n{email_body}\n")
    email.send()


def send_email_func(
    subject, email_body, emails, is_html=False, from_email=settings.DEFAULT_FROM_EMAIL,
):
    email = EmailMessage(subject, email_body, from_email, emails,)
    if is_html:
        email.content_subtype = "html"
    print(f"\n{email_body}\n")
    email.send()


@app.task
def send_support_email(subject: str, body: str):
    email = EmailMessage(
        subject, body, settings.DEFAULT_FROM_EMAIL, [settings.SUPPORT_EMAIL]
    )
    print(f"\n{body}\n")
    email.send()


@app.task
def send_secret_key_email(user_id: int, user_email: str, token: str):
    site = Site.objects.get_current()
    if settings.SITE_URL == "test.engre.co":
        domain = settings.SITE_URL
    else:
        domain = site.domain
    protocol = "http"

    context = {
        "uid": urlsafe_base64_encode(force_bytes(user_id)).decode(),
        "token": token,
        "protocol": protocol,
        "domain": domain,
    }

    email_body = render_to_string("accounts/secret-key-email.html", context)
    print(f"\n{email_body}\n")
    send_email.delay("Secret key download link", email_body, [user_email])
