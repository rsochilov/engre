from core.serializers import NameSlugSerializer, SubscribeSerializer
from core.timezones import new_timezones
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from market_location.models import City
from rest_framework import serializers
from rest_framework_jwt.serializers import jwt_encode_handler, jwt_payload_handler

from .models import Blogger, ContentManager, ProjectManager, User
from .tasks import send_email
from .tokens import account_activation_token


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ("id", "name_ascii")


class TimezoneSerializer(serializers.Serializer):
    tz_db = serializers.CharField()
    tz_display = serializers.CharField()


class ProjectManagerSerializer(serializers.ModelSerializer):
    industry_data = serializers.SerializerMethodField(read_only=True)
    timezone = serializers.SerializerMethodField()
    core_skills = serializers.ListField(write_only=True)

    class Meta:
        model = ProjectManager
        fields = (
            "id",
            "industry",
            "core_skills",
            "skills_level",
            "weekly_availability",
            "industry_data",
            "picture",
            "name",
            "location",
            "timezone",
        )

    def validate_core_skills(self, value):
        if value is None or type(value) != list or value == ["undefined"]:
            raise serializers.ValidationError("Invalid Core skills")
        return value

    def get_industry_data(self, obj):
        if obj.industry:
            return NameSlugSerializer(obj.industry).data

        return None

    def get_timezone(self, obj):
        if obj.timezone:
            for timezone in new_timezones:
                if timezone["tz_db"] == obj.timezone.zone:
                    return timezone

        return None

    def update(self, instance, validated_data):
        core_skills = validated_data.pop("core_skills")

        updated_instance = super().update(instance, validated_data)
        updated_instance.core_skills.clear()

        updated_instance.core_skills.add(
            *[int(skill) for skill in core_skills[0].split(",")]
        )
        updated_instance.save()
        return updated_instance


class BloggerDataSerializer(serializers.ModelSerializer):
    timezone = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField(read_only=True)
    blog_comments_count = serializers.SerializerMethodField(read_only=True)
    blog_articles_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Blogger
        fields = (
            "id",
            "name",
            "location",
            "timezone",
            "picture",
            "company",
            "biography",
            "rating",
            "blog_comments_count",
            "blog_articles_count",
        )

    def get_rating(self, obj):
        return obj.rating

    def get_blog_comments_count(self, obj):
        return obj.blog_comments.count()

    def get_blog_articles_count(self, obj):
        return obj.blog_articles.count()

    def get_timezone(self, obj):
        if obj.timezone:
            for timezone in new_timezones:
                if timezone["tz_db"] == obj.timezone.zone:
                    return timezone

        return None


class UserSerializer(serializers.ModelSerializer):
    location = LocationSerializer(required=False)
    timezone = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()
    company = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()
    is_pm = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)
    picture = serializers.FileField(required=False)
    pm = serializers.SerializerMethodField()
    is_owner = serializers.SerializerMethodField(read_only=True)
    last_seen = serializers.SerializerMethodField()
    online = serializers.SerializerMethodField()
    blogger = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "email",
            "timezone",
            "location",
            "role",
            "picture",
            "company",
            "token",
            "is_pm",
            "password",
            "pm",
            "is_owner",
            "is_blogger",
            "last_seen",
            "online",
            "blogger",
        )

    def get_is_owner(self, obj):
        return obj.is_owner

    def get_pm(self, obj):
        if obj.is_pm:
            return ProjectManagerSerializer(obj.projectmanager).data

        return None

    def get_blogger(self, obj):
        if obj.is_blogger:
            return BloggerDataSerializer(obj.blogger).data

        return None

    def get_timezone(self, obj):
        if obj.timezone:
            for timezone in new_timezones:
                if timezone["tz_db"] == obj.timezone.zone:
                    return timezone

        return None

    def get_role(self, obj):
        return obj.role

    def get_company(self, obj):
        company = obj.get_company()

        if company:
            from company.serializers import CompanySerializer

            return CompanySerializer(company).data

        return None

    def get_token(self, obj):
        payload = jwt_payload_handler(obj)

        return jwt_encode_handler(payload)

    def get_is_pm(self, obj):
        return obj.is_pm

    def get_last_seen(self, obj):
        if obj.last_seen():
            return obj.last_seen().isoformat()

        return None

    def get_online(self, obj):
        return obj.online()

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        password = validated_data.get("password")

        user.set_password(password)
        user.save()

        current_site = self.context.get("request").get_host()
        mail_subject = "Activate your Engre account."

        to_email = validated_data.get("email")
        message = render_to_string(
            "accounts/emails/confirmation_email.html",
            {
                "email": to_email,
                "domain": current_site,
                "uid": urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                "token": account_activation_token.make_token(user),
                "protocol": "https"
                if self.context.get("request").is_secure()
                else "http",
            },
        )

        send_email.delay(mail_subject, message, [to_email])

        return user


class UserShortSerializer(UserSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "email",
            "role",
            "picture",
            "company",
            "token",
            "is_pm",
            "pm",
            "is_owner",
            "timezone",
        )

    def get_company(self, obj):
        company = obj.get_company()
        from user_settings.serializers import CompanySerializer

        if company:
            return CompanySerializer(company).data
        return None


class BloggerSerializer(UserSerializer, serializers.ModelSerializer):
    rating = serializers.SerializerMethodField()
    is_subscribed = serializers.SerializerMethodField()
    blog_comments_count = serializers.SerializerMethodField()
    blog_articles_count = serializers.SerializerMethodField()

    def get_rating(self, obj):
        return obj.rating

    def get_is_subscribed(self, obj):
        request = self.context.get("request")
        user = request.user

        if user:
            return obj.subscribers.filter(pk=user.pk).exists()

        return False

    def get_blog_comments_count(self, obj):
        return obj.blog_comments.count()

    def get_blog_articles_count(self, obj):
        return obj.blog_articles.count()

    class Meta:
        model = Blogger
        fields = (
            "id",
            "name",
            "email",
            "timezone",
            "location",
            "role",
            "picture",
            "biography",
            "rating",
            "is_subscribed",
            "blog_comments_count",
            "blog_articles_count",
            "company",
            "online",
            "password",
        )


class BloggerSubscribeSerializer(
    SubscribeSerializer, BloggerSerializer, serializers.ModelSerializer
):
    pass


class ContentManagerSerializer(UserSerializer):
    rating = serializers.SerializerMethodField()
    news_comments_count = serializers.SerializerMethodField()
    news_articles_count = serializers.SerializerMethodField()

    class Meta:
        model = ContentManager
        fields = (
            "id",
            "name",
            "email",
            "timezone",
            "location",
            "role",
            "picture",
            "rating",
            "news_articles_count",
            "news_comments_count",
        )

    def get_rating(self, obj):
        return obj.rating

    def get_news_comments_count(self, obj):
        return obj.news_comments.count()

    def get_news_articles_count(self, obj):
        return obj.news_articles.count()


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def send_mail(
        self,
        subject_template_name,
        email_template_name,
        context,
        from_email,
        to_email,
        html_email_template_name=None,
    ):
        """
        Send a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = "".join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        if html_email_template_name is not None:
            html_email = loader.render_to_string(html_email_template_name, context)
            email_message.attach_alternative(html_email, "text/html")

        email_message.send()

    def get_users(self, email):
        active_users = User._default_manager.filter(
            **{"%s__iexact" % User.get_email_field_name(): email, "is_active": True,}
        )
        return (u for u in active_users if u.has_usable_password())

    def save(
        self,
        domain_override=None,
        subject=None,
        email_template_name="registration/password_reset_email.html",
        use_https=False,
        token_generator=default_token_generator,
        from_email=None,
        request=None,
    ):
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        email = self.validated_data["email"]

        for user in self.get_users(email):
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override

            site_name = domain = request.get_host()

            context = {
                "email": email,
                "domain": domain,
                "site_name": site_name,
                "uid": urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                "token": token_generator.make_token(user),
                "protocol": "https" if use_https else "http",
            }

            email_body = render_to_string(email_template_name, context)

            send_email.delay(
                subject, email_body, [email], from_email,
            )


class DeleteProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id",)

    def update(self, instance, validated_data):
        instance.is_active = False
        instance.save()

        return super(DeleteProfileSerializer, self).update(instance, validated_data)
