import graphene
from company.schema import WorkerType
from core.timezones import get_timezone
from core.utils import get_media_absolute_url
from django.db.models import Count
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from .models import Blogger, ContentManager, User
from .utils import get_user_subclass


class UserTypeChoice(graphene.Enum):
    USER = 1
    BLOGGER = 2


class UserCommonFields(graphene.ObjectType):
    user_type = UserTypeChoice()
    timezone = graphene.String()
    work = graphene.Field(WorkerType)
    picture = graphene.String()

    def resolve_user_type(self, info):
        user = get_user_subclass(self)

        if isinstance(user, Blogger):
            return UserTypeChoice.BLOGGER

        return UserTypeChoice.USER

    def resolve_timezone(self, info):
        if not self.timezone:
            return None

        if isinstance(self.timezone, str):
            return get_timezone(self.timezone)

        return get_timezone(self.timezone.zone)

    def resolve_picture(self, info):
        if self.picture:
            return get_media_absolute_url(self.picture)
        else:
            return "https://s3.us-east-2.amazonaws.com/engre-static/account-circle-outline.svg"


class UserType(DjangoObjectType, UserCommonFields):
    is_completed = graphene.Boolean()
    role = graphene.Int()

    def resolve_is_completed(self, info):
        if self.name and self.location and self.timezone:
            return True

        return False

    def resolve_role(self, info):
        if hasattr(self, "work"):
            return self.work.company.role

        return None

    class Meta:
        model = User
        filter_fields = {
            "email": ["exact"],
        }
        interfaces = (graphene.relay.Node,)


class BloggerType(DjangoObjectType, UserCommonFields):
    blogger_rating = graphene.String()
    blog_articles_count = graphene.Int()
    blog_comments_count = graphene.Int()

    def resolve_blogger_rating(self, info):
        rating = self.blog_articles.aggregate(rating=Count("interested_users")).get(
            "rating"
        )

        if rating > 0:
            return f"+{rating}"

        return str(rating)

    def resolve_blog_articles_count(self, info):
        return self.blog_articles.count()

    def resolve_blog_comments_count(self, info):
        return self.blog_comments.count()

    class Meta:
        model = Blogger
        filter_fields = {"email": ["exact"]}
        interfaces = (graphene.relay.Node,)


class ContentManagerType(DjangoObjectType):
    class Meta:
        model = ContentManager
        filter_fields = {"email": ["exact"]}
        interfaces = (graphene.relay.Node,)


class UserTypeUnion(graphene.Union):
    class Meta:
        types = (UserType, BloggerType, ContentManagerType)


class Query(graphene.ObjectType):
    users = DjangoFilterConnectionField(UserType)
    bloggers = DjangoFilterConnectionField(BloggerType)
    content_managers = DjangoFilterConnectionField(ContentManagerType)
    user = graphene.relay.Node.Field(UserType)
    me = graphene.Field(UserTypeUnion)

    @login_required
    def resolve_me(self, info, **kwargs):
        return get_user_subclass(info.context.user)
