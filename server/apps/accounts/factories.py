import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "accounts.User"
        django_get_or_create = ("email",)

    name = factory.Sequence(lambda n: "bob%s" % n)
    email = factory.Sequence(lambda n: "bob%s@gmail.com" % n)
    # password = factory.PostGenerationMethodCall('set_password', 'qweqweqwe')
