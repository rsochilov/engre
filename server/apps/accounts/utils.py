from .models import Blogger, ContentManager


def get_user_subclass(user):
    try:
        return user.blogger
    except Blogger.DoesNotExist:
        pass

    try:
        return user.contentmanager
    except ContentManager.DoesNotExist:
        pass

    return user
