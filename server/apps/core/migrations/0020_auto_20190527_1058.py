# Generated by Django 2.1.4 on 2019-05-27 10:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0019_auto_20190527_1056"),
    ]

    operations = [
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="has_company",
            field=models.BooleanField(default=False, verbose_name="With company"),
        ),
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="has_intellectual_services",
            field=models.BooleanField(
                default=False, verbose_name="With intellectual services"
            ),
        ),
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="is_authenticated",
            field=models.BooleanField(default=False, verbose_name="For authenticated"),
        ),
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="is_contractor",
            field=models.BooleanField(
                default=False, verbose_name="Visible for contractor"
            ),
        ),
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="is_customer",
            field=models.BooleanField(
                default=False, verbose_name="Visible for customer"
            ),
        ),
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="is_pm",
            field=models.BooleanField(default=False, verbose_name="Visible for PM"),
        ),
        migrations.AlterField(
            model_name="pagesettingsextension",
            name="is_profile_completed",
            field=models.BooleanField(
                default=False, verbose_name="With profile completed"
            ),
        ),
    ]
