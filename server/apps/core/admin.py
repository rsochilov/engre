from cms.extensions import PageExtensionAdmin
from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import (
    EducationEmail,
    IntellectualServicesSettings,
    Language,
    PageRenderingCondition,
    PageSettingsExtension,
    PartnershipEmail,
    StopWord,
    Tag,
)


class PageRenderingConditionInline(admin.TabularInline):
    model = PageRenderingCondition


class PageSettingsExtensionAdmin(PageExtensionAdmin):
    inlines = [PageRenderingConditionInline]


admin.site.register(Language)
admin.site.register(Tag)
admin.site.register(StopWord)
admin.site.register(PageSettingsExtension, PageSettingsExtensionAdmin)
admin.site.register(IntellectualServicesSettings, SingletonModelAdmin)
admin.site.register(EducationEmail, SingletonModelAdmin)
admin.site.register(PartnershipEmail, SingletonModelAdmin)
