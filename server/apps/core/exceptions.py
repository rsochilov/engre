class ComponentNotFoundException(Exception):
    """ vuejs component not found """

    pass


class CacherException(Exception):
    """
    Exception for .utils.Cacher 
    """

    pass
