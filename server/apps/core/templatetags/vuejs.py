import json
import os

import timeout_decorator
from core.exceptions import ComponentNotFoundException
from django import template
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.template import Context, Template
from django.utils.html import mark_safe

register = template.Library()


@register.simple_tag
@timeout_decorator.timeout(30, use_signals=False)
def render_component(component_name, el, data="", **kwargs):
    stats_path = os.path.join(os.path.dirname(settings.BASE_DIR), "stats.json")
    status = None
    info = None

    if settings.DEBUG:
        try:
            while status != "built":
                with open(stats_path) as f:
                    try:
                        stats = json.loads(f.read())
                    except json.decoder.JSONDecodeError:
                        continue

                    status = stats["status"]

                    info = next(
                        (
                            item
                            for item in stats["components"]
                            if item["component"] == component_name
                        ),
                        None,
                    )

            if info:
                component_path = os.path.join("vuecomponents", info["file"])
                component_abs_path = os.path.join(
                    settings.BASE_DIR, "static", component_path
                )

                while not os.path.isfile(component_abs_path):
                    pass

                with open(component_abs_path) as f:
                    script_content = f.read()
                    script_template = Template(script_content)

                    rendered_script = script_template.render(
                        context=Context({"el": el, "data": data})
                    )

                    return mark_safe(f"<script>{rendered_script}</script>")

        except FileNotFoundError:
            raise ComponentNotFoundException(f"{component_name} component not found")

    else:
        with open(stats_path) as f:
            stats = json.loads(f.read())

            info = next(
                (
                    item
                    for item in stats["components"]
                    if item["component"] == component_name
                ),
                None,
            )

        if info:
            component_path = os.path.join("vuecomponents", info["file"])
            component_abs_path = os.path.join(
                settings.BASE_DIR, "staticfiles", component_path
            )

            with open(component_abs_path) as f:
                script_content = f.read()
                script_template = Template(script_content)

                rendered_script = script_template.render(
                    context=Context({"el": el, "data": data})
                )

                return mark_safe(f"<script>{rendered_script}</script>")


@register.simple_tag
def load_vendor():
    stats_path = os.path.join(os.path.dirname(settings.BASE_DIR), "stats.json")

    with open(stats_path) as f:
        try:
            stats = json.loads(f.read())
        except json.decoder.JSONDecodeError:
            return ""

        vendor_path = os.path.join("vuecomponents", stats["vendor"])

    return mark_safe(f'<script src="{static(vendor_path)}"></script>')
