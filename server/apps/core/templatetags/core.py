from django import template
from django.urls import resolve

register = template.Library()


@register.filter(name="times")
def times(number):
    return range(int(number or 0))


@register.filter(name="url_name")
def get_url_name(request):
    return resolve(request.path_info).url_name
