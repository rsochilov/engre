from re import search
from django.conf import settings
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.request import Request

from .utils import upload_file
from accounts.tasks import send_email


@api_view(["POST"])
def temp_file_upload(request):
    file = request.FILES.get("file")

    if not file:
        return Response()

    path = upload_file("temp", file._name, file.file)

    return Response({"path": path})


@api_view(["POST"])
def phone_email_send(request: Request):
    phone_key = "phone"
    if phone_key not in request.data.keys():
        return Response({"detail": "No phone provided"}, status=400)

    number = request.data[phone_key].strip()
    if not _number_is_valid(number):
        return Response({"detail": "Invalid phone number"}, status=400)
        0
    subj = settings.PHONE_SEND_SUBJECT
    body = number
    emails = [
        settings.SUPPORT_EMAIL,
    ]
    send_email.delay(subj, body, emails)
    return Response({"ok": True})


def _number_is_valid(phone_number: str) -> bool:
    return search(r"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$", phone_number)
