import graphene
from accounts.models import User
from accounts.schema import UserType
from company.schema import CompanyPositionType
from core.utils import to_global_id
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from graphene_django import DjangoConnectionField
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required

from .models import Language, Tag
from .timezones import timezones
from .types import Mutation
from .utils import retrieve_object, send_mail


class RateableMixin:
    rating = graphene.Int()
    is_liked = graphene.Boolean()
    is_disliked = graphene.Boolean()

    def resolve_rating(self, info):
        return self.liked.count() - self.disliked.count()

    def resolve_is_liked(self, info):
        if info.context.user.is_authenticated:
            return self.liked.filter(id=info.context.user.id).exists()

        return False

    def resolve_is_disliked(self, info):
        if info.context.user.is_authenticated:
            return self.disliked.filter(id=info.context.user.id).exists()

        return False


class TagType(DjangoObjectType):
    class Meta:
        model = Tag
        filter_fields = {"name": ["icontains"]}
        interfaces = (graphene.relay.Node,)


class RefuseToken(Mutation):
    token = graphene.String()

    @login_required
    def mutate(self, info):
        info.context.user.profile.active_token = None
        info.context.user.profile.save()
        return RefuseToken()


class LanguageType(DjangoObjectType):
    class Meta:
        model = Language
        filter_fields = ("name",)
        interfaces = (graphene.relay.Node,)


class PasswordReset(Mutation):
    status = graphene.String()

    class Arguments:
        email = graphene.String(required=True)

    def mutate(self, info, email):
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            raise ValidationError("User with provided email doesn't exist")

        message = render_to_string(
            "reset_password/reset_password_email.html",
            {
                "user": user,
                "token": default_token_generator.make_token(user),
                "uid": force_str(urlsafe_base64_encode(force_bytes(user.pk))),
                "domain": info.context.META.get("HTTP_ORIGIN"),
            },
        )

        send_mail(
            settings.DEFAULT_FROM_EMAIL,
            "Password reset on Engre (test)",
            user.email,
            message,
        )

        return PasswordReset(status=_("Success"))


class ArchiveInstance(Mutation):
    instance_id = graphene.ID()

    class Arguments:
        instance_id = graphene.ID()
        model_name = graphene.String()

    @login_required
    def mutate(self, info, **kwargs):
        ct = ContentType.objects.get(model=kwargs.get("model_name"))
        model = ct.model_class()

        instance = retrieve_object(kwargs.get("instance_id"), model)

        instance.is_archived = not instance.is_archived
        instance.save()

        return ArchiveInstance(instance_id=to_global_id(instance))


class UpdateInstanceStatus(Mutation):
    instance_id = graphene.ID()
    status = graphene.Int()

    class Arguments:
        instance_id = graphene.ID()
        instance_status = graphene.Int()
        model_name = graphene.String()

    @login_required
    def mutate(self, info, **kwargs):
        ct = ContentType.objects.get(model=kwargs.get("model_name"))
        model = ct.model_class()

        instance = retrieve_object(kwargs.get("instance_id"), model)

        instance.status = kwargs.get("instance_status")
        instance.save()

        return UpdateInstanceStatus(
            instance_id=to_global_id(instance), status=instance.status
        )


class Query(graphene.ObjectType):
    timezones = graphene.List(graphene.String)
    tags = DjangoFilterConnectionField(TagType)
    company_positions = DjangoFilterConnectionField(CompanyPositionType)
    languages = DjangoConnectionField(LanguageType)
    language = graphene.relay.Node.Field(LanguageType)

    def resolve_languages(self, info):
        return Language.objects.all()

    def resolve_timezones(self, info, **kwargs):
        return timezones.keys()


class Mutation(graphene.ObjectType):
    password_reset = PasswordReset.Field()
    archive_instance = ArchiveInstance.Field()
    update_instance_status = UpdateInstanceStatus.Field()
