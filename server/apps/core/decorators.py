from functools import wraps

from django.core.exceptions import ValidationError
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse_lazy

from graphql_jwt.decorators import context
from rest_framework.response import Response


class RestFramework:
    """ login required decorator for rest views """

    @staticmethod
    def login_required(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if not request.rest_user.is_authenticated:
                return Response(status=401)
            return func(request, *args, **kwargs)

        return wrapper


def user_passes_active_token_test(test_func):
    def decorator(f):
        @wraps(f)
        @context(f)
        def wrapper(context, *args, **kwargs):
            if context.user.is_authenticated and test_func(context.user):
                return f(*args, **kwargs)
            return None

        return wrapper

    return decorator


def catch_errors(cls, func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ValidationError as e:
            return cls(errors=list(e))

    return wrapper


superuser_status_required = user_passes_active_token_test(lambda u: u.is_superuser)


def user_passes_test(test_func, redirect_url=None):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the redirect_url if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)

            if redirect_url:
                return redirect(redirect_url)

            raise Http404

        return _wrapped_view

    return decorator


def profile_completed_required(function=None):
    actual_decorator = user_passes_test(
        lambda u: u.is_profile_completed, redirect_url=reverse_lazy("profile")
    )

    if function:
        return actual_decorator(function)
    return actual_decorator


def not_pm(function=None):
    actual_decorator = user_passes_test(lambda u: not u.is_pm)

    if function:
        return actual_decorator(function)
    return actual_decorator


def owner_only(function=None):
    actual_decorator = user_passes_test(lambda u: u.get_company() and u.is_owner)

    if function:
        return actual_decorator(function)
    return actual_decorator


def role_required(function=None):
    """
    Decorator for views that checks that the user has a role, redirecting
    to the choose role page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.role, redirect_url=reverse_lazy("company")
    )

    if function:
        return actual_decorator(function)
    return actual_decorator


def status_required(status):
    def decorator(function):
        def check_status(user):
            if not hasattr(user, status):
                return False

            return getattr(user, status)

        actual_decorator = user_passes_test(check_status)

        if function:
            return actual_decorator(function)

        return actual_decorator

    return decorator


def one_of_statuses_required(statuses):
    def decorator(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            def check_status(status):
                if not hasattr(request.user, status):
                    return False

                return getattr(request.user, status)

            for status in statuses:
                if check_status(status):
                    return function(request, *args, **kwargs)

            raise Http404

        return wrapper

    return decorator
