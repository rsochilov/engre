import graphene
from graphene.types.field import Field
from graphene.types.utils import yank_fields_from_attrs

from .decorators import catch_errors


class Mutation(graphene.Mutation):
    """
        Class implemented for split Server and Validation errors.
        request:
        mutation {
            someMutation(args){
                errors
                someItem {
                    ...fields...
                }
            }
        }
        response:
        {
            data: {
                errors: [
                    ...errors...
                ],
                someItem: null
            }
        }
        All server errors will be return as is, in main block.
    """

    def mutate(self, info, **kwargs):
        pass

    @classmethod
    def Field(
        cls, name=None, description=None, deprecation_reason=None, required=False
    ):
        cls._meta.fields.update(
            yank_fields_from_attrs(
                {"errors": graphene.List(graphene.String)}, _as=Field
            )
        )
        _resolver = catch_errors(cls, cls._meta.resolver)
        return Field(
            cls._meta.output,
            args=cls._meta.arguments,
            resolver=_resolver,  # catch_errors decorator added in comparison of Super Class
            name=name,
            description=description,
            deprecation_reason=deprecation_reason,
            required=required,
        )
