from abc import abstractmethod
from typing import List

from django.conf import settings
from django.db.models import Model as DbModel
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand
from django.core.exceptions import MultipleObjectsReturned
from openpyxl import load_workbook
from loguru import logger
from core.utils import ArticleHtmlParser, SitemapArticleWriter
from marketplace.gdriveparser import Downloader
from news.models import NewsArticle
from blog.models import BlogArticle
from about.models import Achievement, CommonProject
from seo_tags.utils import TagDbInserter

from .add_classes import StatisticsCounters


class BaseLoaderCommand(BaseCommand):
    table_name = "table.xlsx"
    # table_id = '1PDZ2QiWOhtVhodlMrvRIac6mwu23nGAewXUYQIt47s8'
    table_id = "1vnbYK5uhu8SpemXa6sQ7cek03J-F5FOrwTTEHUDX-0c"
    _models_sitemap = [NewsArticle, Achievement, CommonProject, BlogArticle]

    @abstractmethod
    def check_instance_exists(self, row) -> DbModel:
        raise NotImplementedError()

    @abstractmethod
    def create_instance(self, row) -> DbModel:
        raise NotImplementedError()

    def create_seo_tags(self, data: dict, model: DbModel):
        if not data.get("seo_tags"):
            return
        TagDbInserter.insert_tags(data, model)

    def save_data(self, data: list, model: DbModel, image_row_name: str):
        """Saves data if it's not exists
        
        Arguments:
            data {list} -- list of dicts
            model {DbModel} -- model, that will be created
            image_row_name {str} -- row name in db model for image/picture storage
        """
        stats = StatisticsCounters()
        for row in data:
            instance = self.create_instance(row)
            try:
                existing_instance = self.check_instance_exists(instance)
                if existing_instance:
                    logger.info(f"Instance '{instance}...' already exists, skipping")
                    stats.skipped += 1
                    self.create_seo_tags(row, existing_instance)
                    self.insert_sitemap_info(existing_instance)
                    continue
            except MultipleObjectsReturned as err:
                logger.critical(err)
                stats.errors += 1
                continue

            instance.save()

            if row[image_row_name]:
                getattr(instance, image_row_name).save(
                    row[image_row_name].name, row[image_row_name].file
                )
            logger.success(f'Successfully uploaded "{row["title"]}" row')
            stats.added += 1
            self.create_seo_tags(row, instance)
            self.insert_sitemap_info(instance)
        logger.success(f"Data was successfully uploaded for {model.__name__}\n{stats}")

    def insert_sitemap_info(self, instance):
        if type(instance) not in self._models_sitemap:
            return
        full_url = f"https://{settings.SITE_URL}{instance.get_absolute_url()}"
        SitemapArticleWriter.insert_article_info(full_url, instance)

    def get_table(self) -> bytes:
        """Downloads table by id from google docs
        
        Returns:
            bytes -- table stored in bytes
        """
        downloader = Downloader()
        logger.info("Downloading the table...")

        table_bytes = downloader.download(
            self.table_id,
            mime_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            export=True,
        )
        logger.info("Successfully downloaded the table. Parser started")
        return table_bytes

    def handle_command(self, Parser, model: DbModel, image_row_name: str):
        SitemapArticleWriter.cache_file()
        """Main handler of command. Downloads table, 
        loads workbook and saves data to db
        
        Arguments:
            Parser {[type]} -- data parser (gdriveparser.py)
            model {DbModel} -- Django ORM model
            image_row_name {str} -- Db model row name for storing image/picture data
        """
        table_bytes = self.get_table()
        wb = load_workbook(table_bytes)
        parser = Parser(wb)
        try:
            data = parser.parse_data()
            self.save_data(data, model, image_row_name)
        except (TypeError, AttributeError, IntegrityError) as error:
            logger.exception(
                f"Unexpected error! Please, check document validity.\nError body:\n{type(error)}\n{error}"
            )

        if len(parser.errors):
            logger.info("During parsing errors occured:")
            for e in parser.errors:
                logger.critical(e)

        logger.info("Exiting")
