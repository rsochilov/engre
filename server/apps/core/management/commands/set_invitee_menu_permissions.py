from django.core.management.base import BaseCommand
from cms.models import Title
from loguru import logger

from core.models import PageRenderingCondition, PageSettingsExtension


class Command(BaseCommand):
    help = "Updates invited user permissions for all pages"

    # ids of pages, that must be hidden for invetee
    pages_ids = [26, 28, 70, 78, 22, 82, 92, 8, 30, 84, 94, 96]

    def handle(self, *args, **options):
        for id in self.pages_ids:
            try:
                page = Title.objects.get(pk=id)
                settings = PageSettingsExtension.objects.get(extended_object_id=id)
            except Title.DoesNotExist:
                logger.error(f"Page with id {id} doesn't exists")
                continue
            except PageSettingsExtension.DoesNotExist:
                continue
            _, is_created = PageRenderingCondition.objects.get_or_create(
                condition="is_native_user", strict=True, page_settings=settings
            )

            if is_created:
                logger.success(f'Permission created for page "{page.title}"')
            else:
                logger.info(f'Permission already exists for page "{page.title}"')
