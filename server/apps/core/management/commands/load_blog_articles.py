from django.db.models import Model as DbModel
from django.utils.text import slugify
from openpyxl import load_workbook

from accounts.models import Blogger
from blog.models import BlogArticle
from core.management.base import BaseLoaderCommand
from core.utils import ArticleHtmlParser

from marketplace.gdriveparser import BlogParser
from seo_tags.models import BlogArticleMetaTags


class Command(BaseLoaderCommand):
    help = "Loads blog articles to the DB"
    _metatag_model = BlogArticleMetaTags

    def create_instance(self, article) -> BlogArticle:
        blogger = Blogger.objects.get(pk=article["author_id"])

        return BlogArticle(
            title=article["title"],
            slug=slugify(article["title"]),
            excerpt=article["excerpt"],
            content=article["content"],
            category_id=article["category_id"],
            created_at=article["created_at"],
            author=blogger,
        )

    def check_instance_exists(self, instance: BlogArticle) -> bool:
        try:
            return BlogArticle.objects.get(slug=instance.slug)
        except BlogArticle.MultipleObjectsReturned:
            blogarticle_count = BlogArticle.objects.filter(slug=instance.slug).count()
            raise BlogArticle.MultipleObjectsReturned(
                f"Multiple BlogArticles got \
                with slug '{instance.slug}': {blogarticle_count}"
            )
        except BlogArticle.DoesNotExist:
            pass

    def handle(self, *args, **options):
        self.handle_command(BlogParser, BlogArticle, "picture")
