from cms.models import Page
from menus.base import Modifier
from menus.menu_pool import menu_pool

from .models import PageSettingsExtension


class MenuModifier(Modifier):
    """
    This modifier adds attributes of PageSettingsExtension
    to the node's attr dict
    """

    def modify(self, request, nodes, namespace, root_id, post_cut, breadcrumb):
        # only do something when the menu has already been cut
        if post_cut:
            # only consider nodes that refer to cms pages
            # and put them in a dict for efficient access
            page_nodes = {n.id: n for n in nodes if n.attr["is_page"]}
            # retrieve the attributes of interest from the relevant pages
            pages = Page.objects.filter(id__in=page_nodes.keys()).values(
                "id", "pagesettingsextension"
            )
            # loop over all relevant pages
            for page in pages:
                # take the node referring to the page
                node = page_nodes[page["id"]]
                if page["pagesettingsextension"]:
                    settings = PageSettingsExtension.objects.get(
                        id=page["pagesettingsextension"]
                    )
                    user = request.user

                    # strict conditions checks first
                    rendering_conditions = settings.rendering_conditions.all().order_by(
                        "-strict"
                    )
                    conditions_count = len(rendering_conditions)
                    current_condition_index = 0
                    at_least_one_condition_passed = False

                    for render_condition in rendering_conditions:
                        condition = render_condition.condition
                        try:
                            value = getattr(user, condition)
                        except AttributeError:
                            value = False

                        current_condition_index += 1

                        if not value and render_condition.strict:
                            nodes.remove(node)
                            break

                        if value and not render_condition.strict:
                            at_least_one_condition_passed = True
                            continue

                        elif (
                            not value
                            and current_condition_index + 1 > conditions_count
                            and not at_least_one_condition_passed
                        ):
                            nodes.remove(node)
                            break

        return nodes


menu_pool.register_modifier(MenuModifier)
