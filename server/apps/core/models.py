from accounts.models import User
from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool
from django.db import models
from django.utils.translation import gettext_lazy as _
from solo.models import SingletonModel

WAITING_FOR_APPROVAL, REFUSED, PUBLISHED = range(1, 4)
MODERATION_STATUSES = (
    (WAITING_FOR_APPROVAL, "waiting for approval"),
    (REFUSED, "refused"),
    (PUBLISHED, "published"),
)
ACTIVE, CLOSED, ARCHIVED_VISIBLE, ARCHIVED_INVISIBLE, NOT_ACTIVE = range(1, 6)
STATUSES = (
    (ACTIVE, "active"),
    (CLOSED, "closed"),
    (ARCHIVED_VISIBLE, "archived visible"),
    (ARCHIVED_INVISIBLE, "archived invisible"),
    (NOT_ACTIVE, "not active"),
)
DRAFT, PUBLISHED = range(1, 3)
ARTICLE_STATUSES = ((DRAFT, "draft"), (PUBLISHED, "published"))


class NameSlugModel(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    class Meta:
        abstract = True
        indexes = [models.Index(fields=["slug"])]

    def __str__(self):
        return self.name


class Tag(NameSlugModel):
    pass


class Language(NameSlugModel):
    pass


class ArchivableMixin(models.Model):
    is_archived = models.BooleanField(default=False)

    class Meta:
        abstract = True


class WatchableMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class OrderingMixin(models.Model):
    ordering = models.PositiveSmallIntegerField(null=True, blank=True)

    class Meta:
        abstract = True


class ModeratableMixin(models.Model):
    status = models.PositiveSmallIntegerField(
        choices=MODERATION_STATUSES, default=WAITING_FOR_APPROVAL
    )

    class Meta:
        abstract = True


class AbstractRateable(models.Model):
    rate = models.IntegerField(null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class AbstractArticle(ArchivableMixin, WatchableMixin):
    title = models.CharField(max_length=255)
    excerpt = models.TextField()
    content = models.TextField()
    video = models.FileField(upload_to="article_videos", blank=True, null=True)
    languages = models.ManyToManyField(
        Language, related_name="%(app_label)s_articles", blank=True
    )
    tags = models.ManyToManyField(
        Tag, related_name="%(app_label)s_articles", blank=True
    )
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=ACTIVE)

    class Meta:
        abstract = True
        ordering = ["-created_at"]

    def __str__(self):
        return self.title


class AbstractComment(WatchableMixin):
    comment = models.TextField()
    parent = models.ForeignKey(
        "self",
        related_name="reply_set",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        User, related_name="%(app_label)s_%(class)ss", on_delete=models.CASCADE
    )

    class Meta:
        abstract = True
        ordering = ["-created_at"]


class RateableMixin(models.Model):
    class Meta:
        abstract = True

    interested_users = models.ManyToManyField(
        User, blank=True, related_name="interested_%(app_label)s_%(class)ss"
    )


class LikeDislikeMixin(models.Model):
    class Meta:
        abstract = True

    liked = models.ManyToManyField(
        User, related_name="liked_%(app_label)s_%(class)ss", blank=True
    )
    disliked = models.ManyToManyField(
        User, related_name="disliked_%(app_label)s_%(class)ss", blank=True
    )

    def like(self, user_id):
        if self.disliked.filter(pk=user_id).exists():
            self.disliked.remove(user_id)
        else:
            self.liked.add(user_id)

    def dislike(self, user_id):
        if self.liked.filter(pk=user_id).exists():
            self.liked.remove(user_id)
        else:
            self.disliked.add(user_id)


class PageSettingsExtension(PageExtension):
    def copy_relations(self, oldinstance, language):
        """
        Copy relations like many to many or foreign key relations to the public version.
        Similar to the same named cms plugin function.
        :param oldinstance: the draft version of the extension
        """
        for rendering_condition in oldinstance.rendering_conditions.all():
            PageRenderingCondition.objects.create(
                condition=rendering_condition.condition,
                strict=rendering_condition.strict,
                page_settings=self,
            )


class PageRenderingCondition(models.Model):
    USER_PROPERTIES = (
        ("is_authenticated", "Visible for authenticated"),
        ("is_anonymous", "Visible for anonymous"),
        ("is_customer", "Visible for customer"),
        ("is_contractor", "Visible for contractor"),
        ("is_profile_completed", "Visible if profile completed"),
        ("has_intellectual_services", "Visible if has intellectual services"),
        ("is_pm", "Visible for PM"),
        ("is_owner", "Visible for company owner"),
        ("is_blogger", "Visible for blogger"),
        ("without_role", "Visible for users without role yet"),
        ("has_company", "Visible if has compeny"),
        ("is_anonymous", "Visible for anonymous"),
        ("is_native_user", "Visible for native users"),
    )  # yapf:disable

    condition = models.CharField(max_length=50, choices=USER_PROPERTIES)
    strict = models.BooleanField(default=False)
    page_settings = models.ForeignKey(
        PageSettingsExtension,
        on_delete=models.CASCADE,
        related_name="rendering_conditions",
    )

    def __str__(self):
        return self.get_condition_display()


extension_pool.register(PageSettingsExtension)


class IntellectualServicesSettings(SingletonModel):
    average_rate = models.PositiveIntegerField(default=0)
    projects_count = models.PositiveIntegerField(default=0)


class EducationEmail(SingletonModel):
    email = models.EmailField()


class PartnershipEmail(SingletonModel):
    email = models.EmailField()


class AbstractUserRequest(WatchableMixin, ArchivableMixin):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    company = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=50)
    text = models.TextField()

    class Meta:
        abstract = True


class CapitalLetter(WatchableMixin):
    letter = models.CharField(max_length=1, unique=True)

    def __str__(self):
        return self.letter


class StopWord(WatchableMixin):
    word = models.CharField(max_length=255)
    letter = models.ForeignKey(
        "core.CapitalLetter", on_delete=models.CASCADE, related_name="words"
    )

    def __str__(self):
        return self.word
