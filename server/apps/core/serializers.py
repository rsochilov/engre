from rest_framework import serializers

from .models import IntellectualServicesSettings


class NameSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()


class NameSlugSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    slug = serializers.SlugField()


class LikedDislikedSerializer(serializers.Serializer):
    is_liked = serializers.SerializerMethodField()
    is_disliked = serializers.SerializerMethodField()
    liked_count = serializers.SerializerMethodField()
    disliked_count = serializers.SerializerMethodField()

    def get_is_liked(self, obj):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            return obj.liked.filter(pk=user.pk).exists()

        return False

    def get_is_disliked(self, obj):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            return obj.disliked.filter(pk=user.pk).exists()

        return False

    def get_liked_count(self, obj):
        return obj.liked.count()

    def get_disliked_count(self, obj):
        return obj.disliked.count()


class LikeSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            instance.like(user.id)

        return instance


class DislikeSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            instance.dislike(user.id)

        return instance


class InterestSerializer(serializers.Serializer):
    interested_users_count = serializers.SerializerMethodField()

    def get_interested_users_count(self, obj):
        return obj.interested_users.count()

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            if instance.interested_users.filter(pk=user.pk).exists():
                instance.interested_users.remove(user)
            else:
                instance.interested_users.add(user)

        return super(InterestSerializer, self).update(instance, validated_data)


class SubscribeSerializer(serializers.Serializer):
    is_subscribed = serializers.SerializerMethodField()

    def get_is_subscribed(self, obj):
        request = self.context.get("request")
        user = request.user

        if user and obj.subscribers.filter(pk=user.pk).exists():
            return True

        return False

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user:
            if instance.subscribers.filter(pk=user.pk).exists():
                instance.subscribers.remove(user)
            else:
                instance.subscribers.add(user)

        return super(SubscribeSerializer, self).update(instance, validated_data)


class IntellectualServicesSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntellectualServicesSettings
        fields = "__all__"
