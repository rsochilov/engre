from django.conf import settings
from django.test import TestCase
from os import remove, path
from ..utils import SitemapArticleWriter
from news.models import NewsArticle


class SitemapArticleWriterTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        with open(settings.SITEMAP_FILE, "w") as f:
            f.write("<test_root><info>123</info></test_root>")
        cls.test_article = NewsArticle.objects.first()

    @classmethod
    def tearDownClass(cls):
        if path.exists(settings.SITEMAP_FILE):
            remove(settings.SITEMAP_FILE)

    def test_write_uri(self):
        test_uri = "engre.co/"
        SitemapArticleWriter.insert_article_info(test_uri, self.test_article)
        with open(settings.SITEMAP_FILE, "r") as f:
            text = "".join(f.readlines())
            self.assertTrue(SitemapArticleWriter.template.format(test_uri) in text)

    def test_write_uri_no_file(self):
        test_uri = "engre.co/"
        if path.exists(settings.SITEMAP_FILE):
            remove(settings.SITEMAP_FILE)
        try:
            SitemapArticleWriter.insert_article_info(test_uri, self.test_article)
            self.fail("File doesn't exist, but not exception")
        except FileNotFoundError:
            pass
        finally:
            with open(settings.SITEMAP_FILE, "w") as f:
                f.write("<test_root><info>123</info></test_root>")
