from django.contrib import admin
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url("django-admin/", admin.site.urls),
    url(r"^core/", include("core.urls")),
    url(r"", include("cms.urls")),
]
