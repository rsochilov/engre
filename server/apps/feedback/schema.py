import django_filters
import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from company.models import Company
from company.schema import CompanyType
from graphql_jwt.decorators import login_required
from core.utils import retrieve_object
from feedback.models import Platform, Executor
from feedback.tasks import send_feedback_action_email, send_feedback_creation_email


class PlatformFeedbackType(DjangoObjectType):
    company_customer = graphene.Field(CompanyType)

    class Meta:
        model = Platform
        filter_fields = {"company_customer__id": ["exact",], "status": ["exact",]}
        interfaces = (graphene.relay.Node,)


class PlatformFeedbackFilter(django_filters.FilterSet):
    class Meta:
        model = Platform
        fields = ["comment", "company_customer", "status"]

    @property
    def qs(self):
        if self.request.user.is_anonymous or not self.request.user.is_superuser:
            return super(PlatformFeedbackFilter, self).qs.filter(status="published")
        else:
            return super(PlatformFeedbackFilter, self).qs


class ExecutorFeedbackType(DjangoObjectType):
    company_customer = graphene.Field(CompanyType)
    company_executor = graphene.Field(CompanyType)

    class Meta:
        model = Executor
        filter_fields = {
            "company_executor__id": ["exact",],
            "company_executor": ["exact",],
        }
        interfaces = (graphene.relay.Node,)


class CreatePlatformFeedback(graphene.Mutation):
    platform_feedback = graphene.Field(PlatformFeedbackType)

    class Arguments:
        comment = graphene.String(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        _company_customer = info.context.user.company
        _feedback = Platform.objects.create(
            company_customer=_company_customer, comment=kwargs.get("comment")
        )
        send_feedback_creation_email.delay(user_id=info.context.user.id)
        return CreatePlatformFeedback(platform_feedback=_feedback)


class UpdatePlatformFeedback(graphene.Mutation):
    platform_feedback = graphene.Field(PlatformFeedbackType)

    class Arguments:
        platformFeedbackGlobId = graphene.String(required=True)
        comment = graphene.String(required=False)
        status = graphene.String(required=False)

    @login_required
    def mutate(self, info, **kwargs):
        _feedback = retrieve_object(kwargs.pop("platformFeedbackGlobId"), Platform)
        for attr in kwargs.keys():
            kwattr = kwargs.get(attr, None)
            if kwattr:
                _feedback.__setattr__(attr, kwattr)
        _feedback.save()
        send_feedback_action_email.delay(
            user_id=_feedback.company_customer.owner.id, action=kwargs.get("status")
        )
        return CreatePlatformFeedback(platform_feedback=_feedback)


class CreateExecutorFeedback(graphene.Mutation):
    executor_feedback = graphene.Field(ExecutorFeedbackType)

    class Arguments:
        companyExecutorGlobId = graphene.String(required=True)
        comment = graphene.String(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        _company_executor = retrieve_object(
            kwargs.pop("companyExecutorGlobId"), Company
        )
        company_customer = info.context.user.company
        _feedback = Executor.objects.create(
            company_executor=_company_executor,
            company_customer=company_customer,
            comment=kwargs.pop("comment"),
        )

        return CreateExecutorFeedback(executor_feedback=_feedback)


class UpdateExecutorFeedback(graphene.Mutation):
    executor_feedback = graphene.Field(ExecutorFeedbackType)

    class Arguments:
        executorFeedbackGlobId = graphene.String(required=True)
        comment = graphene.String(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        _feedback = retrieve_object(kwargs.pop("executorFeedbackGlobId"), Executor)
        _feedback.comment = kwargs.pop("comment")
        _feedback.save()
        return UpdateExecutorFeedback(executor_feedback=_feedback)


class Query(graphene.ObjectType):
    all_platform_feedbacks = DjangoFilterConnectionField(
        PlatformFeedbackType, filterset_class=PlatformFeedbackFilter
    )
    platform_feedback = graphene.relay.node.Field(PlatformFeedbackType)
    all_executor_feedbacks = DjangoFilterConnectionField(ExecutorFeedbackType)
    executor_feedback = graphene.relay.node.Field(ExecutorFeedbackType)


class Mutation(graphene.ObjectType):
    create_platform_feedback = CreatePlatformFeedback.Field()
    update_platform_feedback = UpdatePlatformFeedback.Field()
    create_executor_feedback = CreateExecutorFeedback.Field()
