from django.core.exceptions import ValidationError


def validate_code(value):
    if "." in value:
        raise ValidationError("The code cannot have dots")
