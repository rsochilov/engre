from core.models import WatchableMixin
from django.db import models

from .validators import validate_code


class Language(WatchableMixin):
    code = models.CharField(primary_key=True, max_length=2)
    name = models.CharField(max_length=255)


class Category(WatchableMixin):
    code = models.CharField(
        primary_key=True, max_length=255, validators=[validate_code],
    )
    name = models.CharField(max_length=255)


class Message(WatchableMixin):
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    code = models.CharField(max_length=255, validators=[validate_code])
    text = models.TextField()

    class Meta:
        unique_together = (("language", "category", "code"),)
