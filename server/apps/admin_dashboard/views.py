import json

from about.models import Activity, Century, Person
from about.serializers import PersonSerializer
from accounts.models import Blogger, ContentManager, ProjectManager, User
from accounts.serializers import (
    PasswordResetSerializer,
    TimezoneSerializer,
    UserSerializer,
)
from blog.filters import BlogArticleFilterSet
from blog.models import BlogArticle, BlogCategory
from blog.serializers import ArticleSerializer
from careers.models import Category as JobCategory
from careers.models import Job
from chat.models import ChatRoom, Message
from company.models import Document
from core.decorators import status_required
from core.mixins import VueTemplateMixin
from core.models import CapitalLetter, StopWord
from core.serializers import NameSerializer, NameSlugSerializer
from core.timezones import new_timezones
from core.utils import get_image_from_base64
from django.db.models import Q
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from market_location.models import City
from news.models import NewsArticle, NewsCategory
from partnership.models import ForInvestor, Software, SoftwareFile
from project.filters import ProjectFilterSet
from project.models import Category, Industry, Project
from project.serializers import ProjectSerializer
from rest_framework import generics, views
from rest_framework.response import Response
from user_settings.serializers import CitySerializer
from weasyprint import HTML

from .filters import UserManagementFilterSet
from .pagination import Admin25Pagination, AdminPagination, AdminProjectPagination
from .serializers import (
    CapitalLetterSerializer,
    DocumentSerializer,
    ForInvestorSerializer,
    JobSerializer,
    NewsArticleSerializer,
    SoftwareFileSerializer,
    SoftwareSerializer,
    UserManagementSerializer,
)


class UploadPictureAPIView(views.APIView):
    def get_object(self):
        return User.objects.get(pk=self.request.data.get("pk"))

    def post(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.picture = request.data.get("picture")
        instance.save()

        return Response(data=UserSerializer(instance).data, status=200)


@method_decorator(status_required("is_superuser"), name="dispatch")
class DashboardView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/dashboard.html"

    def get_props(self):

        return {"user": UserSerializer(self.request.user).data}


@method_decorator(status_required("is_superuser"), name="dispatch")
class ProjectsView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/projects.html"

    def get_props(self):

        return {
            "user": UserSerializer(self.request.user).data,
            "industries": NameSlugSerializer(Industry.objects.all(), many=True).data,
        }


class ProjectAPIView(generics.ListAPIView, generics.UpdateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    pagination_class = AdminProjectPagination
    filterset_class = ProjectFilterSet

    def get_object(self):
        return Project.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_archived = not instance.is_archived
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)


@method_decorator(status_required("is_superuser"), name="dispatch")
class SendWarningMessageAPIView(views.APIView):
    def post(self, request, *args, **kwargs):
        admin = request.user
        user = User.objects.get(pk=request.data.get("user_id"))
        chat_room = None
        message = """
            Some content that you are posting seems to contradict our terms and conditions.
            Our support team can save your project from being archived or deactivated. Just contact them.
        """

        try:
            chat_room = ChatRoom.objects.filter(users__in=[admin]).get(users__in=[user])
        except ChatRoom.DoesNotExist:
            chat_room = ChatRoom.objects.create(blocked=True)
            chat_room.users.set([user, admin])

        Message.objects.create(
            message=message, sender=admin, chat_room=chat_room, type=Message.SYSTEM
        )

        return Response(status=200)


@method_decorator(status_required("is_superuser"), name="dispatch")
class ContentView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/content.html"

    def get_props(self):

        return {
            "user": UserSerializer(self.request.user).data,
            "centuries": NameSerializer(Century.objects.all(), many=True).data,
            "activities": NameSerializer(Activity.objects.all(), many=True).data,
            "categories": NameSlugSerializer(
                BlogCategory.objects.all(), many=True
            ).data,
            "newsCategories": NameSerializer(
                NewsCategory.objects.all(), many=True
            ).data,
            "contentManagers": NameSerializer(
                ContentManager.objects.all(), many=True
            ).data,
            "jobCategories": NameSerializer(JobCategory.objects.all(), many=True).data,
            "industries": NameSlugSerializer(Industry.objects.all(), many=True).data,
        }


@method_decorator(status_required("is_superuser"), name="dispatch")
class PersonViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, generics.DestroyAPIView
):
    serializer_class = PersonSerializer

    def get_object(self):
        return Person.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        photo = request.data.get("photo")

        if photo:
            del request.data["photo"]

        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)
        instance.photo = get_image_from_base64(instance.name, photo)
        instance.save()

        return Response(data=serializer.data, status=201)

    def update(self, request, *args, **kwargs):
        photo = None

        if request.data.get("photo"):
            photo = request.data.get("photo")
            del request.data["photo"]

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)

        if photo:
            instance.photo = get_image_from_base64(instance.name, photo)
            instance.save()

        return Response(data=serializer.data, status=200)

    def perform_create(self, serializer):
        return serializer.save()


class ArticleAPIView(generics.ListAPIView):
    queryset = BlogArticle.objects.all()
    serializer_class = ArticleSerializer
    filterset_class = BlogArticleFilterSet
    pagination_class = AdminPagination


class NewsArticleAPIView(generics.ListAPIView):
    queryset = NewsArticle.objects.all()
    serializer_class = NewsArticleSerializer
    pagination_class = AdminPagination


class NewsArticleViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, generics.DestroyAPIView
):
    serializer_class = NewsArticleSerializer

    def get_object(self):
        return NewsArticle.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)

        # add photos

        return Response(data=self.get_serializer(instance).data, status=201)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)

        # add photos

        return Response(data=self.get_serializer(instance).data, status=200)

    def perform_create(self, serializer):
        return serializer.save()


class JobListAPIView(generics.ListAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    pagination_class = AdminPagination


class JobViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, generics.DestroyAPIView
):
    serializer_class = JobSerializer

    def get_object(self):
        return Job.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        image = request.data.get("image")
        del request.data["image"]

        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)
        instance.image = get_image_from_base64(instance.title, image)
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=201)

    def update(self, request, *args, **kwargs):
        image = None

        if request.data.get("image"):
            image = request.data.get("image")
            del request.data["image"]

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)

        if image:
            instance.image = get_image_from_base64(instance.title, image)
            instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)

    def perform_create(self, serializer):
        return serializer.save()


class ForInvestorViewSet(
    generics.CreateAPIView,
    generics.UpdateAPIView,
    generics.ListAPIView,
    generics.DestroyAPIView,
):
    queryset = ForInvestor.objects.all()
    serializer_class = ForInvestorSerializer
    pagination_class = AdminPagination

    def get_object(self):
        return ForInvestor.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        image = request.data.get("image")
        del request.data["image"]

        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)
        instance.image = get_image_from_base64(instance.name, image)
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=201)

    def update(self, request, *args, **kwargs):
        image = None

        if request.data.get("image"):
            image = request.data.get("image")
            del request.data["image"]

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)

        if image:
            instance.image = get_image_from_base64(instance.name, image)
            instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)

    def perform_create(self, serializer):
        return serializer.save()


@method_decorator(status_required("is_superuser"), name="dispatch")
class DocumentView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/documents.html"

    def get_props(self):
        user = self.request.user

        return {"user": UserSerializer(user).data}


class DocumentAPIView(generics.ListAPIView):
    queryset = [doc for doc in Document.objects.all() if doc.is_signed]
    serializer_class = DocumentSerializer
    pagination_class = Admin25Pagination


@method_decorator(status_required("is_superuser"), name="dispatch")
class GeneratePDF(views.APIView):
    def get(self, request, *args, **kwargs):
        document = Document.objects.get(pk=request.GET.get("pk"))
        context = {"data": document.body}
        html_string = render_to_string("admin_dashboard/pdf.html", context)
        html = HTML(string=html_string)
        pdf = html.write_pdf()
        response = HttpResponse(pdf, content_type="application/pdf")
        response["Content-Disposition"] = f'inline; filename="{document.name}.pdf"'

        return response


@method_decorator(status_required("is_superuser"), name="dispatch")
class SoftwareView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/software.html"

    def get_props(self):
        user = self.request.user

        return {
            "user": UserSerializer(user).data,
        }


class SoftwareAPIView(generics.ListAPIView):
    queryset = Software.objects.all()
    serializer_class = SoftwareSerializer
    pagination_class = Admin25Pagination


class SoftwareViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, generics.DestroyAPIView
):
    serializer_class = SoftwareSerializer

    def get_object(self):
        return Software.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        logo = request.data.get("logo")
        image = request.data.get("image")

        del request.data["logo"]
        del request.data["image"]

        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)

        instance.logo = get_image_from_base64(instance.name, logo)
        instance.image = get_image_from_base64(instance.name, image)
        instance.save()

        return Response(self.get_serializer(instance).data, status=201)

    def update(self, request, *args, **kwargs):
        logo, image = None, None

        if request.data.get("logo"):
            logo = request.data.get("logo")
            del request.data["logo"]

        if request.data.get("image"):
            image = request.data.get("image")
            del request.data["image"]

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)

        if logo:
            instance.logo = get_image_from_base64(instance.name, logo)

        if image:
            instance.image = get_image_from_base64(instance.name, image)

        instance.save()

        return Response(self.get_serializer(instance).data, status=200)

    def perform_create(self, serializer):
        return serializer.save()


class UploadSoftware(generics.CreateAPIView):
    serializer_class = SoftwareFileSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_create(serializer)

        return Response(data=serializer.data, status=201)


class SoftwareFileAPIView(generics.ListAPIView):
    serializer_class = SoftwareFileSerializer

    def get_queryset(self):
        return SoftwareFile.objects.filter(software_id=self.request.GET.get("pk"))


class FinancesView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/finances.html"


@method_decorator(status_required("is_superuser"), name="dispatch")
class UserManagementView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/user_management.html"

    def get_props(self):
        user = self.request.user

        return {"user": UserSerializer(user).data}


class UserManagementViewSet(
    generics.ListAPIView, generics.UpdateAPIView, generics.DestroyAPIView
):
    queryset = User.objects.all()
    serializer_class = UserManagementSerializer
    pagination_class = AdminPagination
    filterset_class = UserManagementFilterSet

    def get_object(self):
        return User.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_active = not instance.is_active
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)


@method_decorator(status_required("is_superuser"), name="dispatch")
class CreateUserAPIView(generics.CreateAPIView):
    email_template_name = "registration/password_reset_email.html"
    from_email = None
    subject = "Engre password reset"
    success_url = reverse_lazy("password_reset_done")

    def create(self, request, *args, **kwargs):
        type = request.data.pop("type")
        email = request.data.get("email")

        if type == "customer" or type == "contractor":
            User.objects.create(email=email, is_active=True)
        elif type == "pm":
            ProjectManager.objects.create(email=email, is_active=True)
        elif type == "blogger":
            Blogger.objects.create(email=email, is_active=True)
        elif type == "content manager":
            ContentManager.objects.create(email=email, is_active=True)

        serializer = PasswordResetSerializer(data=request.data)

        opts = {
            "use_https": self.request.is_secure(),
            "from_email": self.from_email,
            "email_template_name": self.email_template_name,
            "subject": self.subject,
            "request": self.request,
        }

        if serializer.is_valid():
            serializer.save(**opts)

        return Response(status=201)


class ModerationView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/moderation.html"


class ChatView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/chat.html"


@method_decorator(status_required("is_superuser"), name="dispatch")
class SettingsView(VueTemplateMixin, TemplateView):
    template_name = "admin_dashboard/settings.html"

    def get_props(self):
        user = self.request.user

        return {
            "user": UserSerializer(user).data,
            "timezones": TimezoneSerializer(new_timezones, many=True).data,
            "cities": CitySerializer(
                City.objects.filter(country__name="Ukraine"), many=True
            ).data,
        }


@method_decorator(status_required("is_superuser"), name="dispatch")
class SetAdminPassword(views.APIView):
    def post(self, request, *args, **kwargs):
        user = request.user

        if user.check_password(request.data.get("oldpass")):
            user.set_password(request.data.get("newpass"))
            return Response(status=200)

        return Response(status=401)


class CategoryViewSet(generics.ListAPIView, generics.UpdateAPIView):
    queryset = Category.objects.all()
    serializer_class = NameSlugSerializer

    def get_object(self):
        return Category.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.name = request.data.get("name")
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)


class IndustryViewSet(generics.ListAPIView, generics.UpdateAPIView):
    queryset = Industry.objects.all()
    serializer_class = NameSlugSerializer

    def get_object(self):
        return Industry.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.name = request.data.get("name")
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)


class CategoryOrderingAPIView(generics.UpdateAPIView):
    def update(self, request, *args, **kwargs):
        categories = request.data.get("categories")

        for index, category in enumerate(categories):
            Category.objects.filter(id=category.get("id")).update(ordering=index)

        return Response(status=200)


class IndustryOrderingAPIView(generics.UpdateAPIView):
    def update(self, request, *args, **kwargs):
        industries = request.data.get("industries")

        for index, industry in enumerate(industries):
            Industry.objects.filter(id=industry.get("id")).update(ordering=index)

        return Response(status=200)


class StopWordViewSet(generics.ListAPIView, generics.CreateAPIView):
    queryset = CapitalLetter.objects.all()
    serializer_class = CapitalLetterSerializer

    def create(self, request, *args, **kwargs):
        words = request.data.get("words")

        for word in words:
            letter = None
            try:
                letter = CapitalLetter.objects.get(letter=word[0].lower())
            except CapitalLetter.DoesNotExist:
                letter = CapitalLetter.objects.create(letter=word[0].lower())

            StopWord.objects.create(word=word.lower(), letter=letter)

        return Response(status=201)
