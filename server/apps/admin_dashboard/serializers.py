from accounts.models import User
from accounts.serializers import ContentManagerSerializer
from careers.models import Job
from company.models import Document
from company.serializers import WorkerSerializer
from core.models import CapitalLetter, StopWord
from core.serializers import NameSerializer
from news.models import NewsArticle
from partnership.models import ForInvestor, Software, SoftwareFile
from project.models import IntellectualService, Project
from project.serializers import ProjectSerializer
from rest_framework import serializers


class NewsArticleSerializer(serializers.ModelSerializer):
    category_id = serializers.IntegerField(write_only=True)
    category = NameSerializer(read_only=True)
    author_id = serializers.IntegerField(write_only=True)
    author = ContentManagerSerializer(read_only=True)
    images = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = NewsArticle
        fields = "__all__"

    def get_images(self, obj):
        return [preview.image.url for preview in obj.images.all()]


class JobSerializer(serializers.ModelSerializer):
    category = NameSerializer(read_only=True)
    category_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Job
        fields = "__all__"


class ForInvestorSerializer(serializers.ModelSerializer):
    industry = NameSerializer(read_only=True)
    industry_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = ForInvestor
        fields = "__all__"


class SoftwareSerializer(serializers.ModelSerializer):
    class Meta:
        model = Software
        fields = "__all__"


class DocumentSerializer(serializers.ModelSerializer):
    project = ProjectSerializer()

    class Meta:
        model = Document
        fields = "__all__"


class SoftwareFileSerializer(serializers.ModelSerializer):
    os_data = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    size = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = SoftwareFile
        fields = "__all__"

    def get_os_data(self, obj):
        return obj.get_os_display()

    def get_url(self, obj):
        return obj.file.url

    def get_size(self, obj):
        return obj.file.size


class IntellectualServiceSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    budget = serializers.SerializerMethodField()
    date_finish = serializers.CharField(required=False)

    def get_budget(self, obj):
        if obj.company.role == 1:
            return obj.hourly_rate
        elif obj.company.role == 2:
            return obj.budget

        return None


class UserManagementSerializer(serializers.ModelSerializer):
    work = WorkerSerializer()
    last_intellectual_service = serializers.SerializerMethodField()
    is_customer = serializers.SerializerMethodField()
    is_contractor = serializers.SerializerMethodField()
    is_pm = serializers.SerializerMethodField()
    is_content_manager = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "email",
            "work",
            "picture",
            "is_active",
            "last_intellectual_service",
            "is_customer",
            "is_contractor",
            "is_pm",
            "is_content_manager",
        )

    def get_last_intellectual_service(self, obj):
        if not obj.has_company:
            return None
        elif obj.role == 1:
            try:
                return IntellectualServiceSerializer(
                    obj.work.company.intellectualservices.filter(
                        is_archived=False
                    ).latest("updated_at")
                ).data
            except IntellectualService.DoesNotExist:
                return None
        elif obj.role == 2:
            try:
                return IntellectualServiceSerializer(
                    obj.work.company.projects.filter(is_archived=False).latest(
                        "updated_at"
                    )
                ).data
            except Project.DoesNotExist:
                return None

        return None

    def get_is_customer(self, obj):
        return obj.is_customer

    def get_is_contractor(self, obj):
        return obj.is_contractor

    def get_is_pm(self, obj):
        return obj.is_pm

    def get_is_content_manager(self, obj):
        return obj.is_content_manager


class StopWordSerializer(serializers.ModelSerializer):
    class Meta:
        model = StopWord
        fields = "__all__"


class CapitalLetterSerializer(serializers.ModelSerializer):
    words = StopWordSerializer(many=True)

    class Meta:
        model = CapitalLetter
        fields = (
            "id",
            "letter",
            "words",
        )
