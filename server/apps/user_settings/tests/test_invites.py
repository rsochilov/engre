from uuid import uuid4
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from django.shortcuts import reverse

from company.models import Worker
from core.utils import Cacher
from project.models import Project
from user_settings.views import create_new_invitee_view


class ProjectViewsTests(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.uid = str(uuid4())
        project = Project.objects.filter(company__isnull=False).first()
        inviter_id = Worker.objects.filter(company=project.company).first().user.id
        Cacher.add_invite_info(
            cls.uid, "test_email@mail.mail123", project.id, inviter_id
        )

    @classmethod
    def tearDownClass(cls):
        pass

    def test_create_invitee(self):
        factory = APIRequestFactory()
        request = factory.post(
            reverse("save_invitee"), data={"uid": self.uid, "password": "123456QwertY"}
        )
        response = create_new_invitee_view(request)
        self.assertEqual(response.status_code, 201, response.data)

    def test_create_invitee_invalid_uuid(self):
        factory = APIRequestFactory()
        request = factory.post(
            reverse("save_invitee"),
            data={"uid": str(uuid4()), "password": "123456QwertY"},
        )
        response = create_new_invitee_view(request)
        self.assertEqual(response.status_code, 404, response.data)
