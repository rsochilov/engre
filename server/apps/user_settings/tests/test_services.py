from unittest import skip
from datetime import date
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from django.shortcuts import reverse

from user_settings.views import ServiceViewSet
from accounts.models import User
from project.models import IntellectualService
from company.models import Company


class IntellectualServiceTests(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.admin_user = User.objects.get(email="admin@admin.com")
        cls.service_name = "test_service"
        cls.company_id = 5795
        cls.new_service = {
            "name": cls.service_name,
            "hourly_rate": 123,
            "category_id": 8,
            "industry_id": 4,
            "core_skills": [186],
            "tags": [2],
            "languages": [1],
            "description": "test_descr",
            "video_link": None,
            "company": cls.company_id,
        }
        # other_new_service = dict(cls.new_service)
        # other_new_service['name'] = 'second_test_service'
        # other_new_service['']
        # IntellectualService.objects.create(**other_new_service)

    @classmethod
    def tearDownClass(cls):
        pass

    @skip("")
    def test_create_service(self):
        factory = APIRequestFactory()
        view = ServiceViewSet.as_view()

        request = self.get_request_with_auth(factory)
        responce = view(request)

        self.assertEqual(responce.status_code, 200, str(responce))

        try:
            IntellectualService.objects.get(name=self.service_name)
        except IntellectualService.DoesNotExist:
            self.fail("Service was not created")

    @skip("")
    def test_create_duplicated_project(self):
        factory = APIRequestFactory()
        view = ServiceViewSet.as_view()

        request = self.get_request_with_auth(factory)
        responce = view(request)

        self.assertEqual(responce.status_code, 200)

        request = self.get_request_with_auth(factory)
        responce = view(request)

        self.assertEqual(responce.status_code, 400, "Error! Service was created twice")

    def get_request_with_auth(self, factory: APIRequestFactory):
        request = factory.post(reverse("add-service"), data=self.new_service)
        force_authenticate(request, self.admin_user)
        return request
