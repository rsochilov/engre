from blog import views as blog_views
from chat.views import ChatView
from company import views as company_views
from django.conf.urls import url
from market_location import views as location_views
from project import views as project_views

from . import views

urlpatterns = [
    url(r"profile/", views.ProfileView.as_view(), name="profile"),
    url(r"get-location/", location_views.CityAPIView.as_view(), name="get-location"),
    url(
        r"location-api/", location_views.CityListAPIView.as_view(), name="location-api"
    ),
    url(
        r"delete-profile/(?P<pk>[0-9]+)/",
        views.DeleteProfileAPIView.as_view(),
        name="delete-profile",
    ),
    url(r"save-profile/", views.UpdateProfileView.as_view(), name="update-profile"),
    url(r"chat/", ChatView.as_view(), name="chat"),
    url(r"company/", views.CompanyView.as_view(), name="company"),
    url(
        r"create-company/",
        company_views.CompanyViewSet.as_view(),
        name="create-company",
    ),
    url(
        r"update-company/",
        company_views.CompanyViewSet.as_view(),
        name="update-company",
    ),
    url(r"projects/(?P<pk>\d+)/edit/", views.CreateProjectView.as_view()),
    url(
        r"projects/(?P<pk>\d+)/board/",
        views.ProjectBoardView.as_view(),
        name="project-board",
    ),
    url(r"create-project/", views.ProjectViewSet.as_view(), name="add-project"),
    url(
        r"update-project-type/",
        views.UpdateProjectTypeView.as_view(),
        name="update-project-type",
    ),
    url(r"update-project/", views.ProjectViewSet.as_view(), name="update-project"),
    url(r"project/create/", views.CreateProjectView.as_view(), name="create-project"),
    url(r"projects/", views.ProjectListView.as_view(), name="projects-list"),
    url(
        r"get-core-skills/",
        project_views.CoreSkillListAPIView.as_view(),
        name="get-core-skills",
    ),
    url(r"archive-instance/", views.archive_instance, name="archive-instance"),
    url(r"services/(?P<pk>\d+)/edit/", views.ServiceView.as_view()),
    url(r"service/create/", views.ServiceView.as_view(), name="create-service"),
    url(r"create-service/", views.ServiceViewSet.as_view(), name="add-service"),
    url(r"update-service/", views.ServiceViewSet.as_view(), name="update-service"),
    url(r"services/", views.ServiceListView.as_view(), name="services-list"),
    url(r"archive-service/", views.archive_service, name="archive-service"),
    url(r"update-project/", views.UpdateProjectView.as_view(), name="update-project"),
    url(
        r"update-instance-status/",
        views.update_instance_status,
        name="update-instance-status",
    ),
    url(r"documents/", views.DocumentView.as_view(), name="documents"),
    url(r"create-document/", views.create_document, name="create-document"),
    url(
        r"get-project-documents/",
        views.get_project_documents,
        name="get-project-documents",
    ),
    url(r"archive-document/", views.archive_document, name="archive-document"),
    url(r"create-task/", views.TaskViewSet.as_view(), name="create-task"),
    url(r"update-task/", views.TaskViewSet.as_view(), name="update-task"),
    url(r"list-tasks/", views.TaskViewSet.as_view(), name="get-tasks"),
    url(r"remove-task/", views.TaskViewSet.as_view(), name="remove-task"),
    url(r"create-comment/", views.CommentAPIView.as_view(), name="create-comment"),
    url(r"update-comment/", views.CommentAPIView.as_view(), name="update-comment"),
    url(r"list-comments/", views.CommentAPIView.as_view(), name="get-comments"),
    url(r"remove-comment/", views.CommentAPIView.as_view(), name="remove-comment"),
    url(
        r"add-attachment/", views.TaskAttachmentViewSet.as_view(), name="add-attachment"
    ),
    url(
        r"list-attachments/",
        views.TaskAttachmentViewSet.as_view(),
        name="get-attachments",
    ),
    url(
        r"remove-attachment/",
        views.TaskAttachmentViewSet.as_view(),
        name="remove-attachment",
    ),
    # invitation urls
    url(r"invite-to-project/", views.send_invitation_email, name="invite-user"),
    #
    url(
        r"notifications/",
        views.NotificationsTemplateView.as_view(),
        name="notifications",
    ),
    url(
        r"update-notifications-settings/(?P<pk>[0-9]+)/",
        views.UpdateNotificationsSettingsAPIView.as_view(),
        name="update-notifications-settings",
    ),
    # project manager urls
    url(r"update-pm/", views.UpdatePMView.as_view(), name="update-pm"),
    url(r"review-pm/", views.ReviewPMView.as_view(), name="review-pm"),
    # reviews urls
    url(r"create-review/", company_views.ReviewViewSet.as_view(), name="create-review"),
    url(
        r"get-project-reviews/",
        project_views.ReviewListAPIView.as_view(),
        name="get-reviews",
    ),
    url(r"membership/", views.MembershipTemplateView.as_view(), name="membership"),
    # blogger urls
    url(r"update-blogger/", views.UpdateBloggerView.as_view(), name="update-blogger"),
    url(r"articles/(?P<pk>\d+)/edit/", views.ArticleTemplateView.as_view()),
    url(
        r"articles/create/", views.ArticleTemplateView.as_view(), name="create-article"
    ),
    url(r"articles/", views.ArticlesTemplateView.as_view(), name="article-list"),
    url(
        r"get-more-acticles/",
        blog_views.ArticleListAPIView.as_view(),
        name="get-more-articles",
    ),
    url(r"add-article/", blog_views.ArticleViewSet.as_view(), name="add-article"),
    url(r"update-article/", blog_views.ArticleViewSet.as_view(), name="update-article"),
    url(r"remove-article/", blog_views.ArticleViewSet.as_view(), name="remove-article"),
    url(
        r"archive-unarchive-article/",
        blog_views.ArchiveArticleView.as_view(),
        name="archive-unarchive",
    ),
]
