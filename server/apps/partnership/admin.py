from django.contrib import admin
from .models import ForInvestor, Software, SoftwareFile


class ForInvestorAdmin(admin.ModelAdmin):
    pass


admin.site.register(ForInvestor, ForInvestorAdmin)
admin.site.register(Software)
admin.site.register(SoftwareFile)
