from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r"software/(?P<pk>\d+)/", views.SoftwareDetail.as_view(), name="software_detail"
    ),
    url(r"software/search/", views.SoftwareSearch.as_view(), name="software_search"),
    url(r"software/", views.SoftwareList.as_view(), name="software"),
    url(
        r"investors/(?P<slug>[-a-zA-Z0-9_]+)",
        views.ForInvestorsDetailView.as_view(),
        name="investors-details",
    ),
    url(r"api/", views.ForInvestorsListAPIView.as_view(), name="for-investors-api"),
    url(
        r"interested-request/(?P<pk>[0-9]+)/",
        views.InterestedRequestAPIView.as_view(),
        name="for-investors-interested-request",
    ),
    url(
        r"investors/",
        views.ForInvestorsTemplateView.as_view(),
        name="for-investors-list",
    ),
    url(
        r"send-request/", views.CreatePartnershipRequest.as_view(), name="send-request"
    ),
]
