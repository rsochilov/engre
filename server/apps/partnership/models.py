# Create your models here.
from django.db import models

from core.models import AbstractUserRequest, WatchableMixin

INVESTOR_AMOUNTS = (
    (0, "10k - 50k"),
    (1, "50k - 100k"),
    (2, "100k - 500k"),
    (3, "500k - 1kk"),
)

OPERATING_SYSTEMS = (
    (0, "Windows"),
    (1, "Mac"),
    (2, "Linux"),
)

LICENSE_TYPE = (
    (0, "Standard License"),
    (1, "Extended License"),
    (2, "Multi License"),
)


class ForInvestor(WatchableMixin):
    """VR
    ForInvestor article model for Partnership view
    """

    name = models.CharField(max_length=150)
    slug = models.SlugField(max_length=255, unique=True, null=True, blank=True)
    industry = models.ForeignKey(
        "project.Industry", on_delete=models.CASCADE, related_name="for_investors"
    )
    text = models.TextField()
    excerpt = models.TextField(default="")
    image = models.ImageField(upload_to="for_investor", blank=True, null=True)
    amount = models.SmallIntegerField(choices=INVESTOR_AMOUNTS)
    interested_requests = models.PositiveIntegerField(default=0)

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("investors-details", args=[self.slug])

    def save(self, *args, **kwargs):
        self.slug = "-".join(self.name.split(" "))

        return super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name}"


class Software(models.Model):
    name = models.CharField(max_length=150)
    license = models.SmallIntegerField(choices=LICENSE_TYPE)
    short_description = models.TextField()
    description = models.TextField()
    size = models.CharField(max_length=50, blank=True, null=True)
    image = models.ImageField(upload_to="software_images/", blank=True, null=True)
    logo = models.ImageField(upload_to="software_logo/", blank=True, null=True)
    link = models.CharField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class SoftwareFile(models.Model):
    software = models.ForeignKey(
        "Software", related_name="software_file", on_delete=models.CASCADE
    )
    os = models.SmallIntegerField(choices=OPERATING_SYSTEMS)
    file = models.FileField(upload_to="software/")


class PartnershipRequest(AbstractUserRequest):
    pass
