from django.contrib import admin

from .models import Category, Job, JobRequest

admin.site.register(Category)
admin.site.register(Job)
admin.site.register(JobRequest)
