from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from marketplace.celery import app

from core.utils import create_attachment


@app.task
def send_apply_email(apply_data):
    content = render_to_string("careers/emails/apply.txt", {"apply_data": apply_data,})
    msg = EmailMessage(
        subject="{} / Apply".format(apply_data["email"]),
        body=content,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[settings.CAREERS_APPLY_EMAIL],
    )
    resume = apply_data["resume"]
    if resume["name"] and "," in resume["file"]:
        attachment = create_attachment(resume["name"], resume["file"].split(",", 1)[1])
        msg.attach(attachment)
    msg.send()
