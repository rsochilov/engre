from accounts.models import User
from accounts.serializers import UserSerializer
from apps.core.mixins import VueTemplateMixin
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from core.decorators import one_of_statuses_required
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from rest_framework import generics
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import ChatRoom, Message, ProjectRequest
from .serializers import (
    ArchiveChatRoomSerializer,
    AttachmentSerializer,
    ChatRoomSerializer,
    MessageSerializer,
    ProjectRequestSerializer,
)


@method_decorator(login_required, name="dispatch")
@method_decorator(one_of_statuses_required(["has_company", "is_pm"]), name="dispatch")
class ChatView(VueTemplateMixin, TemplateView):
    template_name = "settings/chat.html"

    def get_props(self):
        context = {"request": self.request}
        query = f"""SELECT cr.* FROM chat_chatroom cr 
JOIN chat_chatroom_users cu ON cr.id = cu.chatroom_id
JOIN accounts_user au ON au.id = cu.user_id
WHERE au.id = {self.request.user.id} 
ORDER BY (SELECT created_at FROM chat_message WHERE chat_room_id = cr.id ORDER BY created_at DESC LIMIT 1) DESC;"""
        chat_rooms = ChatRoom.objects.raw(query)

        return {
            "chatRooms": ChatRoomSerializer(
                chat_rooms, many=True, context=context
            ).data,
            "user": UserSerializer(self.request.user).data,
            "isMobile": self.request.is_mobile,
        }


class MessagesListAPIView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    filter_fields = ("chat_room",)
    pagination_class = LimitOffsetPagination

    def paginate_queryset(self, queryset):
        results = super().paginate_queryset(queryset)

        return reversed(results)


class ChatRoomCreateAPIView(generics.CreateAPIView):
    serializer_class = ChatRoomSerializer

    def create(self, request, *args, **kwargs):
        users = request.data.pop("users")

        chat = (
            ChatRoom.objects.filter(users__id__contains=users[0])
            .filter(users__id__contains=users[1])
            .first()
        )
        if chat:
            instance = chat
        else:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            instance = serializer.save()
            instance.users.set(users)
        data = self.get_serializer(instance).data
        data["url"] = reverse("chat")
        return Response(data, status=201, headers=self.get_success_headers(data))


class CreateMessageAPIView(generics.CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)


class UpdateMessageAPIView(generics.UpdateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)


class DeleteMessageAPIView(generics.DestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)

    def perform_destroy(self, instance):
        channel_layer = get_channel_layer()
        recipient = instance.chat_room.get_recipient(self.request.user)

        for user in self.request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "delete",
                    "chat": instance.chat_room.id,
                    "body": {"message": instance.id,},
                },
            )

        super().perform_destroy(instance)


class CreateAttachmentAPIView(generics.CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = AttachmentSerializer
    permission_classes = (IsAuthenticated,)


class UpdateProjectRequestAPIView(generics.UpdateAPIView):
    queryset = ProjectRequest.objects.all()
    serializer_class = ProjectRequestSerializer
    permission_classes = (IsAuthenticated,)


class UpdateChatRoomAPIView(generics.UpdateAPIView):
    queryset = ChatRoom.objects.all()
    serializer_class = ChatRoomSerializer
    permission_classes = (IsAuthenticated,)


class ArchiveChatAPIView(generics.DestroyAPIView):
    queryset = ChatRoom.objects.all()
    serializer_class = ArchiveChatRoomSerializer
    permission_classes = (IsAuthenticated,)


class RedirectToChatAPIView(APIView):
    def post(self, request, **kwargs):
        recipient_id = request.data.get("recipient")
        user = request.user

        try:
            recipient = User.objects.get(id=recipient_id)

            chat_room = (
                ChatRoom.objects.filter(users=user).filter(users=recipient).first()
            )

            if not chat_room:
                # chat_room = ChatRoom.objects.create(hidden_for=recipient)
                chat_room = ChatRoom.objects.create()
                chat_room.users.set([user, recipient])

            return Response({"url": f'{reverse("chat")}?room={chat_room.id}'})

        except User.DoesNotExist:
            return Response({"url": None})
