# Generated by Django 2.1.4 on 2019-04-08 14:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("chat", "0012_auto_20190408_1012"),
    ]

    operations = [
        migrations.AlterField(
            model_name="message",
            name="status",
            field=models.PositiveSmallIntegerField(
                choices=[(1, "Sending"), (2, "Sent"), (3, "Read")], default=1
            ),
        ),
    ]
