# Generated by Django 2.1.4 on 2019-04-18 09:10

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("chat", "0033_chatroom_blocked_by"),
    ]

    operations = [
        migrations.AddField(
            model_name="chatroom",
            name="blocked_users",
            field=models.ManyToManyField(
                related_name="blocked_in_chats", to=settings.AUTH_USER_MODEL
            ),
        ),
    ]
