# Generated by Django 2.1.4 on 2019-04-12 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("chat", "0020_auto_20190411_0955"),
    ]

    operations = [
        migrations.AddField(
            model_name="message",
            name="type",
            field=models.PositiveSmallIntegerField(
                choices=[
                    (1, "message"),
                    (2, "attachment"),
                    (3, "start project request"),
                    (4, "start project accept"),
                    (5, "start project reject"),
                ],
                default=1,
            ),
        ),
    ]
