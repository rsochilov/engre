from typing import List
from datetime import datetime

from .models import Message
from channels.generic.websocket import AsyncJsonWebsocketConsumer


class ChatConsumer(AsyncJsonWebsocketConsumer):
    """ Handle user's chat messages """

    async def connect(self):
        self.user = self.scope["user"]
        self.room_name = str(self.user.id)

        if self.user.is_authenticated:
            await self.channel_layer.group_add(self.room_name, self.channel_name)

            await self.accept()

    async def receive_json(self, content, **kwargs):
        """Parses received content and starts callbacks
        """
        if content.get("reader"):
            await self._update_readed_messages(content)

    async def _update_readed_messages(self, content: dict):
        ids = content.get("ids")
        if not ids:
            await self.send_json({"Reader error": "No ids found"})
            return
        try:
            Message.objects.filter(pk__in=ids).update(readed_at=datetime.now())
        except Exception as err:
            print(
                f"Unexpected exception occured during updating messages readed state:\n {err}"
            )
            await self.send_json(
                {
                    "Reader error": "Unexpected exception occured during updating messages"
                }
            )

    async def message(self, event):
        """ Handle simple text message """

        await self.send_json(event)

    async def messages_update(self, event):
        """ Handle messages updates """

        await self.send_json(event)

    async def attachment(self, event):
        """ Handle attachments """

        await self.send_json(event)

    async def delete(self, event):
        """ Handle delete a message """

        await self.send_json(event)

    async def room_update(self, event):
        """ Handle room updates, e.g. blocked / unblocked """

        await self.send_json(event)

    async def disconnect(self, code):
        # Leave room group
        await self.channel_layer.group_discard(self.room_name, self.channel_name)

    async def send_to_recipient(self, context, recipient_id):
        """ Send the message to a recipient channel """
        await self.channel_layer.group_send(
            str(recipient_id), context,
        )
