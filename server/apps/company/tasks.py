from accounts.models import User
from accounts.tasks import send_email
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from marketplace.celery import app

from .models import Company
from .tokens import invite_user_token


@app.task
def send_user_invitation(inviter_id, user_id, company_id):
    inviter = User.objects.get(pk=inviter_id)
    user = User.objects.get(pk=user_id)
    company = Company.objects.get(pk=company_id)
    site = Site.objects.get_current()

    domain = site.domain
    protocol = "https"

    context = {
        "user": user,
        "company": company,
        "uid": urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        "cid": urlsafe_base64_encode(force_bytes(company.pk)).decode(),
        "invid": urlsafe_base64_encode(force_bytes(inviter.pk)).decode(),
        "token": invite_user_token.make_token(user),
        "protocol": protocol,
        "domain": domain,
        "inviter": inviter,
    }

    email_body = render_to_string("company/invite-user-email.html", context)

    if inviter.is_pm:
        subject = "Invitation to the membership"
    else:
        subject = f"Invitation to {company.name}"

    send_email.delay(subject, email_body, [user.email])
