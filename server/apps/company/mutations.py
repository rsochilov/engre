import graphene
from core import types
from core.schema import LanguageType, UserType
from core.tasks import send_invitation_email
from core.utils import retrieve_object
from django.core.exceptions import ValidationError
from django.db import IntegrityError, transaction
from django.utils.translation import gettext_lazy as _
from graphql_jwt.decorators import login_required

from .inputs import CompanyInfoInput, CompanyInput, PartnershipInput
from .models import Company, Language, Ownership, Partnership, Worker
from .schema import CompanyType, OwnershipType, PartnershipType


class OwnershipCreateMutation(types.Mutation):
    class Arguments:
        name = graphene.String(required=True)

    ownership = graphene.Field(OwnershipType)

    @login_required
    def mutate(self, info, name):
        ownership, _ = Ownership.objects.get_or_create(name=name)

        return OwnershipCreateMutation(ownership=ownership)


class CompanyCreateMutation(types.Mutation):
    class Arguments:
        company_data = CompanyInput()
        position_id = graphene.ID(required=True)

    company = graphene.Field(CompanyType)
    user = graphene.Field(UserType)

    @transaction.atomic()
    @login_required
    def mutate(self, info, company_data, position_id):
        company = Company.objects.super_create(**company_data)

        worker_data = {
            "user": info.context.user,
            "company": company,
            "current_position": position_id,
        }
        try:
            Worker.objects.super_create(**worker_data)
        except IntegrityError:
            transaction.rollback()

        return CompanyCreateMutation(company=company, user=info.context.user)


class CompanyUpdateMutation(types.Mutation):
    class Arguments:
        company_id = graphene.ID(required=True)
        company_data = CompanyInput()
        position_id = graphene.ID()
        position_value = graphene.ID()

    company = graphene.Field(CompanyType)
    user = graphene.Field(UserType)

    @login_required
    def mutate(self, info, company_id, company_data, position_id, position_value):
        company = Company.objects.super_update(instance_id=company_id, **company_data)

        worker_data = {
            "current_position": position_id,
        }
        Worker.objects.super_update(instance_id=position_value, **worker_data)

        return CompanyCreateMutation(company=company, user=info.context.user)


class CompanyInfoUpdateMutation(types.Mutation):
    class Arguments:
        company_id = graphene.ID(required=True)
        company_info = CompanyInfoInput()

    company = graphene.Field(CompanyType)

    @login_required
    def mutate(self, info, company_id, company_info):
        company = Company.objects.super_update(instance_id=company_id, **company_info)

        return CompanyInfoUpdateMutation(company=company)


class InviteUser(types.Mutation):
    class Arguments:
        email = graphene.String(required=True)
        company_id = graphene.ID(required=True)

    status = graphene.String()

    @login_required
    def mutate(self, info, email, company_id):
        company = retrieve_object(company_id, Company)

        if company.workers.filter(user__email=email).exists():
            raise ValidationError(_("User already consists in the company"))

        send_invitation_email.delay(info.context.user.pk, company.id, email)

        return InviteUser(status="Success")


class PartnershipCreateMutation(types.Mutation):
    class Arguments:
        partnership_data = PartnershipInput()

    partnership = graphene.Field(PartnershipType)

    @login_required
    def mutate(self, info, partnership_data):
        partnership = Partnership.objects.super_create(**partnership_data)

        return PartnershipCreateMutation(partnership=partnership)


class LanguageCreateMutation(types.Mutation):
    class Arguments:
        name = graphene.String(required=True)

    language = graphene.Field(LanguageType)

    @login_required
    def mutate(self, info, name):
        language, _ = Language.objects.get_or_create(name=name)

        return LanguageCreateMutation(language=language)


class Mutation(graphene.ObjectType):
    create_company = CompanyCreateMutation.Field()
    update_company = CompanyUpdateMutation.Field()
    update_company_info = CompanyInfoUpdateMutation.Field()
    create_language = LanguageCreateMutation.Field()
    create_ownership = OwnershipCreateMutation.Field()
    invite_user = InviteUser.Field()
    create_partnership = PartnershipCreateMutation.Field()
