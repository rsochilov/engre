# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["test_create_company 1"] = {
    "data": {
        "createCompany": {
            "company": {
                "dateCreation": "2018-01-01",
                "description": "company desc",
                "languages": {
                    "edges": [
                        {"node": {"name": "language0"}},
                        {"node": {"name": "language1"}},
                        {"node": {"name": "language2"}},
                    ]
                },
                "location": "city0, country0",
                "logo": "http://localhost:8000/media/logos/file0.png",
                "name": "test company",
                "owner": {"username": "john2"},
                "ownership": {"name": "ownership0"},
                "presentation": "http://localhost:8000/media/company_documents/file2.pdf",
                "role": 1,
                "vanityUrl": "vanityUrl",
                "video": "http://localhost:8000/media/videos/file1.mp4",
                "website": "company.org",
            },
            "errors": None,
        }
    }
}

snapshots["test_create_ownership 1"] = {
    "data": {"createOwnership": {"ownership": {"name": "test ownership"}}}
}

snapshots["test_create_language 1"] = {
    "data": {"createLanguage": {"language": {"name": "ENG"}}}
}

snapshots["test_update_company 1"] = {
    "data": {
        "updateCompany": {
            "company": {
                "dateCreation": "2018-02-02",
                "description": "updated desc",
                "languages": {
                    "edges": [
                        {"node": {"name": "language3"}},
                        {"node": {"name": "language4"}},
                        {"node": {"name": "language5"}},
                    ]
                },
                "location": "city1, country1",
                "logo": "http://localhost:8000/media/logos/file6.png",
                "name": "updated name",
                "owner": {"username": "john4"},
                "ownership": {"name": "ownership1"},
                "presentation": "http://localhost:8000/media/company_documents/file8.pdf",
                "role": 2,
                "vanityUrl": "new vanityUrl",
                "video": "http://localhost:8000/media/videos/file7.mp4",
                "website": "newcompany.org",
            },
            "errors": None,
        }
    }
}

snapshots["test_send_user_invitation 1"] = {
    "data": {"inviteUser": {"status": "Success"}}
}

snapshots["test_create_partnership 1"] = {
    "data": {
        "createPartnership": {
            "errors": None,
            "partnership": {
                "company": {"name": "company7"},
                "description": "test desc",
                "name": "test partnership",
                "user": {"username": "john19"},
            },
        }
    }
}

snapshots["test_send_user_invitation_to_the_same_user_in_company 1"] = {
    "data": {
        "inviteUser": {
            "errors": ["User already consists in the company"],
            "status": None,
        }
    }
}
