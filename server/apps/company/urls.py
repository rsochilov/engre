from django.urls import path
from django.views.generic.base import TemplateView

from . import views

urlpatterns = [
    path(
        "sign-document/<pk>/", views.SignDocumentAPIView.as_view(), name="sign-document"
    ),
    path("invite-user/<pk>/", views.InviteUserAPIView.as_view(), name="invite-user"),
    path(
        "invite-user-confirm/<uidb64>/<cidb64>/<invidb64>/<token>/",
        views.InviteUserConfirmView.as_view(),
        name="invite-user-confirm",
    ),
    path(
        "invite-user-invalid/",
        TemplateView.as_view(template_name="company/invalid-invite-link.html"),
        name="invite-user-invalid",
    ),
    path(
        "toggle-worker-activeness/<pk>/", views.ToggleActivenessWorkerAPIView.as_view(),
    ),
    path(
        "delete-member-from-company/<pk>/",
        views.DeleteMemberFromCompanyAPIView.as_view(),
    ),
    path("delete-member-from-pm/<pk>/", views.DeleteMemberFromPmAPIView.as_view(),),
]
