from django.contrib import admin

from .models import Company, Ownership, Review, Worker


class ReviewAdmin(admin.ModelAdmin):
    list_filter = ("is_approved",)
    list_display = ("user", "rating", "date_add", "is_approved")
    ordering = ("user", "date_add")


admin.site.register(Company)
admin.site.register(Ownership)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Worker)
