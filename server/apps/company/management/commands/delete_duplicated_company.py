from django.core.management.base import BaseCommand
from django.utils.text import slugify
from loguru import logger

from company.models import Company


class Command(BaseCommand):
    help = "Deletes duplicated company"
    bugged_company_id = 5791

    def handle(self, *args, **options):
        try:
            bugged_company = Company.objects.get(pk=self.bugged_company_id)
        except Company.DoesNotExist:
            logger.info(f"No company found with id {self.bugged_company_id}")
            return
        bugged_company.delete()
        logger.success(f'Company "{bugged_company}" was deleted')
