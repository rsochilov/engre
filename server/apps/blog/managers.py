from django.db import models


class QuestionManager(models.Manager):
    def visible(self, user):
        qs = self.get_queryset()

        if user:
            try:
                return qs.exclude(
                    pk__in=user.blog_questions_configuration.hidden_questions.all()
                )
            except Exception:
                return qs.all()

        return qs.all()
