from django.db import models


class Notification(models.Model):
    CHAT, PROJECT, ETC = range(1, 4)
    TYPES_NOTIFY = (
        (CHAT, "Chat notification"),
        (ETC, "Etc notification"),
    )
    user = models.ForeignKey("accounts.User", on_delete=models.CASCADE)
    notification_type = models.PositiveSmallIntegerField(
        choices=TYPES_NOTIFY, default=ETC
    )
    body = models.CharField(max_length=256)
    created = models.DateTimeField(auto_now=True, null=False)
    viewed = models.DateTimeField(null=True)
