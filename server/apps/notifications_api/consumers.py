from datetime import datetime
from channels.exceptions import DenyConnection
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.contrib.auth.models import AnonymousUser


from .models import Notification
from .serializers import NotificationSerializer
from apps.core.utils import Notifier


class LiveNotificationConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        user = self.scope["user"]
        if user == AnonymousUser():
            raise DenyConnection("Anonymous user not allowed to receive notifications")
        await self.accept()

        self.user = user

        new_notifications = Notification.objects.filter(user=user, viewed__isnull=True)
        print(f"MSGDEBUG: new notifications count: {new_notifications.count()}")
        data = NotificationSerializer(new_notifications, many=True).data
        await self.send_json(data)

        self.room_name = f"notific_{self.user.id}"
        await self.channel_layer.group_add(self.room_name, self.channel_name)

    async def receive_json(self, content):
        user = self.scope["user"]

        if user == AnonymousUser:
            raise DenyConnection("Anonymous user not allowed to receive notifications")

        notification_data = content.get("notifications")
        if not notification_data:
            await self.send_json({"error": "Incorrect request body format"})
            return
        print(f"MSGDEBUG: received ids: {notification_data}")
        Notifier.update_notifications(notification_data, user)
        await self.send_json({"read_notifiations": notification_data})

    async def new_notification(self, event):
        print(
            f'MSGDEBUG: sending notification "{event.get("body")}" for user {self.user}'
        )
        await self.send_json(event.get("body"))

    async def disconnect(self, code):
        if hasattr(self, "room_name") and hasattr(self, "channel_name"):
            await self.channel_layer.group_discard(self.room_name, self.channel_name)
