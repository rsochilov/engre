from django.db.models import Model, ObjectDoesNotExist
from . import models


class AbstractMetaTagTemplateMixin:
    _tag_model: Model = None
    _tags_name = "seo_tags"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context[self._tags_name] = self._tag_model.objects.get(
                path=self.request.path.strip("/")
            )
        except ObjectDoesNotExist:
            content[self._tags_name] = None
        finally:
            context["seo_request"] = self.request
            return context


class MetaTagTemplateMixin(AbstractMetaTagTemplateMixin):
    """Added seo_tags and seo_request to context in
    get_context_data() method

    seo_tags: HeadMetaTags, filtered by request.path
    seo_request: request object from view
    """

    _tag_model = models.HeadMetaTags


class BlogArticleMetaTagTemplateMixin(AbstractMetaTagTemplateMixin):
    _tag_model = models.BlogArticleMetaTags


class NewsArticleMetaTagTemplateMixin(AbstractMetaTagTemplateMixin):
    _tag_model = models.NewsArticleMetaTags


class CommonProjectMetaTagTemplateMixin(AbstractMetaTagTemplateMixin):
    _tag_model = models.CommonProjectMetaTags


class AchievementMetaTagTemplateMixin(AbstractMetaTagTemplateMixin):
    _tag_model = models.AchievementMetaTags
