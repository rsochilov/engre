from django.apps import AppConfig


class SeoTagsConfig(AppConfig):
    name = "seo_tags"
