from django.conf import settings


class DefaultTagRenderer:
    head_template = """<meta name="description" content="{}" />
<meta name="keywords" content="{}" />
<meta name="robots" content="{}" />
<link rel="canonical" href="{}" />"""
    body_script_template = """<script type="application/ld+json">
{{
 	"@context": "https://schema.org",
    	"@type": "Organization",
    	"brand": "Engre",
    	"logo": "https://engre.co/static/images/logo.svg",
    	"name": "Engre",
    	"alternateName": "Engre",
        "url": "{}",
        "email": "{}",
        "address": {{
  	    "@type": "PostalAddress",
     	    "streetAddress": "71 Spit Brook Rd. Suite 308",
     	    "addressLocality": "Nashua",
    	    "addressRegion": "NH",
    	    "postalCode": "03060"
   	    }},
        "telephone": "{}",
        "sameAs": ["https://www.facebook.com/engre.engineering/","https://twitter.com/Engre_Engineer","https://www.linkedin.com/company/engre-engineering/"]
}}
</script>"""

    @staticmethod
    def render_head(host: str) -> str:
        return DefaultTagRenderer.head_template.format("", "", "noindex, follow", host)

    @staticmethod
    def render_body(host: str):
        return DefaultTagRenderer.body_script_template.format(
            host, settings.SUPPORT_EMAIL, settings.PHONE,
        )


class MainTagRenderer(DefaultTagRenderer):
    og_tags_template = """<meta property="og:type" content="website" />
<meta property="og:title" content="{}" />
<meta property="og:description" content="{}" />
<meta property="og:url" content="{}" />
<meta property="og:site_name" content="Engre" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="{}" />
<meta name="twitter:title" content="{}" />
"""

    @staticmethod
    def render_head(tags, host: str) -> str:
        head_tags = MainTagRenderer.render_head_template(tags, host)
        if not tags.has_og_tags:
            return head_tags
        return head_tags + MainTagRenderer.render_og_tags(tags, host)

    @staticmethod
    def render_head_template(tags, host: str) -> str:
        return MainTagRenderer.head_template.format(
            tags.description, tags.keywords, tags.robots, host
        )

    @staticmethod
    def render_og_tags(tags, host: str) -> str:
        if tags:
            return MainTagRenderer.og_tags_template.format(
                tags.title, tags.description, host, tags.description, tags.title
            )
        else:
            return MainTagRenderer.og_tags_template.format(
                tags.title, "", host, "", tags.title
            )

    @staticmethod
    def render_body(host):
        return MainTagRenderer.body_script_template.format(
            host, settings.SUPPORT_EMAIL, settings.PHONE
        )


class ArticleTagRenderer(MainTagRenderer):
    add_head_tags = """
<meta property="og:image" content="{}">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
<meta property="og:image:alt" content="{}">
<meta name="twitter:image" content="{}" />
"""
    add_body_tags = """<script type="application/ld+json">
{{
  "@context": "http://schema.org/",
  "@type": "Article",
  "mainEntityOfPage": {{
    "@type": "WebPage",
    "@id": "{}"
   }},
  "headline": "{}",
  "datePublished": "{}",
  "dateModified": "{}",
  "description": "{}",
 	 "image": {{
  	  "@type": "ImageObject",
  	  "height": "1200",
  	  "width": "600",
 	   "url": "{}"
  }},
  "author": "{}",
  "publisher": {{
    "@type": "Organization",
    "logo": {{
   	   "@type": "ImageObject",
   	   "url": "https://engre.co/static/images/logo.svg"
 	   }},
    "name": "Engre"
  }}
}}
</script>"""

    @staticmethod
    def render_head(tags, host: str) -> str:
        return (
            MainTagRenderer.render_head_template(tags, host)
            + MainTagRenderer.render_og_tags(tags, host)
            + ArticleTagRenderer._render_add_tags(tags)
        )

    @staticmethod
    def _render_add_tags(tags):
        return ArticleTagRenderer.add_head_tags.format(
            tags.image_path, tags.image_alt, tags.image_path
        )

    @staticmethod
    def render_body(tags, host):
        return MainTagRenderer.render_body(
            host
        ) + ArticleTagRenderer.add_body_tags.format(
            tags.path,
            tags.headline,
            tags.article.created_at,
            tags.article.created_at,
            tags.description,
            tags.image_path,
            tags.article.author,
        )
