from os import path
from urllib.parse import urlparse
from typing import List, Iterator

import xlrd
from django.core.management.base import BaseCommand
from loguru import logger

from seo_tags.models import HeadMetaTags


class Tag:
    def __init__(self, path: str, title: str, description: str, keywords: str):
        self.path = path
        self.title = title
        self.description = description
        self.keywords = keywords


class Command(BaseCommand):
    help = "Inserts tags info from xlsx file to db"
    key_words = [
        "категории",
        "категории аероспейс",
        "корр скиллы аероспейс",
        "категории автомотив",
        "корр скиллы автомотив",
    ]

    def add_arguments(self, parser):
        parser.add_argument("filepath", nargs="+", type=str)

    def handle(self, *args, **options):
        filepath = options["filepath"][0]
        if not path.isfile(filepath):
            logger.critical(f'Unable to find file "{filepath}"')
            exit(-1)

        self.insert_tags(self.read_from_xl(filepath))

    def read_from_xl(self, filepath: str) -> Iterator[Tag]:
        sheet = xlrd.open_workbook(filepath).sheet_by_index(0)
        for row_num in range(1, sheet.nrows):
            vals = sheet.row_values(row_num)
            if not vals[0] in self.key_words:
                parsed_url = urlparse(vals[0])
                yield Tag(parsed_url.path.strip("/"), vals[1], vals[2], vals[3])

    def insert_tags(self, tags: List[Tag]):
        for tag_info in tags:
            tag, created = HeadMetaTags.objects.get_or_create(path=tag_info.path)

            tag.title = tag_info.title
            tag.description = tag_info.description
            tag.keywords = tag_info.keywords
            tag.save(update_fields=["description", "keywords", "title"])

            if created:
                logger.success(f'Data for "{tag_info.path}" created')
            else:
                logger.info(
                    f'Data for "{tag_info.path}" already exists. '
                    + f"Updated fields: description, keywords, title"
                )
