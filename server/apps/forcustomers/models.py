from core.models import WatchableMixin
from django.db import models
from solo.models import SingletonModel


class ForCustomersPage(WatchableMixin, SingletonModel):
    video = models.FileField()
