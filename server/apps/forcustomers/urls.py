from django.conf.urls import url

from .views import ForCustomersView

urlpatterns = [
    url(r"", ForCustomersView.as_view(), name="for_customers"),
]
