from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import ForCustomersPage


admin.site.register(ForCustomersPage, SingletonModelAdmin)
