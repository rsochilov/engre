import graphene
from core.schema import LanguageType, TagType
from core.utils import get_media_absolute_url
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from market_location.schema import CountryType

from .filters import NewsArticleFilterSet, NewsCommentFilterSet
from .models import Comment, NewsArticle, NewsCategory


class NewsArticleType(DjangoObjectType):
    tags = DjangoFilterConnectionField(TagType)
    video = graphene.String()
    country = graphene.Field(CountryType)
    languages = DjangoFilterConnectionField(LanguageType)
    interested = graphene.Int()
    tag_list = graphene.String()
    comments = DjangoFilterConnectionField(
        lambda: NewsCommentType, filterset_class=NewsCommentFilterSet
    )
    comments_count = graphene.Int()
    answers_count = graphene.Int()

    class Meta:
        model = NewsArticle
        interfaces = (graphene.relay.Node,)

    def resolve_interested(self, info):
        return self.interested_users.count()

    def resolve_tag_list(self, info):
        return ", ".join(self.tags.all().values_list("name", flat=True))

    def resolve_comments_count(self, info):
        return self.comments.count()

    def resolve_answers_count(self, info):
        return self.comments.filter(parent__isnull=True).count()

    def resolve_video(self, info):
        return get_media_absolute_url(self.video)


class NewsCategoryType(DjangoObjectType):
    articles = DjangoFilterConnectionField(
        NewsArticleType, filterset_class=NewsArticleFilterSet
    )
    picture = graphene.String()

    def resolve_picture(self, info):
        return get_media_absolute_url(self.picture)

    class Meta:
        model = NewsCategory
        interfaces = (graphene.relay.Node,)
        filter_fields = ("name",)


class NewsCommentType(DjangoObjectType):
    rating = graphene.Int()
    is_liked = graphene.Boolean()
    is_disliked = graphene.Boolean()

    def resolve_rating(self, info):
        return self.liked.count() - self.disliked.count()

    def resolve_is_liked(self, info):
        if info.context.user.is_authenticated:
            return self.liked.filter(id=info.context.user.id).exists()

        return False

    def resolve_is_disliked(self, info):
        if info.context.user.is_authenticated:
            return self.disliked.filter(id=info.context.user.id).exists()

        return False

    class Meta:
        model = Comment
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    news_categories = DjangoFilterConnectionField(NewsCategoryType)
    news_category = graphene.Field(
        NewsCategoryType, slug=graphene.String(required=True)
    )
    news_articles = DjangoFilterConnectionField(
        NewsArticleType, filterset_class=NewsArticleFilterSet
    )
    news_article = graphene.relay.Node.Field(NewsArticleType)
    news_comments = DjangoFilterConnectionField(
        NewsCommentType, filterset_class=NewsCommentFilterSet
    )

    def resolve_news_category(sefl, info, slug):
        try:
            return NewsCategory.objects.get(slug=slug)
        except NewsCategory.DoesNotExist:
            return None
