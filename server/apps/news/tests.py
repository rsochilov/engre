import glob
import json
import os

from core.models import Category, Language, Tag
from core.schema import CategoryType, LanguageType, TagType
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from graphql_jwt.shortcuts import get_token
from graphql_relay import to_global_id
from kombu.utils import json
from market_location.models import Country
from market_location.schema import CountryType
from news.models import NewsArticle, NewsComment
from news.schema import NewsArticleType


class NewsTestCase(TestCase):
    def setUp(self):
        self.test_username = "testuser_moderator"
        self.test_password = "12345678"
        self.test_email = "marketplace.ddi.test_moderator@gmail.com"
        self.test_moderator_user = User.objects.create_user(
            username=self.test_username,
            password=self.test_password,
            email=self.test_email,
        )
        token = get_token(self.test_moderator_user)
        self.test_moderator_user.profile.active_token = token
        self.test_moderator_user.profile.save()
        self.test_user_glob_id = to_global_id(
            User.__name__, self.test_moderator_user.id
        )

        _tag = [Tag.objects.create(name="tag"), Tag.objects.create(name="test")]
        self.glob_tags = json.dumps(
            [to_global_id(TagType.__name__, i.id) for i in _tag]
        )

        _languages = [
            Language.objects.create(name="ENG"),
            Language.objects.create(name="FR"),
        ]
        self.glob_language_EN = to_global_id(LanguageType.__name__, _languages[0].id)
        self.glob_languages = json.dumps(
            [to_global_id(LanguageType.__name__, i.id) for i in _languages]
        )

        _category = Category.objects.create(name="cat")
        self.glob_category = to_global_id(CategoryType.__name__, _category.id)

        _country1 = Country.objects.create(name="Krakozhia")
        _country2 = Country.objects.create(name="Sokovia")
        self.glob_country1 = to_global_id(CountryType.__name__, _country1.id)

        _picture = SimpleUploadedFile(
            "test_file.png", b"file_content", content_type="image/png"
        )

        _title = "title_"

        _content = "content" * 25

        self.test_news_article1 = NewsArticle.objects.create(
            title=_title * 2, content=_content, picture=_picture, country=_country1
        )
        self.glob_test_news_article1 = to_global_id(
            NewsArticleType.__name__, self.test_news_article1.id
        )
        self.test_news_article1.languages.add(*_languages)
        self.test_news_article1.tags.add(*_tag)
        self.test_news_article1.categories.add(_category)
        self.test_news_article1.save()

        self.test_news_article2 = NewsArticle.objects.create(
            title=_title * 3, content=_content, picture=_picture, country=_country2
        )
        self.glob_test_news_article2 = to_global_id(
            NewsArticleType.__name__, self.test_news_article2.id
        )
        self.test_news_article2.languages.add(*_languages)
        self.test_news_article2.tags.add(*_tag)
        self.test_news_article2.categories.add(_category)
        self.test_news_article2.save()

        self.test_username = "testuser1"
        self.test_password = "12345678"
        self.test_email = "marketplace.ddi.test1@gmail.com"
        self.test_comment_user = User.objects.create_user(
            username=self.test_username,
            password=self.test_password,
            email=self.test_email,
        )
        token = get_token(self.test_comment_user)
        self.test_comment_user.profile.active_token = token
        self.test_comment_user.profile.save()
        self.test_user_glob_id = to_global_id(User.__name__, self.test_comment_user.id)

        comment1_0 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment1_0",
            parent=None,
        )
        comment2_0 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment2_0",
            parent=None,
        )
        comment1_1 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment1_1",
            parent=comment1_0,
        )
        comment1_2 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment1_2",
            parent=comment1_0,
        )
        comment1_3 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment1_3",
            parent=comment1_0,
        )
        comment1_1_1 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment1_1_1",
            parent=comment1_1,
        )
        comment1_1_1_1 = NewsComment.objects.create(
            news_article=self.test_news_article1,
            user=self.test_comment_user,
            comment="comment1_1_1_1",
            parent=comment1_1_1,
        )

        self.test_parent_comment_glob_id = to_global_id(
            NewsComment.__name__, comment1_1_1_1.id
        )

        self.test_comment_for_unrate = comment1_1_1_1

        self.test_title_for_add = "test_new_title" * 3
        self.test_content_for_add = "test_new_content" * 20
        self.test_picture_for_add = SimpleUploadedFile(
            "test_new_file.png", b"file_content", content_type="image/png"
        )

        self.test_title_for_update = "test_updated_title" * 3
        self.test_content_for_update = "test_updated_content" * 20
        self.test_picture_for_update = SimpleUploadedFile(
            "test_updated_file.png", b"file_content", content_type="image/png"
        )

        _updated_category = Category.objects.create(name="updated_cat")
        self.glob_updated_category = to_global_id(
            CategoryType.__name__, _updated_category.id
        )

        _updated_tag = [
            Tag.objects.create(name="updated_tag"),
            Tag.objects.create(name="updated_test"),
        ]
        self.glob_updated_tags = json.dumps(
            [to_global_id(TagType.__name__, i.id) for i in _updated_tag]
        )

    def tearDown(self):
        for filename in glob.glob(
            os.path.join(settings.MEDIA_ROOT, "news_images/test_file*")
        ):
            os.remove(filename)

    def test_get_all_news_articles(self):
        query = """ query {
        allNewsArticles {
                   edges {
                    node {
                        id
                        title
                        picture
                        country {
                            name
                        }
                        languages {
                            edges {
                                node {
                                    id
                                    name
                                }
                            }
                        }
                        tags {
                            edges {
                                node {
                                    id
                                    name
                                }
                            }
                        }
                        categories {
                            edges {
                                node {
                                    id
                                    name
                                }
                            }
                        }
                    }
                   }
                }
        }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_all_news_articles_by_country(self):
        query = """ query {
                allNewsArticles(country_Name: "Krakozhia") {
                           edges {
                            node {
                                id
                                title
                                picture
                                country {
                                    id
                                    name
                                }
                                languages {
                                    edges {
                                        node {
                                            id
                                            name
                                        }
                                    }
                                }
                                tags {
                                    edges {
                                        node {
                                            id
                                            name
                                        }
                                    }
                                }
                                categories {
                                    edges {
                                        node {
                                            id
                                            name
                                        }
                                    }
                                }
                            }
                           }
                        }
                }
                """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_all_news_articles_by_language(self):
        query = """ query {
                       allNewsArticles(languages_Name: "ENG") {
                                  edges {
                                   node {
                                       id
                                       title
                                       picture
                                       country {
                                           id
                                           name
                                       }
                                       languages {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                       tags {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                       categories {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                   }
                                  }
                               }
                       }
                       """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_all_news_articles_by_tag(self):
        query = """ query {
                       allNewsArticles(tags_Name: "tag") {
                                  edges {
                                   node {
                                       id
                                       title
                                       picture
                                       country {
                                           id
                                           name
                                       }
                                       languages {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                       tags {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                       categories {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                   }
                                  }
                               }
                       }
                       """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_all_news_articles_by_category(self):
        query = """ query {
                       allNewsArticles(categories_Name: "cat") {
                                  edges {
                                   node {
                                       id
                                       title
                                       picture
                                       country {
                                           id
                                           name
                                       }
                                       languages {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                       tags {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                       categories {
                                           edges {
                                               node {
                                                   id
                                                   name
                                               }
                                           }
                                       }
                                   }
                                  }
                               }
                       }
                       """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_news_article_comments(self):
        data = {
            "query": """query {
                        newsArticleComments(newsArticleGlobId: "%s")
                      }
                  """
            % self.glob_test_news_article1
        }

        response = self.client.post(reverse("api"), data=data)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_create_news_article(self):
        data = {
            "query": """mutation {
                createNewsArticle (title: "%s", content: "%s", languages: %s, tags: %s,
                    categories: "%s" , picture: {file: "%s", name: "%s"}, country: "%s" )
                    { newsArticle {
                        id
                        title
                        content
                        tags {
                          edges {
                            node {
                              name
                              id
                            }
                          }
                        }
                        categories {
                          edges {
                            node {
                              name
                              id
                            }
                          }
                        }
                        languages {
                           edges {
                            node {
                              name
                              id
                            }
                          }
                        }
                        }
                        }
                       }
                        """
            % (
                self.test_title_for_add,
                self.test_content_for_add,
                self.glob_languages,
                self.glob_tags,
                self.glob_category,
                self.test_picture_for_add.file,
                self.test_picture_for_add.name,
                self.glob_country1,
            )
        }
        token = self.test_moderator_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["data"]["createNewsArticle"]["newsArticle"]["title"],
            self.test_title_for_add,
        )

    def test_update_blog_article(self):
        data = {
            "query": """mutation {
                        updateNewsArticle (newsArticleGlobId: "%s" ,title: "%s", content: "%s", languages: %s, tags: %s,
                            categories: "%s" , picture: {file: "%s", name: "%s"})
                            { newsArticle {
                                id
                                title
                                content
                                tags {
                                  edges {
                                    node {
                                      name
                                      id
                                    }
                                  }
                                }
                                categories {
                                  edges {
                                    node {
                                      name
                                      id
                                    }
                                  }
                                }
                                languages {
                                   edges {
                                    node {
                                      name
                                      id
                                    }
                                  }
                                }
                                }
                                }
                               }
                                """
            % (
                self.glob_test_news_article1,
                self.test_title_for_update,
                self.test_content_for_update,
                self.glob_languages,
                self.glob_updated_tags,
                self.glob_updated_category,
                self.test_picture_for_update.file,
                self.test_picture_for_update.name,
            )
        }

        token = self.test_moderator_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["data"]["updateNewsArticle"]["newsArticle"]["title"],
            self.test_title_for_update,
        )

    def test_add_news_article_comment_to_root(self):
        data = {
            "query": """mutation {
                        createNewsComment (newsArticleGlobId : "%s", comment: "fresh_root_comment") {
                            newsComment {
                                id
                                comment
                                createdAt
                                parent {
                                    id
                                }
                            }
                        }
            }
            """
            % self.glob_test_news_article1
        }
        token = self.test_comment_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_add_news_article_comment_to_comment(self):
        data = {
            "query": """mutation {
                        createNewsComment (newsArticleGlobId : "%s", comment: "comment1_1_1_1_1", parentCommentGlobId: "%s") {
                            newsComment {
                                id
                                comment
                                createdAt
                                parent {
                                    id
                                }
                            }
                        }
            }
            """
            % (self.glob_test_news_article1, self.test_parent_comment_glob_id)
        }
        token = self.test_comment_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_rate_news_comment(self):
        data = {
            "query": """ mutation {
                               rateNewsComment(newsCommentGlobId: "%s") {
                                   newsComment {
                                       id
                                       comment
                                       rating
                                       user {
                                           username
                                       }
                                   }
                               }

               }
               """
            % self.test_parent_comment_glob_id
        }
        token = self.test_comment_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_unrate_news_comment(self):
        before = self.test_comment_for_unrate.news_comment_rating.filter(rate=1).count()
        self.test_comment_for_unrate.news_comment_rating.create(
            user=self.test_comment_user, rate=1
        )
        data = {
            "query": """ mutation {
                               rateNewsComment(newsCommentGlobId: "%s") {
                                   newsComment {
                                       id
                                       comment
                                       rating
                                       user {
                                           username
                                       }
                                   }
                               }

               }
               """
            % self.test_parent_comment_glob_id
        }
        token = self.test_comment_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        after = self.test_comment_for_unrate.news_comment_rating.filter(rate=1).count()
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(before, after)
