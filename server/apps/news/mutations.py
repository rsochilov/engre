import graphene
from accounts.schema import UserType
from core import types
from core.utils import get_image_from_base64, retrieve_object, upload_file
from graphql_jwt.decorators import login_required

from .inputs import NewsArticleInput
from .models import Comment, NewsArticle, NewsCategory
from .schema import NewsArticleType, NewsCommentType


class CreateNewsCommentMutation(types.Mutation):
    class Arguments:
        news_article = graphene.ID(required=True)
        parent = graphene.ID()
        comment = graphene.String(required=True)

    comment = graphene.Field(NewsCommentType)
    article = graphene.Field(NewsArticleType)

    @login_required
    def mutate(self, info, **kwargs):
        comment = Comment.objects.super_create(user=info.context.user, **kwargs)

        article = comment.news_article

        return CreateNewsCommentMutation(comment=comment, article=article)


class SubscribeMutation(types.Mutation):
    class Arguments:
        category_id = graphene.ID(required=True)

    me = graphene.Field(UserType)

    @login_required
    def mutate(self, info, category_id):
        category = retrieve_object(category_id, NewsCategory)

        if category.subscribers.filter(id=info.context.user.id).exists():
            category.subscribers.remove(info.context.user)
        else:
            category.subscribers.add(info.context.user)

        return SubscribeMutation(me=info.context.user)


class CreateNewsArticle(types.Mutation):
    class Arguments:
        article_data = NewsArticleInput(required=True)

    article = graphene.Field(NewsArticleType)

    @login_required
    def mutate(self, info, article_data):
        images = article_data.images
        del article_data["images"]

        article = NewsArticle.objects.super_create(**article_data)

        if images:
            images = [
                get_image_from_base64(image.name, image.file)
                for image in article_data.images
            ]

            article.images = [
                upload_file("media/news/images", image.name, image.file)
                for image in images
            ]
            article.save()

        return CreateNewsArticle(article=article)


class UpdateNewsArticle(types.Mutation):
    class Arguments:
        article_id = graphene.ID(required=True)
        article_data = NewsArticleInput(required=True)

    article = graphene.Field(NewsArticleType)

    @login_required
    def mutate(self, info, article_id, article_data):
        images = article_data.images
        del article_data["images"]

        article = NewsArticle.objects.super_update(
            instance_id=article_id, **article_data
        )

        if images:
            try:
                images = [
                    get_image_from_base64(image.name, image.file)
                    for image in article_data.images
                ]

                article.images = [
                    upload_file("media/news/images", image.name, image.file)
                    for image in images
                ]
                article.save()
            except Exception:
                pass

        return UpdateNewsArticle(article=article)


class Mutation(graphene.ObjectType):
    create_news_comment = CreateNewsCommentMutation.Field()
    subscribe_to_news = SubscribeMutation.Field()
    create_news_article = CreateNewsArticle.Field()
    update_news_article = UpdateNewsArticle.Field()
