import copy
import io
import os
import os.path
import pickle
import re
import time
from uuid import uuid4
import asyncio
from time import sleep
from typing import Callable, Iterable
from dateutil.parser import parse as parse_date

import requests
from googleapiclient.http import MediaIoBaseDownload
from bs4 import BeautifulSoup
from django.utils.text import slugify
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from googleapiclient.errors import HttpError
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from openpyxl import load_workbook
from PIL import Image, ImageOps
from .exceptions import ParsingError

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
    "https://www.googleapis.com/auth/drive.file",
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/drive.readonly",
]

NEWS_ARTICLE_FOLDER_NAME = "News article"


class Downloader:
    service = None

    def require_credentials(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(settings.GOOGLE_TOKEN_PATH):
            with open(settings.GOOGLE_TOKEN_PATH, "rb") as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    settings.GOOGLE_CREDENTIALS_PATH, SCOPES
                )
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open(settings.GOOGLE_TOKEN_PATH, "wb") as token:
                pickle.dump(creds, token)

        self.service = build("drive", "v3", credentials=creds)

    def download(self, file_id, mime_type, export=False):
        if not self.service:
            self.require_credentials()

        if export:
            request = self.service.files().export_media(
                fileId=file_id, mimeType=mime_type
            )
        else:
            request = self.service.files().get_media(fileId=file_id)

        buffer = io.BytesIO()

        downloader = MediaIoBaseDownload(buffer, request)
        done = False
        while not done:
            try:
                status, done = downloader.next_chunk()
            except HttpError as err:
                pass

        buffer.seek(0)

        return buffer


class BaseParser:
    __errors = []
    min_index: int
    inject: Callable
    sleep_time: float

    @property
    def errors(self):
        return self.__errors

    def __init__(self, wb):
        self.wb = wb
        self.min_index = 2
        self.sleep_time = 3

    def select_sheet(self, sheet_name):
        self.sheet = self.wb[sheet_name]

    def parse_content(self, content):
        soup = BeautifulSoup(content, "html.parser")

        # handle html tag
        html = soup.find("html")
        html.unwrap()
        p_wrapper = soup.new_tag("div")
        p_wrapper["class"] = "article-text"

        # remove head tag
        soup.head.decompose()

        # handle body tag
        body = soup.find("body")
        body.unwrap()

        # handle h1 tag
        h1 = soup.find("h1")
        if h1:
            new_h1 = soup.new_tag("h1")
            new_h1.string = h1.text
            h1.replace_with(new_h1)

        # handle h2 tags
        h2_tags = soup.find_all("h2")
        for h2 in h2_tags:
            new_h2 = soup.new_tag("h2")
            new_h2.string = h2.text
            h2.replace_with(new_h2)

        # handle p tags
        p_tags = soup.find_all("p")
        for p in p_tags:
            if p.text == "&nbsp;" or (not p.text and not p.find("img")):
                p.decompose()
            else:
                p["style"] = ""

        # handle span tags
        span_tags = soup.find_all("span")
        for span in span_tags:
            if span.text != "&nbsp;":
                span.unwrap()

            span["style"] = ""

        # handle a tags
        a_tags = soup.find_all("a")
        for a in a_tags:
            new_a = copy.copy(a)
            new_a["style"] = ""
            new_a["target"] = "_blank"

            if a.has_attr("href"):
                new_a["href"] = a["href"].split("?q=")[-1]
                if "&sa=D&ust=" in new_a["href"]:
                    new_a["href"] = new_a["href"].split("&sa=D&ust=")[0]
            a.replace_with(new_a)

        # handle img tags
        img_tags = soup.find_all("img")
        for img in img_tags:
            img["style"] = ""
            img["class"] = "img-fluid"

        return soup.prettify()

    def get_rows_count(self) -> int:
        """Returns index of document last row

        Returns:
            int -- [description]
        """
        idx = self.min_index
        while True:
            if self.get_title(idx):
                idx += 1
                continue
            return idx

    def get_data_by_index(self, index: int) -> dict or None:
        """Retreives information from workbook by index,
        uploads docs and pictures and returs

        Rewrite this method in subclusses

        Arguments:
            index {int} -- index

        Returns:
            dict or None -- data dictionary or None
        """
        title = self.get_title(index)
        if not title:
            return

        content_id = self.get_content_id(index)
        try:
            # there is sleep coz project is free so
            # "User Rate Limit Exceeded" error will occure
            sleep(self.sleep_time)
            downloader = Downloader()
            content_bytes = downloader.download(content_id, "text/html", export=True)
        except HttpError as error:
            self.__errors.append(ParsingError(title, error))
            return
        try:
            collected = {
                "title": title,
                "content": self.parse_content(content_bytes.read()),
                "category_id": self.get_category_id(index),
                "tags": self.get_tags(index),
                "excerpt": self.get_excerpt(index),
                "author_id": self.get_author_id(index),
                "created_at": self.get_date(index),
                "country_id": self.get_country_id(index),
            }
            tags = self.get_seo_tags(index)
            if tags:
                collected["seo_tags"] = tags
        except (AttributeError, ValueError) as error:
            self.__errors.append(ParsingError(title, error))
            return

        if self.inject:
            try:
                name, obj = self.inject(index)
            except (HttpError, AttributeError) as error:
                self.__errors.append(ParsingError(title, error))
                return
            collected[name] = obj
        return collected

    def parse_data(self, inject: Callable = None) -> Iterable[dict]:
        """Parses data async, returns generator[dict] of parsed data

        Keyword Arguments:
            inject {Callable} -- callback function, stores in self.inject

        Yields:
            Iterable[dict] -- parsing result, stored in dict
        """
        self.inject = inject
        rows_count = self.get_rows_count()
        loop = asyncio.get_event_loop()

        # TODO: delete this, it's only for tests
        # return (self.get_data_by_index(idx) for idx in range(2, 4))
        # return (self.get_data_by_index(idx) for idx in range(self.min_index, rows_count))

        tasks = asyncio.gather(
            *[
                loop.run_in_executor(None, self.get_data_by_index, idx)
                for idx in range(self.min_index, rows_count)
            ]
        )
        results = loop.run_until_complete(tasks)
        return (res for res in results if res)

    def get_content_id(self, index):
        return self.sheet[f"J{index}"].value.split("id=")[-1]

    def get_title(self, index):
        return self.sheet[f"A{index}"].value

    def get_seo_tags(self, index: int) -> dict:
        if not self.sheet[f"M{index}"].value:
            return None
        image = self.get_seo_image(index, "O")
        img_name = slugify(self.sheet[f"A{index}"].value) + ".jpg"
        img_path = os.path.join(settings.MEDIA_ROOT, img_name)
        image.save(img_path, format="jpeg")
        return {
            "title": self.sheet[f"A{index}"].value,
            "description": self.sheet[f"M{index}"].value,
            "keywords": self.sheet[f"N{index}"].value,
            "image_url": f"https://{settings.SITE_URL}{settings.MEDIA_URL}{img_name}",
            "image_alt": self.sheet[f"P{index}"].value,
        }

    def get_seo_image(self, index: int, letter: str) -> Image:
        image_id = self.sheet[f"{letter}{index}"].value.split("id=")[-1]
        downloader = Downloader()
        sleep(self.sleep_time)
        image_bytes = downloader.download(image_id, "image/jpeg")
        return Image.open(image_bytes)

    def get_category_id(self, index):
        return int(self.sheet[f"B{index}"].value)

    def get_tags(self, index):
        return self.sheet[f"D{index}"].value.split(", ")

    def get_country_id(self, index):
        data = self.sheet[f"F{index}"].value

        if not data:
            return None

        country_id = None

        if isinstance(data, str):
            countries = self.sheet[f"F{index}"].value.split(",")

            if countries:
                if int(countries[0]) == 254:
                    return int(countries[1].strip())

                return int(countries[0].strip())

            return None

        country_id = int(data)
        if country_id == 254:
            return None

        return country_id

    def get_date(self, index):
        return self.sheet[f"H{index}"].value

    def get_excerpt(self, index):
        return self.sheet[f"I{index}"].value

    def get_author_id(self, index):
        return int(self.sheet[f"G{index}"].value)

    def parse_image(self, imgbytes, name):
        image = Image.open(imgbytes)
        buffer = io.BytesIO()
        image.save(buffer, format="JPEG")
        image = ContentFile(buffer.getvalue())
        image_obj = InMemoryUploadedFile(
            image, None, name, "image/jpeg", image.tell, None
        )
        return image_obj


class NewsParser(BaseParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.select_sheet("News article example")
        self.min_index = 3

    def get_preview(self, index):
        preview_id = self.get_preview_id(index)

        downloader = Downloader()
        sleep(self.sleep_time)

        # download preview image
        preview_bytes = downloader.download(preview_id, "image/jpeg")
        # parse preview and create 3 images of different formats
        previews = self.parse_preview(preview_bytes)
        return "previews", previews

    def parse_data(self):
        return super().parse_data(inject=self.get_preview)

    def get_preview_id(self, index):
        return self.sheet[f"K{index}"].value.split("id=")[-1]

    def parse_preview(self, preview):
        sizes = [(481, 380), (368, 428), (368, 788)]

        original_image = Image.open(preview)

        previews = []

        for size_index, size in enumerate(sizes):
            fit_and_resized_image = ImageOps.fit(original_image, size, Image.ANTIALIAS)

            buffer = io.BytesIO()
            fit_and_resized_image.save(buffer, format="JPEG")

            image = ContentFile(buffer.getvalue())
            image_obj = InMemoryUploadedFile(
                image, None, f"preview-{size_index}.jpg", "image/jpeg", image.tell, None
            )

            previews.append(image_obj)

        return previews


class BlogParser(BaseParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.select_sheet("Blog article example")
        self.min_index = 2

    def get_picture(self, index):
        picture_id = self.get_picture_id(index)

        try:
            downloader = Downloader()
            sleep(self.sleep_time)
            picture_bytes = downloader.download(picture_id, "image/jpeg")
            picture = self.parse_image(picture_bytes, "picture.jpg")
        except Exception:
            picture = None

        return "picture", picture

    def parse_data(self):
        return super().parse_data(inject=self.get_picture)

    def get_picture_id(self, index):
        return self.sheet[f"K{index}"].value.split("id=")[-1]


class PersonParser(BaseParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.select_sheet("About - People")
        self.min_index = 2

    def get_data_by_index(self, index: int):
        name = self.get_name(index)
        if not name:
            return
        century_id = self.get_century_id(index)
        year_of_birth = self.get_year_of_birth(index)
        year_of_death = self.get_year_of_death(index)
        short_bio = self.get_short_bio(index)
        bio_id = self.get_bio_id(index)
        activity_id = self.get_activity_id(index)
        photo_id = self.get_photo_id(index)

        downloader = Downloader()
        sleep(self.sleep_time)

        bio_bytes = downloader.download(bio_id, "text/html", export=True)
        # parse and edit html
        bio = self.parse_content(bio_bytes.read())
        downloader = Downloader()
        sleep(self.sleep_time)
        photo_bytes = downloader.download(photo_id, "image/jpeg")
        photo = self.parse_image(photo_bytes, "photo.jpg")
        return {
            "name": name,
            "century_id": century_id,
            "year_of_birth": year_of_birth,
            "year_of_death": year_of_death,
            "short_bio": short_bio,
            "bio": bio,
            "activity_id": activity_id,
            "photo": photo,
        }

    def parse_data(self, inject=None):
        return super().parse_data(inject)

    def get_name(self, index):
        return self.sheet[f"A{index}"].value

    def get_century_id(self, index):
        return int(self.sheet[f"B{index}"].value)

    def get_year_of_birth(self, index):
        return str(int(self.sheet[f"C{index}"].value))

    def get_year_of_death(self, index):
        date = self.sheet[f"D{index}"].value

        if not date:
            return None

        return str(int(date))

    def get_short_bio(self, index):
        return self.sheet[f"E{index}"].value

    def get_bio_id(self, index):
        return self.sheet[f"F{index}"].value.split("id=")[-1]

    def get_activity_id(self, index):
        return int(self.sheet[f"G{index}"].value)

    def get_photo_id(self, index):
        return self.sheet[f"H{index}"].value.split("id=")[-1]


class AchievementsParser(BaseParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.min_index = 2
        self.select_sheet("About - Achievements")

    def get_activity(self, index):
        activity_id = self.get_activity_id(index)
        return activity_id

    def get_data_by_index(self, index: int):
        title = self.get_title(index)
        if not title:
            return
        year = self.get_year(index)
        image_id = self.get_image_id(index)
        description_id = self.get_description_id(index)

        sleep(self.sleep_time)
        downloader = Downloader()

        image_bytes = downloader.download(image_id, "image/jpeg")
        image = self.parse_image(image_bytes, f"slider-{index}.jpg")

        sleep(self.sleep_time)
        description_bytes = downloader.download(
            description_id, "text/html", export=True
        )

        description = self.parse_content(description_bytes.read())
        collected = {
            "title": title,
            "year": year,
            "image": image,
            "description": description,
        }

        if not self.inject:
            activity_id = self.get_activity(index)
            collected["activity_id"] = activity_id
        else:
            name, obj = self.inject(index)
            collected[name] = obj
        return collected

    def parse_data(self, inject=None):
        return super().parse_data(inject=inject)

    def get_year(self, index):
        return int(self.sheet[f"B{index}"].value)

    def get_image_id(self, index):
        return self.sheet[f"C{index}"].value.split("id=")[-1]

    def get_description_id(self, index):
        return self.sheet[f"D{index}"].value.split("id=")[-1]

    def get_activity_id(self, index):
        return int(self.sheet[f"E{index}"].value)

    def get_seo_tags(self, index: int) -> dict:
        if not self.sheet[f"G{index}"].value:
            return None
        image = self.get_seo_image(index, "I")
        img_name = slugify(self.sheet[f"A{index}"].value) + ".jpg"
        img_path = os.path.join(settings.MEDIA_ROOT, img_name)
        image.save(img_path, format="jpeg")
        return {
            "title": self.sheet[f"A{index}"].value,
            "description": self.sheet[f"G{index}"].value,
            "keywords": self.sheet[f"H{index}"].value,
            "image_url": f"https://{settings.SITE_URL}{settings.MEDIA_URL}{img_name}",
            "image_alt": self.sheet[f"J{index}"].value,
        }


class CommonProjectsParser(AchievementsParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.select_sheet("About - Common projects")

    def get_industry(self, index):
        return "industry_id", int(self.sheet[f"E{index}"].value)

    def parse_data(self):
        return super().parse_data(inject=self.get_industry)

    def get_seo_tags(self, index: int) -> dict:
        if not self.sheet[f"G{index}"].value:
            return None
        image = self.get_seo_image(index, "I")
        img_name = slugify(self.sheet[f"A{index}"].value) + ".jpg"
        img_path = os.path.join(settings.MEDIA_ROOT, img_name)
        image.save(img_path, format="jpeg")
        return {
            "title": self.sheet[f"A{index}"].value,
            "description": self.sheet[f"G{index}"].value,
            "keywords": self.sheet[f"H{index}"].value,
            "image_url": f"https://{settings.SITE_URL}{settings.MEDIA_URL}{img_name}",
            "image_alt": self.sheet[f"J{index}"].value,
        }


class StartupParser(BaseParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.min_index = 3
        self.select_sheet("Startup article example")

    def get_data_by_index(self, index: int):
        title = self.get_title(index)
        if not title:
            return
        return {
            "title": title,
            "industry_id": self.get_industry(index),
            "amount": self.get_amount(index),
            "created_at": self.get_date(index),
            "excerpt": self.get_excerpt(index),
            "text": self.get_text(index),
            "preview": self.get_preview(index),
        }

    def parse_data(self):
        return super().parse_data()

    def get_title(self, index):
        return self.sheet[f"A{index}"].value

    def get_industry(self, index):
        return int(self.sheet[f"B{index}"].value)

    def get_amount(self, index):
        return int(self.sheet[f"C{index}"].value) - 1

    def get_date(self, index):
        return self.sheet[f"F{index}"].value

    def get_excerpt(self, index):
        return self.sheet[f"G{index}"].value

    def get_text(self, index):
        link = self.sheet[f"H{index}"].value
        image_link = self.sheet[f"I{index}"].value
        # change image dimansion
        image_link = re.sub(r"dimension=\d{3}x\d{3}", "dimension=1255x500", image_link)

        content = requests.get(link).content

        soup = BeautifulSoup(content, "html.parser")
        text = [
            el for el in soup.find_all("div") if el.attrs.get("data-name") == "text"
        ][0]

        del text.attrs["class"]
        del text.attrs["data-action"]
        del text.attrs["data-name"]
        del text.attrs["id"]

        img = soup.new_tag("img")
        img["src"] = image_link
        img["class"] = "img-fluid"

        img_wrapper = soup.new_tag("div")
        img_wrapper["style"] = "text-align: center;"

        img_wrapper.append(img)

        text.append(img_wrapper)

        return text.prettify()

    def get_preview(self, index):
        link = self.sheet[f"I{index}"].value
        # change image dimansion
        link = re.sub(r"dimension=\d{3}x\d{3}", "dimension=490x380", link)

        img_bytes = requests.get(link).content

        img = self.parse_image(io.BytesIO(img_bytes), "preview.jpg")

        return img

    def parse_image(self, imgbytes, name):
        image = Image.open(imgbytes).convert("RGB")
        buffer = io.BytesIO()
        image.save(buffer, format="JPEG")
        image = ContentFile(buffer.getvalue())
        image_obj = InMemoryUploadedFile(
            image, None, name, "image/jpeg", image.tell, None
        )

        return image_obj


class ContractorParser(BaseParser):
    def __init__(self, wb):
        super().__init__(wb)
        self.min_index = 2
        self.select_sheet("Для заливки")

    def select_sheet(self, sheet_name):
        self.sheet = self.wb[sheet_name]

    def get_data_by_index(self, index: int):
        email = self.get_email(index)
        if not email:
            return
        ceo = self.get_ceo(index)
        company = self.get_company(index)
        ownership = self.get_ownership(index)
        location = self.get_location(index)
        date_of_creation = self.get_date(index)
        description = self.get_description(index)
        languages = self.get_languages(index)
        website = self.get_website(index)
        company_size = self.get_company_size(index)
        flexible_working_hours = self.get_flexible_hours(index)
        slogan = self.get_slogan(index)
        industry = self.get_industry(index)
        core_skills = self.get_core_skills(index)
        service_name = self.get_service_name(index)
        service_description = self.get_service_description(index)
        service_category = self.get_service_category(index)
        tags = self.get_tags(index)
        logo_id = self.get_logo_id(index)
        rate = self.get_rate(index)
        if logo_id:
            downloader = Downloader()
            sleep(self.sleep_time)
            logo_bytes = downloader.download(logo_id, "image/jpeg")
            logo = self.parse_image(logo_bytes, "logo.jpg")
        else:
            logo = None
        return {
            "email": email,
            "ceo": ceo,
            "company": company,
            "ownership": ownership,
            "location": location,
            "date_of_creation": date_of_creation,
            "description": description,
            "languages": languages,
            "website": website,
            "company_size": company_size,
            "flexible_working_hours": flexible_working_hours,
            "slogan": slogan,
            "industry": industry,
            "core_skills": core_skills,
            "service_name": service_name,
            "service_description": service_description,
            "service_category": service_category,
            "tags": tags,
            "logo": logo,
            "rate": rate,
        }

    def parse_data(self):
        return super().parse_data()

    def get_email(self, index):
        return self.sheet[f"A{index}"].value

    def get_ceo(self, index):
        return self.sheet[f"B{index}"].value

    def get_company(self, index):
        return self.sheet[f"C{index}"].value

    def get_ownership(self, index):
        return self.sheet[f"D{index}"].value

    def get_location(self, index):
        return self.sheet[f"E{index}"].value

    def get_date(self, index):
        return int(self.sheet[f"F{index}"].value)

    def get_description(self, index):
        return self.sheet[f"H{index}"].value

    def get_languages(self, index):
        return self.sheet[f"I{index}"].value

    def get_website(self, index):
        return self.sheet[f"J{index}"].value

    def get_company_size(self, index):
        count = int(self.sheet[f"K{index}"].value)

        if 5 <= count <= 15:
            return 1

        if 16 <= count <= 49:
            return 2

        if 50 <= count <= 199:
            return 3

        if 200 <= count <= 499:
            return 4

        if 500 <= count <= 999:
            return 5

        if 1000 <= count:
            return 6

    def get_flexible_hours(self, index):
        flexible_hours = self.sheet[f"L{index}"].value

        if flexible_hours == "Available":
            return True

        return False

    def get_slogan(self, index):
        return self.sheet[f"G{index}"].value

    def get_industry(self, index):
        return self.sheet[f"M{index}"].value

    def get_core_skills(self, index):
        core_skills = self.sheet[f"N{index}"].value
        if core_skills:
            return [skill.strip() for skill in core_skills.split(",")]

        return []

    def get_service_name(self, index):
        return self.sheet[f"O{index}"].value

    def get_service_description(self, index):
        return self.sheet[f"P{index}"].value

    def get_service_category(self, index):
        return self.sheet[f"Q{index}"].value

    def get_tags(self, index):
        return [tag.strip() for tag in self.sheet[f"R{index}"].value.split(",")]

    def get_logo_id(self, index):
        link = self.sheet[f"S{index}"].value
        if link == "НЕТ ЛОГО":
            return None

        return link.split("?id=")[-1]

    def parse_image(self, imgbytes, name):
        image = Image.open(imgbytes).convert("RGB")
        buffer = io.BytesIO()
        image.save(buffer, format="JPEG")
        image = ContentFile(buffer.getvalue())
        image_obj = InMemoryUploadedFile(
            image, None, name, "image/jpeg", image.tell, None
        )

        return image_obj

    def get_rate(self, index):
        return float(self.sheet[f"T{index}"].value)
