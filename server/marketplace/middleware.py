from django.contrib.auth.models import AnonymousUser
from graphql_jwt.shortcuts import get_user_by_token


class RestAuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        user = AnonymousUser()

        try:
            user = get_user_by_token(request.META["HTTP_AUTHORIZATION"].split()[-1])
        except Exception:
            pass

        request.rest_user = user

        response = self.get_response(request)

        return response
