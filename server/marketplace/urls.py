from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from rest_framework.routers import DefaultRouter

from accounts import views as accounts_views
from user_filter_api.views import SearchFilterViewSet
from user_settings import views as settings_views

router = DefaultRouter()
router.register("filters", SearchFilterViewSet)

urlpatterns = [
    path("accounts/", include("accounts.urls")),
    path("core/", include("core.urls")),
    path("company/", include("company.urls")),
    path("chat/", include("chat.urls")),
    url("", include("social_django.urls", namespace="social")),
    path(
        "password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    # set url for accounts app view here because accounts app uses as App hook
    path(
        "reset/<uidb64>/<token>/",
        accounts_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "activate/<uidb64>/<token>/",
        accounts_views.ActivateAccountView.as_view(),
        name="activate_account",
    ),
    path(
        "get-secret-key/<uidb64>/<token>/",
        accounts_views.GetSecretKeyView.as_view(),
        name="get-secret-key",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path(
        "invite/<uid>/",
        settings_views.new_invitee_password_view,
        name="create_invitee_password",
    ),
    path("invite/", settings_views.create_new_invitee_view, name="save_invitee"),
    path(
        "invite/<uid>/<pid>/<token>/", settings_views.save_invitee, name="save-invitee"
    ),
    path("django-admin/", admin.site.urls),
    path("api/v1/chats/", include("chat_api.urls")),
    path("api/v1/accounts/", include("accounts_api.urls")),
    path("api/v1/documents/", include("documents_api.urls")),
    path("api/v1/projects/", include("projects_api.urls")),
    path("api/v1/", include(router.urls)),
    path("", include("cms.urls")),
]

# urlpatterns += patterns(
#     prefix_default_language=False)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
