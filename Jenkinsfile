pipeline {
    agent { label "master" }
    environment {
        //BRANCH_NAME = "develop"
        ARTIFACTS_DIR = "/var/lib/jenkins/artifacts/engre"
        HOME_DIR = "/home/ubuntu/deploy"
        CURRENT_VERSION_DIR = "/home/ubuntu/deploy/current/"
        CURRENT_VERSION_FILE_PATH = "${env.CURRENT_VERSION_DIR}/current.tgz"
        SITE_DIR = "/var/www/marketplace/"
        PGUSER = ""
    }
    stages {
        stage('Preparations') {
            steps {
                echo "Initialize Workspace"
                deleteDir()
                git branch: "${env.BRANCH_NAME}", credentialsId: 'BITBUCKET_ACCESS', url: 'https://bitbucket.org/NIkita_Ryabukhin/engre'
                    
                script {
                    env.GIT_SHORT_HASH = sh( returnStdout: true, script: "git rev-parse --short HEAD" ).split("\r\n")[-1].replaceAll("\\s", "")
                    def verMajMin = "0.0"
                    if ( fileExists('./version') ) {
                      verMajMin = readFile( file: "./version" ).trim()
                    }
                    env.VERSIONFULL = "${verMajMin}.${env.BUILD_ID}-${env.BRANCH_NAME}.${env.GIT_SHORT_HASH}"
                    def msg = "Build started"
                    print(env.VERSIONFULL)
                    writeFile(file: 'version-full.txt', text: env.VERSIONFULL, encoding: "UTF-8")
                    sh "env"
                    sendToSkype("START", msg)     
                }
            }
        }
        stage("Prepare archive") {
            steps {
                echo "archiving workspace"
                sh """
                    tar --exclude-vcs -czvf ${ARTIFACTS_DIR}/${env.VERSIONFULL}.tgz .
                    cp ${ARTIFACTS_DIR}/${env.VERSIONFULL}.tgz ${env.WORKSPACE}
                """
                echo "Deleting old versions"
                sh """
                    cd ${ARTIFACTS_DIR}
                    ls -t | sed -e '1,10d' | xargs -d '\n' rm
                """
            }
            
        }
        stage('Deployment approval'){
            when {
                
                expression { env.BRANCH_NAME == 'master' }
            }
            steps {
                timeout(time: 15, unit: "MINUTES") {
                    input message: 'Do you want to approve the deploy in production?', ok: 'Yes'
                }
            }
        }
        stage('SSH thansfer'){
            steps([$class: 'BapSshPromotionPublisherPlugin']) {
                script{
                    print("${env.ARTIFACTS_DIR}/${env.VERSIONFULL}.tgz")
                    sshPublisher(
                        continueOnError: false, failOnError: true,
                        publishers: [
                            sshPublisherDesc(
                                configName: "${env.BRANCH_NAME}",
                                verbose: true,
                                transfers: [
                                    sshTransfer(sourceFiles: "${env.VERSIONFULL}.tgz")
                                ]
                            )
                        ]
                    )
                }
            }
        }
        stage('Deployment') {
            steps([$class: 'BapSshPromotionPublisherPlugin']) {
                script{
                    withCredentials([string(credentialsId: 'ENGRE_DEV_PG_PASSWORD', variable: 'token')]) {
                        env.PGPASSWORD = "${token}"
                    }
                    sshPublisher(
                        continueOnError: false, failOnError: true,
                        publishers: [
                            sshPublisherDesc(
                                configName: "${env.BRANCH_NAME}",
                                verbose: true,
                                transfers: [
                                    sshTransfer(execCommand: """
                                        echo "Deployment started"
                                        if test -f "${env.CURRENT_VERSION_FILE_PATH}"; then
                                            echo "Delete old backup"
                                            rm -f ${env.CURRENT_VERSION_FILE_PATH}
                                        fi
                                        echo "Archive site dir"
                                        cd ${env.SITE_DIR}
                                        tar -czf ${env.CURRENT_VERSION_FILE_PATH} .
                                        
                                        echo "Dump DB"
                                        rm -f ${env.CURRENT_VERSION_DIR}/current_dump.sql
                                        pg_dump -U marketplace -d marketplace > ${env.CURRENT_VERSION_DIR}/current.dump 
                                        
                                        echo "Update files"

                                        tar -xzvf ${env.HOME_DIR}/${env.VERSIONFULL}.tgz ./
                                        
                                        cd /var/www/marketplace

                                        echo "Install requirements"
                                        pip3 install -r ${env.SITE_DIR}requirements.txt
                                        npm install
                                    
                                        npm rebuild node-sass

                                        echo '''
                                        #!/bin/bash

                                        echo "Run migrations"
                                        python3 server/manage.py migrate

                                        gulp prod
                                        ''' >> migrations_and_gulp.sh
                                        
                                        chmod +x migrations_and_gulp.sh
                                        /bin/bash ${env.SITE_DIR}migrations_and_gulp.sh
                                        rm migrations_and_gulp.sh

//                                        /bin/bash  ${env.SITE_DIR}scripts/update_db.sh
                                        
                                        echo "Restart services"
                                        sudo service marketplace restart
                                        sudo service nginx restart
                                        rm -f ${env.HOME_DIR}/${env.VERSIONFULL}.tgz
                                    """)
                                ]
                            )
                        ]
                    )
                }
            }
        }
        stage("Healthcheck"){
            steps{
                script{
                    def siteUrl
                    if(env.BRANCH_NAME == 'master') { 
                        siteUrl = 'https://engre.co' 
                    }else{
                        siteUrl = 'https://test.engre.co' 
                    }
                    sleep(time:5,unit:"SECONDS")
                    def command = "curl -s -o /dev/null -w '%{http_code}' ${siteUrl}"
                    def response = sh( returnStdout: true, script: "${command}")
                    print("Response is: ${response}")
                    if(response == '200'){
                        print("Healthcheck successful")
                    } else{
                        makeRollback(env.BRANCH_NAME)
                        env.MSG = "Healthchek failed after deploy.Rollback finshed"   
                        throw new Exception("Healthcheck failed")
                    }
                }
            }
        }
     
    }
    post {
        always {
            echo "POST ACTIONS"
        }

        success {
            script {
                def msg = "Deployment successfully finished"
                sendToSkype("SUCCESS", msg)
            }
            deleteDir()
        }

        failure {
            script {
                def linkTobuild = "http://3.12.82.190/job/engre/job/${env.JOB_BASE_NAME}/${env.BUILD_ID}/console"
                def msg = "Deployment error\nLink to build logs:${linkTobuild}"
                sendToSkype("ERROR", msg)
            }
            deleteDir() 
        }
    }
}


def sendToSkype(status, text){
    env.LAST_COMMIT_AUTHOR = sh(returnStdout: true, script: "git show -s --pretty=%an");
    env.LAST_COMMIT_MESSAGE = sh(returnStdout: true, script: "git log -1 --pretty=%B");
    
    msg = """
        Status: ${status} 
        Version: ${env.VERSIONFULL}
        Branch: ${env.BRANCH_NAME}
        Last commit author:${env.LAST_COMMIT_AUTHOR.replaceAll("\\n","")}
        Last commit message:${env.LAST_COMMIT_MESSAGE.replaceAll("\\n","")}
        Additional info: ${text}
    """    
    //Dev Marketplace
    //env.SKYPE_CHAT_ID = "19:bb1fad3a84534b81a7e928885973c08a@thread.skype"
    //Marketplace CI
    env.SKYPE_CHAT_ID = "19:481e13dc2dcf4072b1dc9a69ac70bb91@thread.skype"
    //env.SKYPE_CHAT_ID = "8:romka846" 
    env.MSG = msg
    withCredentials([usernamePassword( credentialsId: 'ENGRE_SKYPE_ACCESS', passwordVariable: 'SKYPE_PASSWORD', usernameVariable: 'SKYPE_USER' )]){
        sh "python3 /var/lib/jenkins/send_to_skype.py"                   
    }
}

def makeRollback(branch){
    sshPublisher(
        continueOnError: false, failOnError: true,
        publishers: [
            sshPublisherDesc(
                configName: "${branch}",
                verbose: true,
                transfers: [
                    sshTransfer(execCommand: """
                        echo "Rollback started"
                    
                        echo "Restore files"
                        cd ${env.SITE_DIR}
                        tar -xzvf ${env.CURRENT_VERSION_DIR}/current.tgz ${env.SITE_DIR}
                        
                        echo "Restore DB"
                        pg_restore -c -U marketplace -d marketplace ${env.CURRENT_VERSION_DIR}/current.dump - restore  
                        
                        #echo "Run migrations"
                        #python3 server/manage.py makemigrations
                        #python3 server/manage.py migrate

                         echo '''
                        #!/bin/bash
                        gulp prod
                        ''' >> gulp.sh
                        
                        chmod +x gulp.sh
                        /bin/bash gulp.sh
                        rm gulp.sh
                                
                        echo "Restart services"
                        sudo service marketplace restart
                        sudo service nginx restart
                        
                    """)
                ]
            )
        ]
    ) 
}
