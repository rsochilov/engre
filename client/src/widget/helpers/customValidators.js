const phoneValidator = (value) => {
    if (value.length === 0) return true
    const phoheRegExp = /([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/
    return phoheRegExp.test(value);
}

const urlValidator = (value) => {
    if (value.length === 0) return true
    const urlRegExp = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi
    return urlRegExp.test(value);
}

export {phoneValidator, urlValidator}