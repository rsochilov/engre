export const isRequired = (name, value) => (value ? false : 'This field is required')
export const RichTextEditorContentRequired = (name, value) => (value === '<p></p>' || value === '' ? 'This field is required' : false)
export const isEmail = (name, value) => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const _isEmail = re.test(value)

  if (!_isEmail) {
    return 'Email invalid'
  }

  return false
}

export const shouldBeEqual = (fieldName, errorMsg) => (name, value, fields) => {
  if (value === fields[fieldName].value) {
    return false
  }

  return errorMsg
}

export const minLength = length => (name, value) => {
  if (value.toString().length >= length) {
    return false
  }

  return `Must be at least ${length} characters`
}

export const maxLength = length => (name, value) => {
  if (value.toString().length <= length) {
    return false
  }

  return `Must be no more than ${length} characters`
}

export const digitsOnly = (name, value) => {
  if (/^\d+$/.test(value)) {
    return false
  }

  return 'Invalid format'
}

export const isCyrillic = (name, value) => {
  const re = /[а-яА-Я]/
  const _isCyrillic = re.test(value)

  if (_isCyrillic) {
    return 'Your input contains cyrillic sybmols'
  }

  return false
}
