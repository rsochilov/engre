import Vue from 'vue'
import Swal from 'sweetalert2'

const dataValue = '{{data}}'
const propsContent = JSON.parse(dataValue.length > 0 ? dataValue : '{"props": {}}')
window.test = propsContent

export const prepareComponent = (component, options, mode = 'render') => {
  if (mode === 'render') {
    return new Vue({
      el: '#{{ el }}',
      ...options,
      render: h => h(component, propsContent)
    })
  }

  if (mode === 'register') {
    return new Vue({
      el: '#{{ el }}',
      ...options,
      components: {
        [component.name]: component
      },
      props: propsContent
    })
  }

  throw Error('Invalid prepare mode specified')
}

export const yearsRange = () => {
  const years = []

  for (let index = 2018; index <= new Date().getFullYear(); index += 1) {
    years.push(index)
  }

  return years
}

export const getProps = () => propsContent.props

export const getKeyByValue = (object, value) => Object.keys(object).find(key => object[key] === value)

export const findItem = (arr, itemId) => {
  if (arr.length) {
    const index = arr.findIndex(item => item.id === itemId)

    if (index > -1) {
      return arr[index]
    }

    for (let i = 0; i < arr.length; i += 1) {
      const comment = findItem(arr[i].reply_set, itemId)

      if (comment) {
        return comment
      }
    }
  }

  return null
}

export const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
})

const getRandom = array => array[Math.floor(Math.random() * array.length)]

export class Grid {
  constructor() {
    this.bricks = []
    this.gutter = 30

    this.wide = 1
    this.long = 2
    this.longFilled = 3
    this.small = 4
    this.types = [this.wide, this.long, this.longFilled, this.small]
  }

  getLong() {
    const choices = [this.long, this.longFilled]

    return choices[Math.floor(Math.random() * choices.length)]
  }

  addBrick(type) {
    this.bricks.push(type)
  }

  clear() {
    this.bricks = []
  }

  getNextBrickType() {
    const type = this._getNextType()

    this.addBrick(type)

    if (this.isBuilt()) {
      this.clear()
    }

    return type
  }

  _getNextType() {
    const randomType = this.types[Math.floor(Math.random() * this.types.length)]

    if (this.bricks.length === 0) {
      return randomType
    }

    if (this.bricks.length === 1) {
      if (this.contains(this.wide)) {
        return getRandom([this.small, this.getLong()])
      }

      if (this.contains(this.small)) {
        return this.wide
      }
    }

    if (this.bricks.length === 2) {
      if (this.contains(this.wide) && this.containsLong()) {
        return this.small
      }

      if (this.containsLong() && this.contains(this.small)) {
        return getRandom([this.small, this.wide])
      }

      if (this.containsLongTwice()) {
        return this.small
      }

      if (this.contains(this.wide) && this.contains(this.small)) {
        return getRandom([this.small, this.wide])
      }

      if (this.containsTimes(this.small, 2)) {
        return this.getLong()
      }
    }

    if (this.bricks.length === 3) {
      if (
        this.containsLong()
        && this.contains(this.wide)
        && this.contains(this.small)
      ) {
        return this.small
      }

      if (this.containsLongTwice() && this.contains(this.small)) {
        return this.small
      }

      if (this.containsLong() && this.containsTimes(this.small, 2)) {
        return this.wide
      }

      if (this.contains(this.wide) && this.containsTimes(this.small, 2)) {
        return this.wide
      }

      if (this.containsTimes(this.wide, 2) && this.contains(this.small)) {
        return this.small
      }
    }

    return randomType
  }

  containsLong(tiles = null) {
    let arr = tiles

    if (!arr) {
      arr = this.bricks
    }

    return arr.includes(3) || arr.includes(2)
  }

  containsOnly(value) {
    return this.bricks.every(item => item === value)
  }

  contains(value) {
    return this.bricks.includes(value)
  }

  containsTimes(value, times) {
    return (
      this.bricks.includes(value)
      && this.bricks.filter(item => item === value).length === times
    )
  }

  notContains(values) {
    for (let index = 0; index < values.length; index += 1) {
      if (this.bricks.includes(values[index])) {
        return false
      }
    }

    return true
  }

  containsLongTwice() {
    const containsLong = this.containsLong()

    if (!containsLong) {
      return false
    }

    if (this.containsTimes(this.long, 2)) {
      return true
    }

    if (this.containsTimes(this.longFilled, 2)) {
      return true
    }

    if (
      this.containsTimes(this.long, 1)
      && this.containsTimes(this.longFilled, 1)
    ) {
      return true
    }

    return false
  }

  isBuilt() {
    if (this.bricks.length < 4) {
      return false
    }

    if (this.containsLongTwice() && this.containsTimes(this.small, 2)) {
      return true
    }

    if (
      this.containsLong()
      && this.containsTimes(this.small, 2)
      && this.contains(this.wide)
    ) {
      return true
    }

    if (this.containsLong() && this.containsTimes(this.small, 4)) {
      return true
    }

    if (this.containsTimes(this.wide, 2) && this.containsTimes(this.small, 2)) {
      return true
    }

    return false
  }
}

export const capitalizeOne = str => str
  .charAt(0)
  .toUpperCase()
  .concat(str.slice(1).toLowerCase())

export const invokeFunctions = (obj, component) => {
  const result = {}

  Object.keys(obj).map((key) => {
    if (typeof obj[key] === 'function') {
      result[key] = obj[key].bind(component)()

      if (
        Object.values(result[key]).find(value => typeof value === 'function')
      ) {
        result[key] = invokeFunctions(result[key], component)
      }
    } else {
      result[key] = obj[key]
    }

    return null
  })

  return result
}
