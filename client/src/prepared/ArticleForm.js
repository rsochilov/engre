import ArticleForm from '@/components/Settings/Blogger/ArticleForm'
import { prepareComponent } from '@/utils'
import Vue from 'vue'
import VuejsClipper, {
  clipperFixed,
  clipperUpload,
  clipperPreview
} from 'vuejs-clipper'

Vue.use(VuejsClipper)
Vue.component('ClipperFixed', clipperFixed)
Vue.component('ClipperUpload', clipperUpload)
Vue.component('ClipperPreview', clipperPreview)

prepareComponent(ArticleForm)
