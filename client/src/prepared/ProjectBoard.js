import Vue from 'vue'
import ProjectBoard from '@/containers/Settings/ProjectBoard'
import { prepareComponent } from '@/utils'
import bModal from 'bootstrap-vue/es/components/modal/modal'
import bModalDirective from 'bootstrap-vue/es/directives/modal/modal'

Vue.component('b-modal', bModal)
Vue.directive('b-modal', bModalDirective)

prepareComponent(ProjectBoard)
