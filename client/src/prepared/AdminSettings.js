import AdminSettings from '@/containers/Admin/Settings'
import AdminLanguages from '@/components/Admin/Settings/Languages'
import AdminProfile from '@/components/Admin/Settings/Profile'
import AdminChangePassword from '@/components/Admin/Settings/ChangePassword'
import AdminCategory from '@/components/Admin/Settings/Category'
import AdminIndustry from '@/components/Admin/Settings/Industry'
import AdminStopWords from '@/components/Admin/Settings/StopWords'
import Vue from 'vue'
import VueRouter from 'vue-router'
import { prepareComponent, getProps } from '@/utils'

Vue.use(VueRouter)

const routes = [
  {
    path: '/admin/settings/languages',
    component: AdminLanguages,
    name: 'languages',
    props: getProps()
  },
  {
    path: '/admin/settings/profile',
    component: AdminProfile,
    name: 'profile',
    props: getProps()
  },
  {
    path: '/admin/settings/change-password',
    component: AdminChangePassword,
    name: 'change-password',
    props: getProps()
  },
  {
    path: '/admin/settings/category',
    component: AdminCategory,
    name: 'category',
    props: getProps()
  },
  {
    path: '/admin/settings/industry',
    component: AdminIndustry,
    name: 'industry',
    props: getProps()
  },
  {
    path: '/admin/settings/stop-words',
    component: AdminStopWords,
    name: 'stop-words',
    props: getProps()
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

prepareComponent(AdminSettings, { router })
