import Vue from 'vue'
import VueRouter from 'vue-router'
import News, { List } from '@/components/News'
import { prepareComponent, getProps } from '@/utils'
import { VueMasonryPlugin } from 'vue-masonry'

Vue.use(VueMasonryPlugin)
Vue.use(VueRouter)

const routes = [
  {
    path: '/:base/:category',
    component: List,
    props: getProps()
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

prepareComponent(News, { router })
