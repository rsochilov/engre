#!/bin/bash

MODE=$1
OPTION=$2

SLEEP_TIME=8
CONT_DB_NAME="marketplace_db"
CONT_DJANGO_NAME="config_django_1"
CMS_DUMP="cms_dump.sql"
DB_PORT=5432

shift
source ./config/.env

if [[ $MODE == "dev" ]]
    then
        DOCKER="docker-compose -f config/docker-compose.yml -f config/dev.yml"

elif [[ $MODE == "staging" ]]
    then
        DOCKER="docker-compose -f config/docker-compose.yml -f config/staging.yml"

elif [[ $MODE == 'prod' ]]
    then
        DOCKER="docker-compose -f config/docker-compose.yml -f config/prod.yml"

elif [[ $MODE == 'testing' ]]
    then
        DOCKER="docker-compose -f config/docker-compose.yml -f config/dev.yml -f config/testing.yml"

elif [[ $MODE == 'help' ]]
    then
        printf "First start: \"./app dev deploy_local\"\nUsual start: \"./app dev up\"\nApplying migrations: \"./app dev migrate\"\nLoad from google docs: \"./app dev upload_from_google\"\n"
    exit 1

else
    echo "\"$MODE\" is unkown start option. Use one of the following: [\"dev\", \"prod\", \"staging\", \"testing\"]"
    exit 1
fi

# run commands with docker-compose
if [[ $OPTION == "dropdb" ]]
    then
        $DOCKER up -d db &&
        $DOCKER kill django &&
        $DOCKER run db dropdb -h db -U $DATABASE_USER $DATABASE_NAME

elif [[ $OPTION == "createdb" ]]
    then
        $DOCKER up -d db &&
        $DOCKER run db createdb -h db -U $DATABASE_USER -O $DATABASE_USER $DATABASE_NAME

elif [[ $OPTION == 'resetdb' ]]
    then
        $DOCKER up -d db &&
        $DOCKER kill django &&
        $DOCKER run db dropdb -h db -U $DATABASE_USER $DATABASE_NAME
        $DOCKER run db createdb -h db -U $DATABASE_USER -O $DATABASE_USER $DATABASE_NAME &&
        $DOCKER run db psql -h db -U $DATABASE_USER $DATABASE_NAME < dump.sql

elif [[ $OPTION == "makedump" ]]
    then
        $DOCKER up -d db &&
        $DOCKER run db pg_dump -h db -U $DATABASE_USER -f /app/dump.sql

elif [[ $OPTION == "applydump" ]]
    then
        $DOCKER up -d db &&
        $DOCKER run db psql -h db -U $DATABASE_USER $DATABASE_NAME < dump.sql

elif [[ $OPTION == "deploy_local" ]]
    then
        $DOCKER up -d db &&
        bin/wait-for-it.sh -h db -p $DB_PORT -t $SLEEP_TIME --
        echo "Starting dump restore" &&
        gunzip < pg_dumps/dump_production.gz | docker exec -i $CONT_DB_NAME psql -h localhost -U $DATABASE_USER $DATABASE_NAME &&
#        echo "Restoring last cms updates" &&
#        scripts/cms_restore.sh pg_dumps/$CMS_DUMP &&
        echo "Starting other docker containers" &&
        $DOCKER up

elif [[ $OPTION == "cms_dump" ]]
    then
	DATE_TIME=$(date +'%m.%d.%Y_%H:%M:%S');
	BACKUP_FILENAME="cms_dump_$DATE_TIME.$USER.sql";
        $DOCKER up -d db &&
        bin/wait-for-it.sh -h db -p $DB_PORT -t $SLEEP_TIME --
	$DOCKER exec db pg_dump marketplace -h db -U $DATABASE_USER -f /app/pg_dumps/$BACKUP_FILENAME -t 'cms_*' &&
        echo "Cms backup saved to $BACKUP_FILENAME";

elif [[ $OPTION == "mergedump" ]]
    then
        $DOCKER up -d db &&
        $DOCKER run db pg_dump -h db -U $DATABASE_USER -f /app/temp_dump.sql
        diff temp_dump.sql dump.sql > patch.input &&
        patch dump.sql patch.input

elif [[ $OPTION == "createsuperuser" ]]
    then
        $DOCKER run django python create_user.py

elif [[ $OPTION == "shell" ]]
    then
        $DOCKER run django python manage.py shell_plus

elif [[ $OPTION == "migrate" ]]
    then
        $DOCKER up -d db django &&
        docker exec -it $CONT_DJANGO_NAME python manage.py migrate;

elif [[ $OPTION == "upload_from_google" ]]
    then
        $DOCKER up -d db django &&
	echo "Running load_about_achievements" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_about_achievements &&
	echo "Running load_about_commonprojects" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_about_commonprojects &&
	echo "Running load_about_persons" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_about_persons &&
	echo "Running load_blog_articles" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_blog_articles &&
	echo "Running load_contractors" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_contractors &&
	echo "Running load_news_articles" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_news_articles &&
	echo "Running load_startups" &&
        docker exec -it $CONT_DJANGO_NAME python manage.py load_startups;

elif [[ $OPTION == "slugify_ownerships" ]]
    then
        $DOCKER up -d db django &&
        docker exec -it $CONT_DJANGO_NAME python manage.py slugify_company_ownership;

elif [[ $OPTION == "freeze" ]]
    then
        $DOCKER run django pip freeze > requirements.txt &&
        head -n -2 requirements.txt > temp.txt && cat temp.txt > requirements.txt && rm temp.txt

elif [[ $OPTION == "renode" ]]
    then
        $DOCKER kill node &&
        $DOCKER up -d node

else
    $DOCKER "$@"
fi
