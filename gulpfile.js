const gulp = require('gulp')
const sass = require('gulp-sass');
const fs = require('fs')
const log = require('fancy-log')
const webpack = require('webpack-stream')
const named = require('vinyl-named')
const tap = require('gulp-tap')

const saveStats = (data) => {
  fs.writeFileSync('./stats.json', JSON.stringify(data), (err) => {
    if (err) {
      return console.log(err)
    }

    log('stats.json updated')
  })
}

const getFileName = (filePath) => {
  const paths = filePath.split('/')
  return paths[paths.length - 1]
}

/* eslint-disable global-require */
gulp.task('build:dev', () => {
  const data = { status: 'built', components: [], vendor: null }

  return gulp
    .src('client/src/prepared/**/*.js')
    .pipe(named())
    .pipe(
      tap((file, t) => {
        const componentName = getFileName(file.path).split('.')[0]
        data.components.push({ component: componentName })
      })
    )
    .pipe(
      webpack(require('./webpack.config.js'), null, (err, stats) => {
        const { hash } = stats.compilation

        data.components = data.components.map(({ component }) => ({
          component,
          file: `${component}.${hash}.js`
        }))
        data.vendor = `vendor.${hash}.js`
      })
    )
    .pipe(gulp.dest('server/static/vuecomponents/'))
    .on('finish', () => saveStats(data))
})

gulp.task('build:prod', () => {
  const data = { status: 'built', components: [], vendor: null }

  return gulp
    .src('client/src/prepared/**/*.js')
    .pipe(named())
    .pipe(
      tap((file, t) => {
        const componentName = getFileName(file.path).split('.')[0]
        data.components.push({ component: componentName })
      })
    )
    .pipe(
      webpack(require('./webpack.prod.config.js'), null, (err, stats) => {
        const { hash } = stats.compilation

        data.components = data.components.map(({ component }) => ({
          component,
          file: `${component}.${hash}.js`
        }))
        data.vendor = `vendor.${hash}.js`
      })
    )
    .pipe(gulp.dest('server/staticfiles/vuecomponents/'))
    .on('finish', () => saveStats(data))
})

gulp.task('move:images', () => gulp.src('server/static/images/**/*.*').pipe(
  gulp.dest('server/staticfiles/images')
))

gulp.task('move:style', () => gulp.src('server/static/scss/**/*.*').pipe(
  gulp.dest('server/staticfiles/scss')
))

gulp.task('move:js', () => gulp.src('server/static/js/**/*.*').pipe(
    gulp.dest('server/staticfiles/js')
))

gulp.task('sass:static', () => gulp.src('server/static/scss/*.scss').pipe(sass().on('error', sass.logError)).pipe(gulp.dest('server/static/scss/')))

gulp.task('sass:staticfiles', () => gulp.src('server/staticfiles/scss/*.scss').pipe(sass().on('error', sass.logError)).pipe(gulp.dest('server/staticfiles/scss/')))


gulp.task('clean', () => gulp.src('server/static/vuecomponents/**/*.{js,map,css}').pipe(
  tap((file, t) => {
    const stats = { status: 'building', components: [] }

    saveStats(stats)

    fs.unlink(file.path, (err) => {
      if (err) {
        log(err)
      }
    })
  })
))

gulp.task('watch', () => gulp.watch('./client/src/**/*.{js,vue}',{ignoreInitial: false}, gulp.series('sass', 'move')))

gulp.task('move', gulp.series('move:images', 'move:style', 'move:js'))

gulp.task('sass', gulp.series('sass:static', 'sass:staticfiles'))

gulp.task('dev', gulp.series('clean', 'sass', 'move', 'build:dev'))

gulp.task('prod', gulp.series('clean', 'build:prod', 'move', 'sass'))

gulp.task('default', gulp.series('clean', 'build:dev', 'watch'))